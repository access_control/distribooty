#/bin/bash

configFiles=("config1.hjson" "config2.hjson" "config3.hjson")
debugPorts=(8000 8001 8002)

if [[ -n $1 ]]; then

if [[ -z $2 ]]; then
c="C_SIMPLE_LOCKS"
else
c=$2
fi

. ./init_environment.sh
java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=localhost:${debugPorts[$1 - 1]} test.RemotePeerDemo ${configFiles[$1 - 1]} $c

else

echo "Provide Argument"

fi

