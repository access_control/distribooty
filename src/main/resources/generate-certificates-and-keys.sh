 #!/bin/bash
 
 # Generating of root certificate
 
val=1000

# Key generation
keytool -genkeypair -v -alias pid-root -dname "CN=Connect Four, OU=Java Freaks, O=Amazing Connect4, L=Connect4 City, ST=Connect4 Land, C=C4" -keyalg EC -keysize 384 -sigalg SHA512withECDSA -ext bc=ca:true -validity $val -keystore connect4-root.keystore -storepass connect4-root -storetype pkcs12

#keytool -list -v -keystore connect4-root.keystore -storepass connect4-root

## export ca

keytool -exportcert -alias pid-root -file connect4-root.crt -keystore connect4-root.keystore -storepass connect4-root
 
 ### Generating certificates for 2000

# key generation
keytool -genkeypair -v -alias pid-2000 -dname "CN=localhost, OU=Java Freaks, O=Amazing Connect4, L=Connect4 City, ST=Connect4 Land, C=C4" -keyalg EC -keysize 384 -sigalg SHA512withECDSA -validity $val -keystore connect4-2000.keystore -storepass connect4-2000 -storetype pkcs12

#keytool -list -v -keystore connect4-2000.keystore -storepass connect4-2000

# certificate signing request generation
keytool -certreq -v -alias pid-2000 -sigalg SHA512withECDSA -file connect4-2000.csr -keystore connect4-2000.keystore -storepass connect4-2000

# certificate signing reply generation
keytool -gencert -v -alias pid-root -validity $val -keystore connect4-root.keystore -storepass connect4-root -infile connect4-2000.csr -outfile connect4-2000.cer

# import ca certificate
echo "yes" | keytool -importcert -v -alias pid-root -file connect4-root.crt -keystore connect4-2000.keystore -storepass connect4-2000

# import certificate reply
keytool -importcert -v -alias pid-2000 -file connect4-2000.cer -keystore connect4-2000.keystore -storepass connect4-2000

# export pid-2000 certificate
keytool -exportcert -rfc -alias pid-2000 -file connect4-2000.crt -keystore connect4-2000.keystore -storepass connect4-2000

# export pid-2000 key
echo "connect4-2000" | openssl pkcs12 -in connect4-2000.keystore -nodes -nocerts -out  connect4-2000-key.pem

### Generating certificates for 2001

keytool -genkeypair -v -alias pid-2001 -dname "CN=localhost, OU=Java Freaks, O=Amazing Connect4, L=Connect4 City, ST=Connect4 Land, C=C4" -keyalg EC -keysize 384 -sigalg SHA512withECDSA -validity $val -keystore connect4-2001.keystore -storepass connect4-2001 -storetype pkcs12

#keytool -list -v -keystore connect4-2001.keystore -storepass connect4-2001

keytool -certreq -v -alias pid-2001 -sigalg SHA512withECDSA -file connect4-2001.csr -keystore connect4-2001.keystore -storepass connect4-2001

keytool -gencert -v -alias pid-root -validity $val -keystore connect4-root.keystore -storepass connect4-root -infile connect4-2001.csr -outfile connect4-2001.cer

echo "yes" | keytool -importcert -v -alias pid-root -file connect4-root.crt -keystore connect4-2001.keystore -storepass connect4-2001

keytool -importcert -v -alias pid-2001 -file connect4-2001.cer -keystore connect4-2001.keystore -storepass connect4-2001

#keytool -list -v -keystore connect4-2001.keystore -storepass connect4-2001

keytool -exportcert -rfc -alias pid-2001 -file connect4-2001.crt -keystore connect4-2001.keystore -storepass connect4-2001

echo "connect4-2001" | openssl pkcs12 -in connect4-2001.keystore -nodes -nocerts -out  connect4-2001-key.pem


### Generating certificates for 2002

keytool -genkeypair -v -alias pid-2002 -dname "CN=localhost, OU=Java Freaks, O=Amazing Connect4, L=Connect4 City, ST=Connect4 Land, C=C4" -keyalg EC -keysize 384 -sigalg SHA512withECDSA -validity $val -keystore connect4-2002.keystore -storepass connect4-2002 -storetype pkcs12

#keytool -list -v -keystore connect4-2002.keystore -storepass connect4-2002

keytool -certreq -v -alias pid-2002 -sigalg SHA512withECDSA -file connect4-2002.csr -keystore connect4-2002.keystore -storepass connect4-2002

keytool -gencert -v -alias pid-root -validity $val -keystore connect4-root.keystore -storepass connect4-root -infile connect4-2002.csr -outfile connect4-2002.cer

echo "yes" | keytool -importcert -v -alias pid-root -file connect4-root.crt -keystore connect4-2002.keystore -storepass connect4-2002

keytool -importcert -v -alias pid-2002 -file connect4-2002.cer -keystore connect4-2002.keystore -storepass connect4-2002

#keytool -list -v -keystore connect4-2002.keystore -storepass connect4-2002

keytool -exportcert -rfc -alias pid-2002 -file connect4-2002.crt -keystore connect4-2002.keystore -storepass connect4-2002

echo "connect4-2002" | openssl pkcs12 -in connect4-2002.keystore -nodes -nocerts -out  connect4-2002-key.pem


