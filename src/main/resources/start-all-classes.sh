#/bin/bash

config_numbers=(1 2 3)

if [[ $# -gt 0 ]]; then
for c in ${config_numbers[@]}; do
konsole --noclose -e java -cp .:hjson-3.0.0.jar $1 config"$c".hjson
done
else
echo "an argument as full class name is missing"
fi
