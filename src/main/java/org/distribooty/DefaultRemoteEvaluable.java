package org.distribooty;

import java.io.Serializable;

import org.distribooty.connector.RemoteElectable;
import org.distribooty.util.LamportClock;

public class DefaultRemoteEvaluable implements Serializable, Comparable<DefaultRemoteEvaluable> {

	private static final long serialVersionUID = 568381644920461674L;
	
	private long term;
	private long value;
	
	public DefaultRemoteEvaluable(long term, long value) {
		this.term = term;
		this.value = value;
	}

	@Override
	public int compareTo(DefaultRemoteEvaluable o) {
		/*int d = LamportClock.compare(term, o.term, RemoteElectable.DELTA); // TODO uncomment
		if(d != 0)
			return d;
		else	*/		
			return Long.signum(value - o.value);
	}

}
