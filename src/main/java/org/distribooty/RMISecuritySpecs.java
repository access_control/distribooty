package org.distribooty;

import java.nio.file.Path;

public class RMISecuritySpecs {
	private Path keystorePath;
	private String keystorePassword;
	private String enabledCipherSuites;
	private String enabledProtocols;
	private boolean needClientAuth;
	
	public RMISecuritySpecs(Path keystorePath, String keystorePassword, String enabledCipherSuites,
			String enabledProtocols, boolean needClientAuth) {
		this.keystorePath = keystorePath;
		this.keystorePassword = keystorePassword;
		this.enabledCipherSuites = enabledCipherSuites;
		this.enabledProtocols = enabledProtocols;
		this.needClientAuth = needClientAuth;
	}
	
	public Path getKeystorePath() {
		return keystorePath;
	}
	
	public String getKeystorePassword() {
		return keystorePassword;
	}
	
	public String getEnabledCipherSuites() {
		return enabledCipherSuites;
	}
	
	public String getEnabledProtocols() {
		return enabledProtocols;
	}
	
	public boolean isNeedClientAuth() {
		return needClientAuth;
	}	
}