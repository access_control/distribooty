package org.distribooty;

import java.io.Serializable;

public interface VotingMachine<E extends Serializable & Comparable<E>> {
	void onVotingBegin();
	void onEvaluableObject(int srcPid, E evaluableObject) throws Exception;
	ElectionResult onEvaluationEnd() throws Exception;
	void onEvaluationAbort(int srcPid, Throwable reason);
}
