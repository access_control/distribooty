package org.distribooty.replicate;

import java.io.Serializable;
import java.rmi.RemoteException;

import org.distribooty.util.AbstractConsumer;

public class RemoteSyncResponserService
<Q extends Serializable, 
R extends Serializable> 
extends AbstractConsumer<SyncRequestStatus> 
implements RemoteSyncResponser<Q> {

	private RemoteSyncRequestor<Q, R> requestor;
	private LocalSyncResponser<Q, R> responser;
	
	public RemoteSyncResponserService(RemoteSyncRequestor<Q, R> requestor, LocalSyncResponser<Q, R> responser) {
		super(SyncRequestStatus.FINISHED, "SyncResponserService-Thread");
		this.requestor = requestor;
		this.responser = responser;
	}

	@Override
	public SyncRequestStatus receiveRequestFragment(DataFragment<Q> requestFragment)
			throws RemoteException {
		SyncRequestStatus status = this.responser.collectRequestFragment(requestFragment);
		if(status != SyncRequestStatus.CONTINUE || requestFragment.isLastDataFragment())
			enqueue(status);
		return status;
	}

	@Override
	public void continueResponse() throws RemoteException {
		enqueue(SyncRequestStatus.CONTINUE);
	}

	@Override
	public void setResponseFragmentSize(long responseFragmentSize) throws RemoteException {
		this.responser.setResponseFragmentSize(responseFragmentSize);
	}

	@Override
	public void run() {
		
		onRunEnter();
		
		request_loop:
		while(true)
		{
			SyncRequestStatus requestStatus = dequeue();
			switch(requestStatus)
			{
				case WAIT_UNTIL_CONTINUE_REQUESTED:
					this.responser.applyCollectedRequestFragments();
					try {
						this.requestor.continueRequest();
					} catch (RemoteException e) {
						this.responser.abort();
						break request_loop;
					}
					break;
				case FINISHED:
					this.responser.applyCollectedRequestFragments();
				case CONTINUE:
				case OBLIGED_TO_SEND_RESPONSE:
					if(sendResponses())
						break;
					else
						break request_loop;					
			}
		}
		
		onRunLeave();
	}
	
	private boolean sendResponses() {
		response_loop:
		while(true)
		{
			DataFragment<R> pulledResponse = this.responser.pullResponseFragment();
			try {
				boolean last = pulledResponse.isLastDataFragment();
				SyncResponseStatus responseStatus = this.requestor.receiveResponseFragment(pulledResponse);
				switch(responseStatus)
				{
				case CONTINUE:
					if(last)
						break response_loop;
					else
						break;
				case WAIT_UNTIL_CONTINUE_REQUESTED:
				case FINISHED:
					break response_loop;
				}
			} catch (RemoteException e) {
				this.responser.abort();
				return false;
			}			
		}
		return true;
	}	

}
