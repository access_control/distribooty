package org.distribooty.replicate;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteSynchronizable <Q extends Serializable, R extends Serializable> extends Remote
{	
	void requestSync(RemoteSyncRequestor<Q, R> syncRequestor,
			long proposedRequestFragmentSize) throws RemoteException;
}
