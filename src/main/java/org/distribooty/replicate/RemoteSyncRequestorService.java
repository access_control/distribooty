package org.distribooty.replicate;

import java.io.Serializable;
import java.rmi.RemoteException;

import org.distribooty.util.AbstractConsumer;

public class RemoteSyncRequestorService
<Q extends Serializable, 
R extends Serializable> 
extends 
AbstractConsumer<SyncResponseStatus> 
implements 
RemoteSyncRequestor<Q, R> {

	private LocalSyncRequestor<Q, R> requestor;
	private RemoteSyncResponser<Q> responser;
	
	public RemoteSyncRequestorService(LocalSyncRequestor<Q, R> requestor) {
		super(SyncResponseStatus.FINISHED, "SyncRequestorService");
		this.requestor = requestor;
	}
	
	@Override
	public SyncResponseStatus receiveResponseFragment(DataFragment<R> responseFragment)
			throws RemoteException {
		SyncResponseStatus status = this.requestor.collectResponseFragment(responseFragment);
		if(status != SyncResponseStatus.CONTINUE || responseFragment.isLastDataFragment())
			enqueue(status);
		return status;
	}

	@Override
	public void continueRequest() throws RemoteException {
		enqueue(SyncResponseStatus.CONTINUE);
	}

	@Override
	public void prepareForSync(RemoteSyncResponser<Q> responser, long requestFragmentSize, long proposedResponseFragmentSize)
			throws RemoteException {
		this.responser = responser;
		long responseFragmentSize = requestor.proposeResponseFragmentSize(proposedResponseFragmentSize);
		this.requestor.setRequestFragmentSize(requestFragmentSize);
		this.responser.setResponseFragmentSize(responseFragmentSize);
		enqueue(SyncResponseStatus.CONTINUE);
	}

	@Override
	public void run() {
		
		onRunEnter();
		
		response_loop:
		while(true)
		{
			SyncResponseStatus responseStatus = dequeue();
			switch(responseStatus)
			{
				case CONTINUE:
					request_loop:
					do
					{
						try {
							SyncRequestStatus requestStatus = this.responser.receiveRequestFragment(this.requestor.pullRequestFragment());
							switch(requestStatus)
							{
								case CONTINUE:
									break;
								case WAIT_UNTIL_CONTINUE_REQUESTED:
								case OBLIGED_TO_SEND_RESPONSE:
								case FINISHED:
									break request_loop;
							}
						} catch (RemoteException e) {
							this.requestor.abort();
							break response_loop;
						}
					} while(true);
					break;
				case WAIT_UNTIL_CONTINUE_REQUESTED:
					this.requestor.applyCollectedResponseFragments();
				try {
					this.responser.continueResponse();
				} catch (RemoteException e) {
					this.requestor.abort();
					break response_loop;
				}
					break;
				case FINISHED:
					this.requestor.applyCollectedResponseFragments();
					break response_loop;
			}
		}
		
		onRunLeave();
	}
	
	

}
