package org.distribooty.replicate;

import java.io.Serializable;

public interface LocalSyncRequestor<Q extends Serializable, R extends Serializable> extends Abortable {
	SyncResponseStatus collectResponseFragment(DataFragment<R> responseFragment);
	DataFragment<Q> pullRequestFragment();
	void applyCollectedResponseFragments();
	long proposeResponseFragmentSize(long proposedResponseFragmentSize);
	void setRequestFragmentSize(long requestFragmentSize);
	long getInitialProposedRequestFragmentSize();
}
