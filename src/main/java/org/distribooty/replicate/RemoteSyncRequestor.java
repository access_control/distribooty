package org.distribooty.replicate;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteSyncRequestor<Q extends Serializable, R extends Serializable> extends Remote {
	SyncResponseStatus receiveResponseFragment(DataFragment<R> responseFragment) throws RemoteException;
	void continueRequest() throws RemoteException;
	void prepareForSync(RemoteSyncResponser<Q> responser,
			long requestFragmentSize,
			long proposedResponseFragmentSize) throws RemoteException;
}
