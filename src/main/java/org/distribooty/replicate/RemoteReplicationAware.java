package org.distribooty.replicate;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

import org.distribooty.TransactionID;

public interface RemoteReplicationAware<D extends Serializable> extends Remote {
	void abort(TransactionID transactionId) throws RemoteException;
	boolean preCommit(TransactionID transactionId) throws RemoteException;
	void commit(TransactionID transactionId, D data) throws RemoteException, CommitException;
}
