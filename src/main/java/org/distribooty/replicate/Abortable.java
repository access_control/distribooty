package org.distribooty.replicate;

public interface Abortable {
	void abort();
}
