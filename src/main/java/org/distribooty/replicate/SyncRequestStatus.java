package org.distribooty.replicate;

public enum SyncRequestStatus {
	CONTINUE, WAIT_UNTIL_CONTINUE_REQUESTED, OBLIGED_TO_SEND_RESPONSE, FINISHED;
}
