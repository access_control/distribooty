package org.distribooty.replicate;

public class CommitException extends Exception {

	private static final long serialVersionUID = 694890310706772075L;

	public CommitException() {
	}

	public CommitException(String message) {
		super(message);
	}

	public CommitException(Throwable cause) {
		super(cause);
	}

	public CommitException(String message, Throwable cause) {
		super(message, cause);
	}

	public CommitException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
