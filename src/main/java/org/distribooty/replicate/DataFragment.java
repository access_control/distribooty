package org.distribooty.replicate;

import java.io.Serializable;

public class DataFragment<T extends Serializable> implements Serializable {

	private static final long serialVersionUID = 5917000944835515829L;
	
	private T dataFragment;
	private boolean lastDataFragment;
	
	public DataFragment(T dataFragment, boolean lastDataFragment) {
		this.dataFragment = dataFragment;
		this.lastDataFragment = lastDataFragment;
	}
	
	public T getDataFragment() {
		return dataFragment;
	}

	public boolean isLastDataFragment() {
		return lastDataFragment;
	}
}
