package org.distribooty.replicate;

import java.io.Serializable;

public interface LocalSyncResponser<Q extends Serializable, R extends Serializable> extends Abortable {
	SyncRequestStatus collectRequestFragment(DataFragment<Q> requestFragment);
	DataFragment<R> pullResponseFragment();
	void applyCollectedRequestFragments();
	long proposeRequestFragmentSize(long proposedRequestFragmentSize);
	void setResponseFragmentSize(long responseFragmentSize);
	long getInitialProposedResponseFragmentSize();
}
