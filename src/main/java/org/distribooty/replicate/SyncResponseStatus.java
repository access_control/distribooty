package org.distribooty.replicate;

public enum SyncResponseStatus {
	CONTINUE, WAIT_UNTIL_CONTINUE_REQUESTED, FINISHED;
}
