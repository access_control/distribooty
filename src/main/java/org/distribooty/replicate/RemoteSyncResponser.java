package org.distribooty.replicate;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteSyncResponser<Q extends Serializable> extends Remote {
	SyncRequestStatus receiveRequestFragment(DataFragment<Q> requestFragment) throws RemoteException;
	void continueResponse() throws RemoteException;
	void setResponseFragmentSize(long responseFragmentSize) throws RemoteException;
}
