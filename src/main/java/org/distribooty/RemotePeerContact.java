package org.distribooty;

public class RemotePeerContact {

	private int pid;
	private String host;
	private int port;
	private String bindName;
	public RemotePeerContact(int pid, String host, int port, String bindName) {
		this.pid = pid;
		this.host = host;
		this.port = port;
		this.bindName = bindName;
	}
	
	public int getPid() {
		return pid;
	}
	
	public String getHost() {
		return host;
	}
	
	public int getPort() {
		return port;
	}
	
	public String getBindName() {
		return bindName;
	}
}
