package org.distribooty;

import java.util.Set;

/**
 * Called if some neighbours detected as lost.
 */
public interface RemotePeerFailureListener {
	/**
	 * 
	 * @param unavailablePids the pids of the neighbour components
	 */
	void onRemotePeerFail(Set<Integer> unavailablePids);
}
