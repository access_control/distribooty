package org.distribooty;

import java.util.Map;

import org.distribooty.connector.RemoteDiscoverable;
import org.distribooty.connector.RemoteObjectContext;

public interface DiscoveryAware {
	void onNeighbourDiscoveryChange(Map<Integer, RemotePeerDescriptor> retrievedPeers,
			BasicRemotePeerService service);
	void onRegister(int srcPid, RemoteDiscoverable discoverable);
	void onUnregister(int srcPid);
	RemoteObjectContext getRemoteObjectContext();
}
