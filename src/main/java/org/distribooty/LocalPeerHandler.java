package org.distribooty;

import java.io.Serializable;

public interface LocalPeerHandler<E extends Serializable & Comparable<E>, H extends Serializable> extends
MasterListener, MasterChangeListener, RemotePeerFailureListener, DiscoveryAware, LocalElectable<E>, LocalHeartbeatAware<H>
{
	void onInit(StartupMode startupMode, InitRemotePeerService initRemotePeerService);
	void onShutdown();
	void onRemotePeerShutdown(int srcPid);
}
