package org.distribooty.debug;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class RemoteLogger {
	
	//private static final Logger LOGGER = Logger.getLogger("org.distribooty");
	private static final Map<Integer, Logger> LOGGERS = new ConcurrentHashMap<>();
	private static final DefaultRemoteHandler HANDLER;
	private static Level defaultLevel;
	
	public static class DefaultRemoteFormatter extends Formatter {

		@Override
		public String format(LogRecord record) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(baos);
			ps.println(String.format("[%S %05d]: %s", record.getLevel().getName(), record.getSequenceNumber(), record.getMessage()));
			if(record.getThrown() != null)				
				record.getThrown().printStackTrace(ps);				
			return baos.toString();
		}
		
	}
	
	public static void applyDefaultLevel(Level defaultLevel) {
		RemoteLogger.defaultLevel = defaultLevel;
	}
	
	public static class DefaultRemoteHandler extends Handler {

		@Override
		public synchronized void publish(LogRecord record) {
			System.out.println(getFormatter().format(record));
		}

		@Override
		public synchronized void flush() {
			System.out.flush();			
		}

		@Override
		public void close() throws SecurityException {
			// Do nothing
		}
		
	}
	
	
	static {
		DefaultRemoteHandler handler = new DefaultRemoteHandler();
		Formatter formatter = new DefaultRemoteFormatter();
		handler.setFormatter(formatter);		
		try {
			handler.setEncoding("UTF-8");
		} catch (SecurityException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		handler.setLevel(Level.ALL);
		HANDLER = handler;
		/*LOGGER.setLevel(Level.ALL);
		LOGGER.addHandler(handler);
		LOGGER.setUseParentHandlers(false);	*/	
	}
	
	public static final Logger getInstance(int pid) {
		return LOGGERS.computeIfAbsent(pid, key -> {
			Logger logger = Logger.getLogger("org.distribooty.pid=" + key);
			logger.setLevel(defaultLevel);
			logger.setUseParentHandlers(false);
			logger.addHandler(HANDLER);
			return logger;
		});
		//return LOGGER;
	}

	public RemoteLogger() {
	}
	
	public static <T> String getPrintableList(Collection<T> items) {
		StringBuilder strBuild = new StringBuilder();
		boolean firstItemWritten = false;
		for(T item : items)
			if(firstItemWritten)
				strBuild.append(',').append(item.toString());
			else
			{
				strBuild.append(item.toString());
				firstItemWritten = true;
			}
		return strBuild.toString();
	}
	
	public static <T> String getPrintableList(@SuppressWarnings("unchecked") T... items) {
		StringBuilder strBuild = new StringBuilder();
		boolean firstItemWritten = false;
		for(T item : items)
			if(firstItemWritten)
				strBuild.append(',').append(item.toString());
			else
			{
				strBuild.append(item.toString());
				firstItemWritten = true;
			}
		return strBuild.toString();
	}
	
	public static StackTraceElement getStackTraceElement(int i) {
		return Thread.currentThread().getStackTrace()[i+1];
	}

}
