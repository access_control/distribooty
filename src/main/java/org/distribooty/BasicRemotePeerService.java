package org.distribooty;

import org.distribooty.lock.BasicPrivilegedTaskService;

public interface BasicRemotePeerService extends BasicPrivilegedTaskService, InitRemotePeerService {	
	void startElection();
}
