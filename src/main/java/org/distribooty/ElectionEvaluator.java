package org.distribooty;

import java.io.Serializable;

public interface ElectionEvaluator<E extends Serializable & Comparable<E>> {
	void onEvaluableObjectRetrieved(int srcPid, E evaluableObject);
	int getLeader();
	void onElectionFailed(int failedPid);
}
