package org.distribooty;

/**
 * Should be implemented by every component. It is used if the component
 * is elected as slave.
 */
public interface MasterChangeListener {
	/**
	 * Called if the master is lost
	 * @param masterPid the last pid of master as master was available
	 */
	void onMasterLost(int masterPid);
	
	/**
	 * Called if the master is changed
	 * @param masterPid the pid of new master
	 */
	void onMasterChange(int masterPid);	
	
}
