package org.distribooty;

import java.util.Map;
import java.util.Set;

import org.distribooty.connector.RemoteFailureAwareContext;
import org.distribooty.connector.RemoteObjectContext;
import org.distribooty.lock.PrivilegedTaskService;

public interface LocalPeerService extends BasicRemotePeerService, PrivilegedTaskService {
	int getMasterPid();
	RemoteObjectContext retrieveRemoteObjectContext(int pid);
	Map<Integer, RemoteObjectContext> retrieveRemoteObjectContexts(Set<Integer> pids);
	RemoteFailureAwareContext getRemoteFailureAwareContext();
	void startReelection();
}
