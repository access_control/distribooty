package org.distribooty;

public enum SlaveStatusHint {
	MASTER_SAME_STATE, MASTER_DIFFERENT_STATE, MASTER_INSTALLED;
}
