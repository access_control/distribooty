package org.distribooty;

import java.nio.file.Path;
import java.util.Map;
import java.util.logging.Level;

import org.distribooty.lock.centralized.CSimpleLockGraphVertexKeeperMode;

public interface RemotePeerConnectorConfiguration {
	int getPid();
	String getShutdownPassword();
	RemotePeerContact[] getContacts();
	Map<Integer, RemotePeerContact> getMappedRemotePeerContacts();
	Path getPersistenceFile();
	RMISecuritySpecs getRmiSecuritySpecs();
	boolean isStartElectionAutomatically();
	boolean isQuorumAware();
	CSimpleLockGraphVertexKeeperMode getMode();
	long getHeartbeatPeriod();
	long getNeighbourDiscoveryPeriod();
	long getCleanerPeriod();
	long getDLockWorkerLiveTime();
	long getDLockWorkerTimeout();
	long getCSimpleLockKeeperLiveTime();
	Level getLoggingLevel();
}