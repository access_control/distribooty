package org.distribooty;

public interface MasterListener {	
	void onMasterBeforeInCharge();
	void onMasterAfterInCharge();
	void onMasterBeforeRetirement();
	void onMasterAfterRetirement();
}
