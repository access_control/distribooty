package org.distribooty;

import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.distribooty.connector.RemoteDiscoverable;
import org.distribooty.connector.RemoteObjectContext;
import org.distribooty.debug.RemoteLogger;

public class DefaultRemotePeerHandler implements LocalPeerHandler<DefaultRemoteEvaluable, DefaultHeartbeatData> {

	protected Logger logger;
	private long term;
	private long value;
	
	public DefaultRemotePeerHandler(long value, int pid) {
		this.value = value;
		this.logger = RemoteLogger.getInstance(pid);
	}

	@Override
	public void onMasterLost(int masterPid) {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + "(" + masterPid + ")");		
	}

	@Override
	public void onMasterChange(int masterPid) {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + "(" + masterPid + ")");		
	}

	@Override
	public void onRemotePeerFail(Set<Integer> unavailablePids) {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"(" + RemoteLogger.getPrintableList(unavailablePids) + ")");
	}

	@Override
	public void onNeighbourDiscoveryChange(Map<Integer, RemotePeerDescriptor> retrievedPeers,
			BasicRemotePeerService service) {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"(" + RemoteLogger.getPrintableList(retrievedPeers.keySet()) + ")");
	}

	@Override
	public void onRegister(int srcPid, RemoteDiscoverable discoverable) {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"(" + srcPid + ")");
	}

	@Override
	public void onUnregister(int srcPid) {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"(" + srcPid + ")");		
	}

	@Override
	public RemoteObjectContext getRemoteObjectContext() {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"()");
		return null;
	}

	@Override
	public DefaultRemoteEvaluable getEvaluableObject() {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"(" + "" + ")");
		return new DefaultRemoteEvaluable(term, value);
	}

	@Override
	public void onElectionEnd(int masterPid, long term, SlaveStatusHint hint, LocalPeerService localPeerService) {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"(" + masterPid + ", " + term + ", " + hint.name() + ")");
	}

	@Override
	public void onElectionNonNeccesary(int masterPid, long term, boolean thisPeerMaster, LocalPeerService localPeerService) {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"(" + masterPid + ", " + term + ", " + thisPeerMaster + ")");
	}

	@Override
	public void onElectionFailure(int srcPid, Throwable reason) {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"(" + srcPid + ")");
		logger.log(Level.INFO, "", reason);
	}

	@Override
	public void applyHeartbeatData(int srcPid, DefaultHeartbeatData heartbeatData) {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"(" + srcPid + ")");
	}

	@Override
	public DefaultHeartbeatData getHeartbeatData() {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"(" + "" + ")");
		return null;
	}

	@Override
	public void onInit(StartupMode startupMode, InitRemotePeerService initRemotePeerService) {
		this.term = initRemotePeerService.getTerm();
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"(" + startupMode.name() + ", " + term + ")");
	}

	@Override
	public void onShutdown() {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"(" + "" + ")");		
	}

	@Override
	public void onRemotePeerShutdown(int srcPid) {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + 
				"(" + srcPid + ")");		
	}

	@Override
	public void onMasterBeforeInCharge() {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + "()");
	
	}

	@Override
	public void onMasterAfterInCharge() {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + "()");
	}

	@Override
	public void onMasterBeforeRetirement() {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + "()");		
	}

	@Override
	public void onMasterAfterRetirement() {
		StackTraceElement ste = RemoteLogger.getStackTraceElement(1);
		logger.log(Level.INFO, "calling " + ste.getClassName() + "." + ste.getMethodName() + "()");		
	}
}
