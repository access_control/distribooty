package org.distribooty;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteIdentifiable extends Remote {
	int getPid() throws RemoteException;
	RemoteState getRemoteState() throws RemoteException;
	long getCurrentTimestamp() throws RemoteException;
}
