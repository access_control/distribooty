package org.distribooty;

import java.util.Map;

import org.distribooty.connector.RemotePeerConnectorExporter;
import org.distribooty.util.Shutdownable;

public interface InitRemotePeerService extends Shutdownable {
	Map<Integer, RemotePeerContact> getRemoteContacts();
	int getLocalPid();
	RemoteState getLocalRemoteState();
	long getTerm();
	RemotePeerConnectorExporter getRemotePeerConnectorExporter();
	RemotePeerConnectorConfiguration getRemotePeerConnectorConfiguration();
}
