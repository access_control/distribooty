package org.distribooty;

import java.io.Serializable;

public interface LocalHeartbeatAware<H extends Serializable> {
	void applyHeartbeatData(int srcPid, H heartbeatData);
	H getHeartbeatData();
}
