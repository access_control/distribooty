package org.distribooty;

import org.distribooty.connector.RemoteObjectContext;

public abstract class RemotePeerDescriptor {
	
	private final int pid;
	private final RemoteState remoteState;
	
	public RemotePeerDescriptor(int pid, RemoteState remoteState) {
		this.pid = pid;
		this.remoteState = remoteState;
	}

	public int getPid() {
		return pid;
	}

	public RemoteState getRemoteState() {
		return remoteState;
	}

	public abstract RemoteObjectContext getRemoteObjectContext();
}
