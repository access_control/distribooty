package org.distribooty;

import java.io.Serializable;

public class RemoteInfo implements Serializable {

	private static final long serialVersionUID = 2742961677307158022L;

	private final int pid;
	private final RemoteState remoteState;
	private final boolean master;
	private final long currentTimestamp;
	private final long lastSeenTerm;
	
	public RemoteInfo(int pid, RemoteState remoteState, boolean master, long currentTimestamp, long lastSeenTerm) {
		this.pid = pid;
		this.remoteState = remoteState;
		this.master = master;
		this.currentTimestamp = currentTimestamp;
		this.lastSeenTerm = lastSeenTerm;
	}
	
	public int getPid() {
		return pid;
	}
	
	public RemoteState getRemoteState() {
		return remoteState;
	}
	
	public boolean isMaster() {
		return master;
	}
	
	public long getCurrentTimestamp() {
		return currentTimestamp;
	}
	
	public long getLastSeenTerm() {
		return lastSeenTerm;
	}	
}
