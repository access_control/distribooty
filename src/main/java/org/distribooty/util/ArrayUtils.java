package org.distribooty.util;

import java.util.Collection;

public class ArrayUtils {

	public static <T> void checkArrayForEmptiness(T[] a) throws NullPointerException, IllegalArgumentException {
		if(a == null)
			throw new NullPointerException("The provided array is null");
		if(a.length == 0)
			throw new IllegalArgumentException("The provided array is empty");
		for(T t : a)
			if(t == null)
				throw new NullPointerException("The provided array contains null pointers");
	}
	
	public static <T> boolean isArrayEmpty(T[] a) {
		return a == null || a.length == 0;
	}
	
	public static boolean isCollectionEmpty(Collection<?> c) {
		return c == null || c.isEmpty();
	}

}
