package org.distribooty.util;

import java.io.IOException;
import java.net.Socket;
import java.nio.file.Path;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManagerFactory;
import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;


public class RmiSslHelper {
	
	private static SSLContext sslContext;

	private static String[] splitStrings(String str, String splitPattern) {
		List<String> splittedStrings = new ArrayList<>();
		for(String s : str.split(splitPattern))
			splittedStrings.add(s.trim());
		
		return splittedStrings.toArray(new String[0]);
	}
	
	private static SecureRandom getSecureRandom() {
		final int length = 512 / 8;
		byte seed[] = new byte[length];
		for(int i = 0; i < length; i++)
			seed[i] = (byte) System.nanoTime();
		
		return new SecureRandom(seed);
	}
	
	private static SSLContext getSSLContext(Path keystorePath, 
			String keystorePassword,
			String sslProtocol) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableKeyException, KeyManagementException {
		
		char password[] = keystorePassword.toCharArray();
		KeyStore ks = KeyStore.getInstance(keystorePath.toFile(), password);
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		kmf.init(ks, password);
		tmf.init(ks);
		SSLContext sslContext = SSLContext.getInstance(sslProtocol);
		sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), getSecureRandom());
		
		return sslContext;
	}
		
	public static SslRMIServerSocketFactory getSslRMIServerSocketFactory(Path keystorePath, 
			String keystorePassword,
			String enabledCipherSuites,
			String enabledProtocols,
			boolean needClientAuth) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableKeyException, KeyManagementException {
		
		String enabledProtocolList[] = splitStrings(enabledProtocols, ",");
		String enabledCipherSuiteList[] = splitStrings(enabledCipherSuites, ",");
		SSLContext sslContext = getSSLContext(keystorePath, keystorePassword, enabledProtocolList[0]);
		
		return new SslRMIServerSocketFactory(sslContext, enabledCipherSuiteList, enabledProtocolList, needClientAuth);
	}
	
	private static class SpecialSslRMIClientSocketFactory extends SslRMIClientSocketFactory {
		
		private static final long serialVersionUID = 4138952454732841067L;
		private String[] enabledProtocols;
		private String[] enabledCipherSuits;	

		public SpecialSslRMIClientSocketFactory(String[] enabledProtocols,
				String[] enabledCipherSuits) {
			this.enabledProtocols = enabledProtocols;
			this.enabledCipherSuits = enabledCipherSuits;
		}

		@Override
		public Socket createSocket(String host, int port) throws IOException {
			SSLSocket sslSocket = (SSLSocket) sslContext.getSocketFactory().createSocket(host, port);
			sslSocket.setEnabledCipherSuites(enabledCipherSuits);
			sslSocket.setEnabledProtocols(enabledProtocols);
			sslSocket.setUseClientMode(true);
			return sslSocket;
		}	
		
	}
	
	public static SslRMIClientSocketFactory getSslRMIClientSocketFactory(Path keystorePath, 
			String keystorePassword,
			String enabledCipherSuites,
			String enabledProtocols) throws UnrecoverableKeyException, KeyManagementException, KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		
		String enabledProtocolList[] = splitStrings(enabledProtocols, ",");
		String enabledCipherSuiteList[] = splitStrings(enabledCipherSuites, ",");
		SSLContext sslContext = getSSLContext(keystorePath, keystorePassword, enabledProtocolList[0]);
		RmiSslHelper.sslContext = sslContext;
		
		return new SpecialSslRMIClientSocketFactory(enabledProtocolList, enabledCipherSuiteList);
	}

}
