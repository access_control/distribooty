package org.distribooty.util;

public class Shutdowner implements Shutdownable {

	private Shutdownable[] shutdownableObjects;

	public Shutdowner(Shutdownable ... shutdownableObjects) {
		this.shutdownableObjects = shutdownableObjects;
	}

	@Override
	public void shutdown(boolean graceful) {
		for(Shutdownable s : shutdownableObjects)
			try {
				s.shutdown(true);
			} catch (NullPointerException e) {
				
			}
		
	}

	@Override
	public void waitForShutdownComplete() {
		for(Shutdownable s : shutdownableObjects)
			try {
				s.waitForShutdownComplete();
			} catch (NullPointerException e) {
				
			}		
	}
	

}
