package org.distribooty.util;

public interface ParallelRemoteComponentHandlerErrorReceiver {
	void reportFailure(int pid);
}
