package org.distribooty.util;

public abstract class AbstractWorker extends AbstractRunnableRoutine {

	public enum AwaitStatus {
		NOTIFIED, TIMEOUTED, INTERRUPTED;
	}
	
	protected Object lock = new Object();
	private boolean notified = false;
	private long timeout = 0L;
	protected boolean shutdownFlag = false;

	public AbstractWorker(boolean notified) {
		this.notified = notified;
	}
	
	public AbstractWorker(boolean notified, String name) {
		this(notified);
		this.name = name;
	}
	
	public long getTimeout() {
		synchronized(lock)
		{
			return timeout;
		}
	}

	public void setTimeout(long timeout) {
		synchronized(lock)
		{
			this.timeout = timeout;
		}
	}

	/**
	 * notifies this worker.
	 */
	protected void wakeUp() {
		synchronized(lock)
		{
			notified = true;
			lock.notify();
		}
	}
	
	/**
	 * notifies all workers
	 */
	protected void wakeUpAll() {
		synchronized(lock)
		{
			notified = true;
			lock.notifyAll();
		}
	}
	
	/**
	 * 
	 * @param timeout if larger then 0, wait until what happens first: interrupted, notified or timeout elapsed. If 0 then timeout never elapsed.
	 * @return 
	 */
	protected AwaitStatus await(long timeout) {
		long t, offset = 0L;
		// Negative timeout is not allowed
		if(timeout < 0L)
			timeout = 0L;
		AwaitStatus returnStatus = null;
		synchronized(lock)
		{
			t = System.currentTimeMillis();
			while(!notified)
				try 
				{
					lock.wait(timeout - offset);
					// if suspicious wakeup, ignore it and wait the remaining time if timemout aware or until notified, what happens first.
					if(!notified && timeout > 0L && (offset = System.currentTimeMillis() - t) >= timeout)
						{
							returnStatus = AwaitStatus.TIMEOUTED;
							break;
						}
				} catch (InterruptedException e) {
					// if interrupted
					returnStatus = AwaitStatus.INTERRUPTED;
					break;
				}
			// if either interrupted nor timeouted
			if(returnStatus == null)				
				returnStatus = AwaitStatus.NOTIFIED;				
			notified = false;
			return returnStatus;
		}
	}
	
	protected AwaitStatus await() {
		return await(0L);
	}
	
	@Override
	public void shutdown(boolean graceful) {
		synchronized(lock)
		{
			shutdownFlag = true;
			wakeUp();
		}
	}
	
}
