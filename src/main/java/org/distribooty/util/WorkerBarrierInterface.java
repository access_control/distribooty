package org.distribooty.util;

public interface WorkerBarrierInterface {
	boolean waitForStart();
	void leave();
}