package org.distribooty.util;

import java.io.Serializable;
import java.util.Arrays;

public class AtomicUniversalCounter implements Comparable<AtomicUniversalCounter>, Serializable {

	private static final long serialVersionUID = 7022941316514130515L;

	private final static UniversalCounter ID = new UniversalCounter(128);
	
	private final UniversalCounter id;
	
	private byte[] countValue;
	
	public AtomicUniversalCounter(int numberOfBits) {
		if(numberOfBits <= 0 || numberOfBits % 8 != 0)
			throw new IllegalArgumentException("The provided number of bits must be positive integer which is multiple of 8");
		synchronized(ID)
		{
			id = new UniversalCounter(ID.getBitLength());
			id.set(ID.getCountValue());
			ID.changeBy((byte) 1);			
		}
		countValue = new byte[numberOfBits / 8];
		reset();
	}
	
	public synchronized void reset() {
		for(int i = 0; i < countValue.length; i++)
			countValue[i] = 0;
	}
	
	public synchronized void set(byte[] value) {
		int i, j;
		for(i = value.length - 1, j = countValue.length - 1; i >= 0 && j >= 0; i--, j--)
			countValue[j] = value[i];
		while(j >= 0)
			countValue[j--] = 0;
	}
	
	public synchronized boolean changeBy(byte val) {		
		int i;
		for(i = countValue.length - 1; i >= 0; i--)
		{
			int v = (int) countValue[i] & 0xFF + val;
			countValue[i] = (byte) (v & 0xFF);
			if(v > 0xFF)
				val = 1;
			else if(v < 0)
				val = -1;
			else
				break;
		}
		return i < 0;
	}
	
	
	
	public synchronized byte[] snapshot() {
		return countValue.clone();
	}
	
	public int getBitLength() {
		return countValue.length * 8;
	}
	
	private int compare(AtomicUniversalCounter o) {
		int i, cmp;
		for(i = cmp = 0; i < countValue.length && cmp == 0; i++)		
			cmp = Integer.compare(countValue[i] & 0xFF, o.countValue[i] & 0xFF);
		return cmp;
	}
	
	@Override
	public int compareTo(AtomicUniversalCounter o) {
		if(o.countValue.length != countValue.length)
			throw new IllegalArgumentException("The bit lengths of the universal counters does not match");
		if(id.compareTo(o.id) < 0)
		{
			synchronized(this)
			{
				synchronized(o)
				{
					return compare(o);
				}
			}
		}
		else
		{
			synchronized(o)
			{
				synchronized(this)
				{
					return compare(o);
				}
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(countValue);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtomicUniversalCounter other = (AtomicUniversalCounter) obj;
		if (!Arrays.equals(countValue, other.countValue))
			return false;
		return true;
	}

}
