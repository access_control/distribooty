package org.distribooty.util;

import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;

import org.distribooty.debug.RemoteLogger;

public abstract class AbstractRunnableRoutine implements Runnable, Shutdownable {
	
	protected int pid;
	protected String name;
	private String oldName;
	protected CountDownLatch countDownLatch = new CountDownLatch(1);
	private CountDownLatch guardForThreadEstablishing = new CountDownLatch(1);
	private Thread executingThread;

	public AbstractRunnableRoutine() {
		
	}
	
	public AbstractRunnableRoutine(String name) {
		this.name = name;
	}
	
	public void setPid(int pid) {
		this.pid = pid;
	}
	
	public void waitForThreadEstablishing() {
		ConcurrentUtils.waitUninterruptible(guardForThreadEstablishing);
	}
	
	public Thread getExecutingThread() {
		waitForThreadEstablishing();
		return executingThread;
	}

	@Override
	public void shutdown(boolean graceful) {
		countDownLatch.countDown();
	}

	@Override
	public void waitForShutdownComplete() {
		ConcurrentUtils.waitUninterruptible(countDownLatch);
	}
	
	private boolean isNameSet() {
		return name != null && !name.equals("");
	}

	protected void onRunEnter() {
		executingThread = Thread.currentThread();
		boolean nameSet = isNameSet();
		if(nameSet)
			{
				oldName = executingThread.getName();
				executingThread.setName(name);				
			}
		guardForThreadEstablishing.countDown();
		if(nameSet)
			RemoteLogger.getInstance(pid).log(Level.INFO, name + " started");
	}
	
	protected void onRunLeave() {
		countDownLatch.countDown();
		if(oldName != null)
			Thread.currentThread().setName(oldName);
	}
}
