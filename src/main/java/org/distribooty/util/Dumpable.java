package org.distribooty.util;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Dumpable extends Remote {
	void dump(String command) throws RemoteException;
}
