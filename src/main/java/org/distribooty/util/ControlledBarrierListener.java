package org.distribooty.util;

public interface ControlledBarrierListener {
	void onBegin();
	void onEnd();
}
