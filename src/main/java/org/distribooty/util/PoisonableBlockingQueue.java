package org.distribooty.util;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * 
 * This queue acts as usual queue, but as soon as it is poisoned, no further addition to the queue
 * possible and only poison element will be extracted as soon as the queue becomes empty after being poisoned.
 * 
 * @param <E> type of the element which can be queued to and dequeued from the provided queue.
 */
public class PoisonableBlockingQueue<E> {
	
	private final E POISON;
	private LinkedList<E> queue = new LinkedList<>();
	private volatile boolean poisoned = false;
	private volatile boolean alwaysReturnPoison = false;

	/**
	 * 
	 * @param poison element, which poisons this queue.
	 * @throws NullPointerException if the poison is null
	 */
		
	public PoisonableBlockingQueue(E poison) throws NullPointerException {
		Objects.requireNonNull(poison);
		POISON = poison;		
	}
	
	/**
	 * 
	 * @param e the element to add to queue
	 * @return true if the element has been successfully added to queue,
	 * false if the queue is already poisoned, so this means the element
	 * has not been added to queue at all.
	 * @throws NullPointerException if the provided element is null
	 */
	public boolean enqueue(E e) throws NullPointerException {
		Objects.requireNonNull(e);
		if(poisoned)
			return false;
		synchronized(this)
		{	
			if(!poisoned)
			{
				if(!poisoned && e.equals(POISON))					
						poisoned = true;					
				else if(!e.equals(POISON))
					queue.add(e);
				notify();
				return true;				
			}
			return false;
		}
		
	}
	
	/**
	 * 
	 * @return extracts the next head element from the queue, if it is not empty.
	 * If the queue is empty and not poisoned the method blocks until either a valid
	 * element at the head of the queue is available for extraction and this element
	 * will be returned or the queue is poisoned while waiting and the poison element
	 * will be returned.
	 * If the queue is empty and poisoned the poison element is returned.
	 */
	public E dequeue() {
		if(alwaysReturnPoison)
			return POISON;
		synchronized(this)
		{
			E e;
			while(true)
			{
				if(poisoned && queue.isEmpty())				
					{
						alwaysReturnPoison = true;
						e = POISON;
						notifyAll();
					}
				else
				{
					try {
						e = queue.remove();
						notify();
					} catch (NoSuchElementException e1) {
						try {
							wait();
						} catch (InterruptedException e2) {
						}
						continue;
					}
				}
				return e;				
			}
		}		
	}
	
	/**
	 * 
	 * @return the poison element which is able to poison this queue.
	 */
	public E getPoison() {
		return POISON;
	}
	
	/**
	 * 
	 * @return true if this queue is poisoned, else false.
	 */
	public boolean isPoisoned() {
		return poisoned;
	}
	
	/**
	 * 
	 * @return true if this queue is empty, else false.
	 */
	
	public boolean isEmpty() {
		synchronized (this) {
			return queue.isEmpty();			
		}
	}
	
	/**
	 * Poisons this queue. Equivalent to:
	 * 
	 * <code>enqueue(getPoison());</code>
	 */
	public void poison() {
		enqueue(getPoison());
	}
	
	/**
	 * 
	 * @param fun
	 */
	public synchronized void browse(Consumer<ListIterator<E>> fun) {
		if(!alwaysReturnPoison)
			fun.accept(queue.listIterator());
		else
			fun.accept(null);
		notify();
	}
}
