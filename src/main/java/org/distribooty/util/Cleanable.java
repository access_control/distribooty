package org.distribooty.util;

public interface Cleanable {
	/**
	 * The object is cleanable, if it is "idle".
	 * @return true if the object is cleanable, else false.
	 */
	boolean isCleanable();
	
	/**
	 * 
	 * @return
	 */
	boolean resetCleanableStatus();
	
	long getLiveTime();
}
