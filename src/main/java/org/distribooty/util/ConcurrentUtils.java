package org.distribooty.util;

import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class ConcurrentUtils {
	
	public static long computeMillis(long time, TimeUnit unit) {
		if(unit != null)
		{
			boolean mult = true;
			long factor;
			switch(unit) 
			{
				case DAYS:
					factor = 24 * 60 * 60 * 1000;
					break;
				case HOURS:
					factor = 60 * 60 * 1000;
					break;
				case MINUTES:
					factor = 60 * 1000;
					break;
				case SECONDS:
					factor = 1000;
					break;
				case MILLISECONDS:
					factor = 1;
					break;
				case MICROSECONDS:
					factor = 1000;
					mult = false;
					break;
				case NANOSECONDS:
					factor = 1000 * 1000;
					mult = false;
					break;
				default:
					factor = 1;
			}
			if(mult)
				time *= factor;
			else
				time /= factor;
		}
		
		return time;
	}
	
	public static void waitUninterruptible(CountDownLatch countDownLatch) {
		while(true)
			try {
				countDownLatch.await();
				break;
			} catch (InterruptedException e) {
				
			}
	}
	
	public static void waitUninterruptible(ExecutorService executorService, long timeout, TimeUnit timeUnit) {
		while(true)
			try {
				if(executorService.awaitTermination(timeout, timeUnit))
					break;
			} catch (InterruptedException e) {
				
			}
	}
	
	public static void waitUninterruptible(Map<?, ?> container) {
		synchronized(container)
		{
			while(!container.isEmpty())
				try {
					container.wait();
				} catch (InterruptedException e) {
					
				}
		}
	}
}
