package org.distribooty.util;

public class SequencialShutdowner {
	
	private Shutdownable[] shutdownableObjects;

	public SequencialShutdowner(Shutdownable ...shutdownableObjects) {
		this.shutdownableObjects = shutdownableObjects;
	}
	
	public void shutdownBlocking() {
		for(Shutdownable s : shutdownableObjects)
			if(s != null)
			{
				s.shutdown(true);
				s.waitForShutdownComplete();
			}
	}

}
