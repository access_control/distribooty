package org.distribooty.util;

public interface Shutdownable {
	void shutdown(boolean graceful);
	void waitForShutdownComplete();
}
