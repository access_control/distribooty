package org.distribooty.util;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteShutdownable extends Remote {
	void shutdown(String shutdownPassword) throws RemoteException;
}
