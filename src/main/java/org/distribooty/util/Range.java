package org.distribooty.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

/**
 * 
 * This class is helpful for maintaining ranges, which could be helpful for the marking of the
 * bounds of some complex data.
 *
 * @param <T> represents the type of the bound which limits the range. Should be immutable.
 */

public class Range<T extends Serializable & Comparable<T>> implements Serializable, Comparable<Range<T>>, Cloneable {
	
	private static final long serialVersionUID = 4322730915774990594L;
	
	private static final int BOTH_NULL = 0, FIRST = 1, SECOND = 2, EQUAL = 3;
	
	/**
	 * The lower and upper bounds. The null for lower bound means "minus infinity" and for upper bound means "plus infinity".
	 */
	private final T lowerBound, upperBound;
	private final boolean lowerBoundInclusive, upperBoundInclusive;
	
	/**
	 * Creates the new range with specified lower and upper bounds, which limits the range.
	 * @param lowerBound lower bound, which limits the range "from bottom". If null, the limit means the "minus infinity".
	 * @param lowerBoundInclusive true means the lowerBound is inclusive in the range, false exclusive. If lowerBound == null or lowerBound == upperBound, value of this parameter is forced to true.
	 * @param upperBound upper bound, which limits the range "from top". If null, the limit means the "plus infinity".
	 * @param upperBoundInclusive true means the upperBound is inclusive in the range, false exclusive. If upperBound == null or upperBound == lowerBound, value of this parameter is forced to true.
	 * @throws IllegalArgumentException if lowerBound is "larger" then upperBound, both being not null.
	 */
	public Range(T lowerBound, boolean lowerBoundInclusive, T upperBound, boolean upperBoundInclusive) throws IllegalArgumentException {
		if(lowerBound != null && upperBound != null && lowerBound.compareTo(upperBound) > 0)
			throw new IllegalArgumentException("lowerBound " + lowerBound.toString() + " is \"larger\" then " + upperBound.toString());
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		if(lowerBound == null || lowerBound.equals(upperBound))
			this.lowerBoundInclusive = true;
		else
			this.lowerBoundInclusive = lowerBoundInclusive;
		if(upperBound == null || upperBound.equals(lowerBound))
			this.upperBoundInclusive = true;
		else
			this.upperBoundInclusive = upperBoundInclusive;
	}

	
	/**
	 * The same as Range(lowerBound, true, upperBound, true)
	 * @param lowerBound
	 * @param upperBound
	 */
	public Range(T lowerBound, T upperBound) {
		this(lowerBound, true, upperBound, true);
	}

	/**
	 * The same as Range(bound, bound).
	 * @param bound lower and upper bound.
	 */
	public Range(T bound) {
		this(bound, bound);
	}
	
	/**
	 * The same as Range(lowerBound, lowerBoundInclusive, upperBound, true);
	 * @param lowerBound
	 * @param lowerBoundInclusive
	 * @param upperBound
	 */
	
	public Range(T lowerBound, boolean lowerBoundInclusive, T upperBound) {
		this(lowerBound, lowerBoundInclusive, upperBound, true);
	}
	
	
	/**
	 * The same as Range(lowerBound, true, upperBound, upperBoundInclusive)
	 * @param lowerBound
	 * @param upperBound
	 * @param upperBoundInclusive
	 */
	public Range(T lowerBound, T upperBound, boolean upperBoundInclusive) {
		this(lowerBound, true, upperBound, upperBoundInclusive);
	}
	
	/**
	 * @return the lower bound
	 */
	public T getLowerBound() {
		return lowerBound;
	}

	/**
	 * @return the upper bound
	 */
	public T getUpperBound() {
		return upperBound;
	}
	
	/**
	 * 
	 * @return either the lower bound is inclusive
	 */
	
	public boolean isLowerBoundInclusive() {
		return lowerBoundInclusive;
	}


	/**
	 * 
	 * @return either the upper bound is inclusive
	 */
	public boolean isUpperBoundInclusive() {
		return upperBoundInclusive;
	}


	/**
	 * @return 0 if this range overlaps with provided range.
	 * -1 if this range ordered before the provided range.
	 * +1 if this range ordered after the provided range.
	 * In both last cases it is meant that the this range and the provided one are not overlapping.
	 */
	@Override
	public int compareTo(Range<T> o) {
		Objects.requireNonNull(o);
		int cmp;
		if(upperBound != null && o.lowerBound != null && ((cmp = upperBound.compareTo(o.lowerBound)) < 0 || cmp == 0 && !(upperBoundInclusive && o.lowerBoundInclusive)))
			return -1;
		else if(lowerBound != null && o.upperBound != null && ((cmp = lowerBound.compareTo(o.upperBound)) > 0 || cmp == 0 && !(lowerBoundInclusive && o.upperBoundInclusive)))
			return 1;
		else
			return 0;
	}
	
	/**
	 * 
	 * @param bound1 the bound to compare
	 * @param bound2 the bound to compare
	 * @param lowerBounds only has a meaning if either bound1 or bound2 is null. If lowerBounds is true, the null is treated as "-infinity", else "+infinity"
	 * @return 0 means (bound1 == null && bound2 == null), 
	 * 1 means (bound1 < bound2), 
	 * 2 means (bound1 > bound2), 
	 * 3 means (bound1 == bound2 && bound1 != null && bound2 != null)
	 */
	
	private static <T extends Comparable<T>> int minDecide(T bound1, T bound2, boolean lowerBounds) {
		if(bound1 != null && bound2 != null) // bound1 and bound2 both not "infinity"
		{
			int cmp = bound1.compareTo(bound2);
			if(cmp < 0)
				return FIRST;
			else if(cmp > 0)
				return SECOND;
			else // if(cmp == 0)
				return EQUAL;
		}
		if(bound1 == null && bound2 == null) // bound1 and bound2 both "infinity"
			return BOTH_NULL;
		
		// implies also if(bound1 == null ^ bound2 == null)
		if(lowerBounds && bound1 == null || !lowerBounds && bound1 != null)
			return FIRST;
		else
			return SECOND;
	}
	
	/**
	 * 
	 * @param bound1 the bound to compare
	 * @param bound2 the bound to compare
	 * @param upperBounds only has a meaning if either bound1 or bound2 is null. If upperBounds is true, the null is treated as "+infinity", else "-infinity"
	 * @return 0 means (bound1 == null && bound2 == null), 
	 * 1 means (bound1 > bound2), 
	 * 2 means (bound1 < bound2), 
	 * 3 means (bound1 == bound2 && bound1 != null && bound2 != null)
	 */
	private static <T extends Comparable<T>> int maxDecide(T bound1, T bound2, boolean upperBounds) {
		if(bound1 != null && bound2 != null) // bound1 and bound2 both not "infinity"
		{
			int cmp = bound1.compareTo(bound2);
			if(cmp < 0)
				return SECOND;
			else if(cmp > 0)
				return FIRST;
			else // if(cmp == 0)
				return EQUAL;
		}
		if(bound1 == null && bound2 == null) // bound1 and bound2 both "infinity"
			return BOTH_NULL;
		
		// implies also if(bound1 == null ^ bound2 == null)
		if(upperBounds && bound1 == null || !upperBounds && bound1 != null)
			return FIRST;
		else
			return SECOND;
	}
	
	/**
	 * 
	 * @param status see {@link #minDecide(Comparable, Comparable, boolean)} and {@link #maxDecide(Comparable, Comparable, boolean)}
	 * @param inclusionStatus1
	 * @param inclusionStatus2
	 * @param intersect means only if status == 3.
	 * @return
	 */
	private static boolean inclusionDecide(int status, boolean inclusionStatus1, boolean inclusionStatus2, boolean intersect) {
		
		switch(status)
		{
			case BOTH_NULL:
				return true;
			case FIRST:
				return inclusionStatus1;
			case SECOND:
				return inclusionStatus2;
			case EQUAL:
				if(intersect)
					return inclusionStatus1 & inclusionStatus2;
				else
					return inclusionStatus1 | inclusionStatus2;
			default:
				throw new IllegalArgumentException("status must be one of {0, 1, 2, 3}");
		}
	}
	
	/**
	 * 
	 * @param status see {@link #minDecide(Comparable, Comparable, boolean)} and {@link #maxDecide(Comparable, Comparable, boolean)}
	 * @param bound1
	 * @param bound2
	 * @return
	 */
	private static <T> T boundDecide(int status, T bound1, T bound2) {
		switch(status)
		{
			case BOTH_NULL:
			case FIRST:
			case EQUAL:
				return bound1;
			case SECOND:
				return bound2;
			default:
				throw new IllegalArgumentException("status must be one of {0, 1, 2, 3}");
		}
	}
	
	/**
	 * 
	 * @param range
	 * @return the range representing the range between this range and the provided range or null if the ranges are not overlapping.
	 * @throws NullPointerException if range is null
	 */
	public Range<T> getOverlappingRange(Range<T> range) throws NullPointerException {
		Objects.requireNonNull(range);
		if(compareTo(range) == 0) // overlapping
		{
			int lb = maxDecide(lowerBound, range.lowerBound, false);
			int ub = minDecide(upperBound, range.upperBound, false);
			return new Range<T>(boundDecide(lb, lowerBound, range.lowerBound), 
					inclusionDecide(lb, lowerBoundInclusive, range.lowerBoundInclusive, true),
					boundDecide(ub, upperBound, range.upperBound),
					inclusionDecide(ub, upperBoundInclusive, range.upperBoundInclusive, true));
		}
		else
			return null;
	}
	
	/**
	 * 
	 * @param range
	 * @return the range representing the gap between this range and the provided range or null if the ranges are overlapping.
	 * @throws NullPointerException if range is null
	 */
	public Range<T> getGapRange(Range<T> range) throws NullPointerException {
		Objects.requireNonNull(range);
		int cmp = compareTo(range);
		if(cmp != 0 && !isContinuousWith(range)) // not overlapping
			return cmp < 0 
					? new Range<T>(upperBound, !upperBoundInclusive, range.lowerBound, !range.lowerBoundInclusive) 
					: new Range<T>(range.upperBound, !range.upperBoundInclusive, lowerBound, !lowerBoundInclusive);		
		else
			return null;
	}
	
	/**
	 * 
	 * @param range
	 * @return true if this range contains completely the provided range.
	 */
	public boolean containsRange(Range<T> range) {
		// Some boiler code but heap is not used
		Objects.requireNonNull(range);
		if(compareTo(range) == 0) // overlapping
		{
			int lbStat = maxDecide(lowerBound, range.lowerBound, false);
			int ubStat = minDecide(upperBound, range.upperBound, false);
			T lbOver = boundDecide(lbStat, lowerBound, range.lowerBound);
			boolean lbOverInc = inclusionDecide(lbStat, lowerBoundInclusive, range.lowerBoundInclusive, true);
			T ubOver = boundDecide(ubStat, upperBound, range.upperBound);
			boolean ubOverInc = inclusionDecide(ubStat, upperBoundInclusive, range.upperBoundInclusive, true);
			int _lbStat, _ubStat;
			if((_lbStat = maxDecide(lowerBound, lbOver, false)) == FIRST || _lbStat == EQUAL && !lowerBoundInclusive && lbOverInc ||
			   (_ubStat = minDecide(upperBound, ubOver, false)) == FIRST || _ubStat == EQUAL && !upperBoundInclusive && ubOverInc)
				return false;
			else
				return true;
		}
		else
			return false;		
	}
	
	/**
	 * 
	 * @param value
	 * @return true if value in the defined range, else false.
	 */
	public boolean containsValue(T value) {
		Objects.requireNonNull(value);
		if(		lowerBound == null && upperBound == null || 
				lowerBound == null && (upperBoundInclusive && upperBound.compareTo(value) >= 0 || 
										!upperBoundInclusive && upperBound.compareTo(value) > 0) || 
				upperBound == null && (lowerBoundInclusive && lowerBound.compareTo(value) <= 0 || 
										!lowerBoundInclusive && lowerBound.compareTo(value) < 0) ||
				
				(lowerBoundInclusive && lowerBound.compareTo(value) <= 0 || !lowerBoundInclusive && lowerBound.compareTo(value) < 0) 
				&& 
				(upperBoundInclusive && upperBound.compareTo(value) >= 0 ||	!upperBoundInclusive && upperBound.compareTo(value) > 0))
			return true;
		else
			return false;
	}
	
	/**
	 * 
	 * @param range
	 * @return true if this range is continuous with the provided range.
	 */
	public boolean isContinuousWith(Range<T> range) {
		// Some boiler code but heap not used
		Objects.requireNonNull(range);
		int cmp = compareTo(range);
		if(cmp != 0) // not overlapping
		{
			T lb, ub;
			boolean lbInc, ubInc;
			if(cmp < 0)
			{
				lb = upperBound;
				lbInc = upperBoundInclusive;
				ub = range.lowerBound;
				ubInc = range.lowerBoundInclusive;
			}
			else // if(cmp > 0)
			{
				lb = range.upperBound;
				lbInc = range.upperBoundInclusive;
				ub = lowerBound;
				ubInc = lowerBoundInclusive;
			}
			if(lb.compareTo(ub) == 0 && lbInc ^ ubInc)
				return true;
			else
				return false;
		}
		else
			return false;
	}
	
	/**
	 * Returns merged range which is consisted of the this range and the provided range if it is possible, else null.
	 * The merge is possible, if there is an overlapping range between this range and provided range or if this merge
	 * is continuous with the provided range, or mergeGap is true.
	 * @param range
	 * @param mergeGap
	 * @return merged range, or null if merge failed.
	 */
	public Range<T> merge(Range<T> range, boolean mergeGap) {
		Objects.requireNonNull(range);
		int cmp = compareTo(range);
		if(cmp == 0)
		{
			int lbStat = minDecide(lowerBound, range.lowerBound, true);
			int ubStat = maxDecide(upperBound, range.upperBound, true);
			return new Range<T>(boundDecide(lbStat, lowerBound, range.lowerBound),
					inclusionDecide(lbStat, lowerBoundInclusive, range.lowerBoundInclusive, false),
					boundDecide(ubStat, upperBound, range.upperBound),
					inclusionDecide(ubStat, upperBoundInclusive, range.upperBoundInclusive, false));			
		}
		else if(isContinuousWith(range) || mergeGap)
		{
			if(cmp < 0)			
				return new Range<T>(lowerBound, lowerBoundInclusive, range.upperBound, range.upperBoundInclusive);			
			else // if cmp > 0			
				return new Range<T>(range.lowerBound, range.lowerBoundInclusive, upperBound, upperBoundInclusive);			
		}
		else 
			return null;
	}
	
	/**
	 * 
	 * @param cutRange
	 * @return ranges, which after cutting, or null if this range is contained in the cutRange.
	 */
	
	@SuppressWarnings("unchecked")
	public Range<T>[] cut(Range<T> cutRange) {
		Objects.requireNonNull(cutRange);
		// Some boiler code but no heap is used
		if(compareTo(cutRange) == 0)
		{
			// Compute overlapping begin
			int lbStat = maxDecide(lowerBound, cutRange.lowerBound, false);
			int ubStat = minDecide(upperBound, cutRange.upperBound, false);
			T lb = boundDecide(lbStat, lowerBound, cutRange.lowerBound);
			boolean lbInc = inclusionDecide(lbStat, lowerBoundInclusive, cutRange.lowerBoundInclusive, true);
			T ub = boundDecide(ubStat, upperBound, cutRange.upperBound);
			boolean ubInc = inclusionDecide(ubStat, upperBoundInclusive, cutRange.upperBoundInclusive, true);
			// Compute overlapping end
			
			Range<T> r1 = null, r2 = null;
			int cmp, l, i;
			cmp = minDecide(lowerBound, lb, true);
			if(cmp == FIRST || cmp == EQUAL && lowerBoundInclusive && !lbInc)
				r1 = new Range<T>(lowerBound, lowerBoundInclusive, lb, !lbInc);
			cmp = maxDecide(upperBound, ub, true);
			if(cmp == FIRST || cmp == EQUAL && upperBoundInclusive && !ubInc)
				r2 = new Range<T>(ub, !ubInc, upperBound, upperBoundInclusive);
			l = r1 != null && r2 != null ? 2 : r1 != null || r2 != null ? 1 : 0;
			if(l == 0)
				return null;
			@SuppressWarnings("rawtypes")
			Range[] ranges = new Range[l];
			i = 0;
			if(r1 != null)
				ranges[i++] = r1;
			if(r2 != null)
				ranges[i++] = r2;
			return ranges;
		}
		else
			return new Range[] {this};
	}
	
	/**
	 * @return the clone of this range, but the lower and upper bound objects are not cloned.
	 */
	@SuppressWarnings("unchecked")
	public Range<T> clone() {
		try {
			return (Range<T>) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new IllegalStateException(e);
		}
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lowerBound == null) ? 0 : lowerBound.hashCode());
		result = prime * result + (lowerBoundInclusive ? 1231 : 1237);
		result = prime * result + ((upperBound == null) ? 0 : upperBound.hashCode());
		result = prime * result + (upperBoundInclusive ? 1231 : 1237);
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		Range<T> other = (Range<T>) obj;
		if (lowerBound == null) {
			if (other.lowerBound != null)
				return false;
		} else if (!lowerBound.equals(other.lowerBound))
			return false;
		if (lowerBoundInclusive != other.lowerBoundInclusive)
			return false;
		if (upperBound == null) {
			if (other.upperBound != null)
				return false;
		} else if (!upperBound.equals(other.upperBound))
			return false;
		if (upperBoundInclusive != other.upperBoundInclusive)
			return false;
		return true;
	}
	
	public static <T extends Serializable & Comparable<T>> Range<T>[] computeRanges(T[] values) {
		try {
			ArrayUtils.checkArrayForEmptiness(values);
		} catch(Exception e) {
			return null;
		}
		@SuppressWarnings("unchecked")
		Range<T>[] result = new Range[values.length];
		int i = 0;
		for(T v : values)
			result[i++] = new Range<T>(v);
		return result;
	}
	
	
	public static <T extends Serializable & Comparable<T>> Range<T>[] computeRanges(Collection<T> values) {
		if(values == null || values.size() == 0)
			return null;
		@SuppressWarnings("unchecked")
		Range<T>[] result = new Range[values.size()];
		int i = 0;
		for(T v : values)
			result[i++] = new Range<T>(v);
		return result;
	}
	
}
