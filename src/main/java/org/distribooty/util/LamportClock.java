package org.distribooty.util;

import java.io.Serializable;
import java.util.Comparator;

public class LamportClock implements Serializable, Comparable<LamportClock> {

	private static final long serialVersionUID = -4190979199261304353L;
	
	private final long delta;
	private final long threshold;
	private volatile long clock = 0L;
	
	public LamportClock(long delta) {
		if(delta <= 0 || delta > Long.MAX_VALUE / 2L)
			throw new IllegalArgumentException("delta negativ or larger than Long.MAX_VALUE / 2.");
		this.delta = delta;
		this.threshold = Long.MAX_VALUE - delta;
	}
	
	public LamportClock(long delta, long initialClockValue) {
		this(delta);
		if(initialClockValue < 0L)
			throw new IllegalArgumentException("initialClockValue is lesser than zero.");
		this.clock = initialClockValue;
	}

	@Override
	public int compareTo(LamportClock otherClock) {
		long d = clock - otherClock.clock;
		return Long.signum(Math.abs(d) > threshold ? -d : d);
	}
	
	private void incrementClock() {
		if(clock == Long.MAX_VALUE)
			clock = 0L;
		else
			clock++;
	}
	
	public synchronized long getAndIncrement() {
		long t = clock;
		incrementClock();
		return t;
	}
	
	public synchronized long incrementAndGet() {
		incrementClock();
		return clock;
	}
	
	public synchronized void updateClock(LamportClock otherClock) {
		updateClock(otherClock.clock);
	}
	
	public synchronized void updateClock(long timeStamp) {
		clock = Math.max(clock, timeStamp);
		incrementClock();
	}
	
	public synchronized void resetClock() {
		clock = 0L;
	}
	
	public synchronized void setClock(long timeStamp) {
		if(timeStamp < 0L)
			clock = 0L;
		else
			clock = timeStamp;
	}
	
	public synchronized void setClock(LamportClock otherClock) {
		clock = otherClock.clock;
	}
	
	public synchronized long getClock() {
		return clock;
	}

	public long getDelta() {
		return delta;
	}

	public long getThreshold() {
		return threshold;
	}

	public static int compare(long t1, long t2, long delta) {
		if(delta <= 0L || delta > Long.MAX_VALUE / 2L)
			throw new IllegalArgumentException("delta negativ or larger than Long.MAX_VALUE / 2.");
		if(t1 < 0L || t2 < 0L)
			throw new IllegalArgumentException("t1 or t2 is lesser than zero.");
		long d = t1 - t2;
		return Long.signum(Math.abs(d) > Long.MAX_VALUE - delta ? -d : d);
	}
	
	public static Comparator<Long> getComparator(long delta) {
		return new Comparator<Long>() {
			
			@Override
			public int compare(Long t1, Long t2) {
				return LamportClock.compare(t1, t2, delta);
			}
		};
	}
}
