package org.distribooty.util;

public interface ControllerBarrierInterface {
	boolean execute(ControlledBarrierListener listener) throws IllegalStateException;
	void waitForReadyParties(int parties) throws IllegalArgumentException;
}
