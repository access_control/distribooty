package org.distribooty.util;

public abstract class AbstractConsumer<E> extends AbstractRunnableRoutine {
	
	protected PoisonableBlockingQueue<E> queue;
	
	protected AbstractConsumer() {
	}
	
	public AbstractConsumer(E poison) {
		queue = new PoisonableBlockingQueue<E>(poison);
	}
	
	public AbstractConsumer(E poison, String name) {
		super(name);
		queue = new PoisonableBlockingQueue<E>(poison);
	}
	
	public boolean enqueue(E e) {
		return queue.enqueue(e);
	}
	
	public E dequeue() {
		return queue.dequeue();
	}

	@Override
	public void shutdown(boolean graceful) {
		queue.poison();
	}

}
