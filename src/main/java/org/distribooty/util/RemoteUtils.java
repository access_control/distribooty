package org.distribooty.util;

import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

import org.distribooty.RemotePeerContact;

public class RemoteUtils {
	public static <T extends Remote> T retrieveRemoteObject(Class<T> cls, RemotePeerContact contact) {
		try {
			return cls.cast(LocateRegistry.getRegistry(contact.getHost(), contact.getPort()).lookup(contact.getBindName()));
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace(); // TODO Remove ?
			return null;
		}
	}
	
	public static void unexport(Remote localImplementation) {
		while(true)
			try {
				while(!UnicastRemoteObject.unexportObject(localImplementation, false))
					;
				break;
			} catch (NoSuchObjectException e) {
				break;
			}
	}
}
