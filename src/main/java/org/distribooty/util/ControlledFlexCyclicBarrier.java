package org.distribooty.util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * 
 * @author access_control
 * 
 * 
 * The WorkerBarrierInterface instance of this object must be provided
 * to a worker thread through constructor and/or setter before the
 * run method is started.
 * 
 * the worker threads run method should implement follow pattern.
 * void run() {
 *  // controlledFlexCyclicBarrier is the object of type ControlledFlexCyclicBarrier
 *  (the type of WorkerBarrierInterface sufficient).
 * 	while(true)
 * 	{
 * 	controlledFlexCyclicBarrier.waitForStart();
 *  if(this worker must end)
 *  	break;
 *  
 *  // Do some work this worker is intended for
 * 
 * 	}
 *  controlledFlexCyclicBarrier.leave();
 * }
 *
 */

public class ControlledFlexCyclicBarrier implements WorkerBarrierInterface, ControllerBarrierInterface, Shutdownable {
	
	private static class ThreadExecutePermission {
		private boolean permitted;
		
		public ThreadExecutePermission(boolean permitted) {
			this.permitted = permitted;
		}

		public boolean isPermitted() {
			return permitted;
		}

		public void permit() {
			permitted = true;
		}
		
		public void deny() {
			permitted = false;
		}
	}
	
	private LinkedList<ThreadExecutePermission> pendingControllers = new LinkedList<>();
	private Map<Thread, ThreadExecutePermission> threads = new HashMap<>();
	private ThreadExecutePermission currentThreadExecutePermission = new ThreadExecutePermission(false);
	private Object startWaiter = new Object();
	private int executingThreads, completedThreads, currentNumberOfParties = 0;
	private ThreadExecutePermission shutdownObject = new ThreadExecutePermission(true);
	private CountDownLatch shutdownLatch = new CountDownLatch(1);
	
	public ControlledFlexCyclicBarrier() {}

	private static void waitForBlockerReady(ThreadExecutePermission blocker) {
		synchronized(blocker)
		{
			while(!blocker.isPermitted())
				try {
					blocker.wait();
				} catch (InterruptedException e) {
					
				}
		}
	}

	@Override
	public boolean execute(ControlledBarrierListener listener) throws IllegalStateException {		
		ThreadExecutePermission blocker;
		synchronized(this)
		{
			synchronized(shutdownObject)
			{
				if(!shutdownObject.isPermitted())
					return false;
				if(listener == null)
					{
						shutdownObject.deny();
						return true;
					}
			}
			if(threads.size() == 0)
				throw new IllegalStateException("There is no workers");
			blocker = new ThreadExecutePermission(pendingControllers.isEmpty());				
			pendingControllers.add(blocker);
		}
		waitForBlockerReady(blocker);
		blocker.deny();		
		if(listener != null)
			listener.onBegin();
		synchronized(this)
		{
			completedThreads = 0;
			executingThreads = threads.size();
			currentThreadExecutePermission.permit();
			notifyAll();
		}
		waitForBlockerReady(blocker);
		if(listener != null)
			listener.onEnd();
		notifyController(true);
		return true;
	}

	private synchronized void notifyController(boolean peek) {
		ThreadExecutePermission blocker;
		if(peek)
			blocker = pendingControllers.peek();
		else
			blocker = pendingControllers.remove();
		if(blocker != null)
		{
			blocker.permit();
			synchronized(blocker)
			{
				blocker.notify();
			}			
		}
	}

	@Override
	public synchronized boolean waitForStart() {
		final Thread thread = Thread.currentThread();
		if(currentThreadExecutePermission.isPermitted())
			currentThreadExecutePermission = new ThreadExecutePermission(false);
		boolean alreadyIn;
		if((alreadyIn = threads.containsKey(thread)) && ++completedThreads == executingThreads)		
			notifyController(false);		
		threads.put(thread, currentThreadExecutePermission);		
		if(!alreadyIn)
			{
				synchronized(startWaiter)
				{
					currentNumberOfParties = threads.size();
					startWaiter.notifyAll();
				}				
			}
		synchronized(shutdownObject)
		{
			if(!shutdownObject.isPermitted())
				return false;
		}
		while(!threads.get(thread).isPermitted())
			try {
				wait();
			} catch (InterruptedException e) {
				return false;
			}
		return true;
	}

	@Override
	public synchronized void leave() {
		if(threads.remove(Thread.currentThread()) != null && completedThreads == --executingThreads)
			notifyController(false);
		currentNumberOfParties--;
	}

	@Override
	public void waitForReadyParties(int parties) throws IllegalArgumentException {
		if(parties <= 0)
			throw new IllegalArgumentException("parties must be greater then 0");
		synchronized(startWaiter)
		{
			while(parties > currentNumberOfParties)
				try {
					startWaiter.wait();
				} catch (InterruptedException e) {
					
				}
		}
	}

	@Override
	public void shutdown(boolean graceful) {		
		execute(null);
		shutdownLatch.countDown();		
	}

	@Override
	public void waitForShutdownComplete() {
		while(true)
		{
			try {
				shutdownLatch.await();
				break;
			} catch (InterruptedException e) {
				
			}
		}
	}
}
