package org.distribooty.util;

public interface Cleaner {
	boolean clean(Cleanable cleanableObject);
	void reportUsage(Cleanable cleanableObject);
}
