package org.distribooty.util;

import java.io.Serializable;
import java.util.Arrays;

public class UniversalCounter implements Comparable<UniversalCounter>, Serializable {

	private static final long serialVersionUID = -3931260372384783072L;
	
	private byte[] countValue;
	
	public UniversalCounter(int numberOfBits) {
		if(numberOfBits <= 0 || numberOfBits % 8 != 0)
			throw new IllegalArgumentException("The provided number of bits must be positive integer which is multiple of 8");
		countValue = new byte[numberOfBits / 8];
		reset();
	}
	
	public UniversalCounter(byte[] value) {
		this(value.length * 8);
		set(value);
	}
	
	public UniversalCounter(UniversalCounter counter) {
		this(counter.countValue);
	}
	
	public void reset() {
		for(int i = 0; i < countValue.length; i++)
			countValue[i] = 0;
	}
	
	public void set(byte[] value) {
		int i, j;
		for(i = value.length - 1, j = countValue.length - 1; i >= 0 && j >= 0; i--, j--)
			countValue[j] = value[i];
		while(j >= 0)
			countValue[j--] = 0;
	}
	
	public boolean changeBy(int val) {
		if(val > 127 || val < -128)
			throw new IllegalArgumentException("val must be lesser than 128 and greater then -129");
		int i;
		for(i = countValue.length - 1; i >= 0; i--)
		{
			val += ((int) countValue[i] & 0xFF);
			countValue[i] = (byte) (val & 0xFF);
			if(val > 0xFF)
				val = 1;
			else if(val < 0)
				val = -1;
			else
				break;
		}
		return i < 0;
	}
	
	public byte[] getCountValue() {
		return countValue.clone();
	}
	
	public int getBitLength() {
		return countValue.length * 8;
	}
	
	@Override
	public int compareTo(UniversalCounter o) {
		if(o.countValue.length != countValue.length)
			throw new IllegalArgumentException("The bit lengths of the universal counters does not match");
		int i, cmp;
		for(i = cmp = 0; i < countValue.length && cmp == 0; i++)		
			cmp = Integer.compare(countValue[i] & 0xFF, o.countValue[i] & 0xFF);
		return cmp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(countValue);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UniversalCounter other = (UniversalCounter) obj;
		if (!Arrays.equals(countValue, other.countValue))
			return false;
		return true;
	}

	public void set(UniversalCounter sequenceNumber) {
		set(sequenceNumber.countValue);		
	}
	
}
