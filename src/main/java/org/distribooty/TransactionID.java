package org.distribooty;

import org.distribooty.lock.TimestampedEntity;

public class TransactionID extends TimestampedEntity {
	
	private static final long serialVersionUID = 5480017222544399773L;
	
	private final long term, sequenceNumber;

	public TransactionID(int srcPid, long timeStamp, long term, long sequenceNumber) {
		super(srcPid, timeStamp);
		this.term = term;
		this.sequenceNumber = sequenceNumber;
	}
	
	public long getTerm() {
		return term;
	}

	public long getSequenceNumber() {
		return sequenceNumber;
	}	
}
