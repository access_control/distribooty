package org.distribooty;

import java.io.Serializable;

public class DefaultHeartbeatData implements Serializable {
	
	private static final long serialVersionUID = 8346256552197278213L;

	private long numberOfClients = 0;

	public DefaultHeartbeatData() {}
	
	public DefaultHeartbeatData(long numberOfClients) {
		this.numberOfClients = numberOfClients;
	}

	public long getNumberOfClients() {
		return numberOfClients;
	}
}
