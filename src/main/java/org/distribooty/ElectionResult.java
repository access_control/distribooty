package org.distribooty;

import java.io.Serializable;
import java.util.Set;

public class ElectionResult implements Serializable {

	private static final long serialVersionUID = -7646681845772863779L;
	
	private int masterPid;
	private Set<Integer> sameStateAsMasterPids;
	private Set<Integer> participantPids;
	
	public ElectionResult(int masterPid, Set<Integer> sameStateAsMasterPids, Set<Integer> participantPids) {
		this.masterPid = masterPid;
		this.sameStateAsMasterPids = sameStateAsMasterPids;
		this.participantPids = participantPids;
	}
	
	public int getMasterPid() {
		return masterPid;
	}	
	
	public Set<Integer> getSameStateAsMasterPids() {
		return sameStateAsMasterPids;
	}

	public Set<Integer> getParticipantPids() {
		return participantPids;
	}	
}
