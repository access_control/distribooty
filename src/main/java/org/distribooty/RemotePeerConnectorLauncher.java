package org.distribooty;

import java.io.IOException;
import java.io.Serializable;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import org.distribooty.connector.DefaultPersistentAccessProvider;
import org.distribooty.connector.DefaultRemotePeerConnector;
import org.distribooty.connector.DefaultRemotePeerConnectorProvider;
import org.distribooty.connector.PersistentAccessProvider;
import org.distribooty.debug.RemoteLogger;

public class RemotePeerConnectorLauncher {
	public static <E extends Serializable & Comparable<E>, H extends Serializable> void
	launch(RemotePeerConnectorConfigurationBuilder builder, 
			LocalPeerHandler<E, H> remotePeerHandler, boolean blocking) {
		launch(builder.build(), remotePeerHandler, blocking);
	}
	
	public static <E extends Serializable & Comparable<E>, H extends Serializable> void
	launch(RemotePeerConnectorConfiguration configuration, LocalPeerHandler<E, H> remotePeerHandler, boolean blocking) {
		RemoteLogger.applyDefaultLevel(configuration.getLoggingLevel());
		PersistentAccessProvider persistentAccessProvider = new DefaultPersistentAccessProvider(configuration.getPersistenceFile());
		RemotePeerContact thisContact = configuration.getMappedRemotePeerContacts().get(configuration.getPid());
		DefaultRemotePeerConnectorProvider remotePeerConnectorProvider;
		try {
			remotePeerConnectorProvider = new DefaultRemotePeerConnectorProvider(thisContact, configuration.getRmiSecuritySpecs());
			DefaultRemotePeerConnector<E, H> connector = new DefaultRemotePeerConnector<>(configuration, 
					remotePeerConnectorProvider, 
					remotePeerHandler, 
					persistentAccessProvider);
			connector.start();
			if(blocking)
				connector.waitForShutdownComplete();
		} catch (UnrecoverableKeyException | KeyManagementException | KeyStoreException | NoSuchAlgorithmException
				| CertificateException | IOException e) {
			throw new IllegalArgumentException(e);
		}
	}
}
