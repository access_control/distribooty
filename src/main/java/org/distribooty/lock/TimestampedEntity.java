package org.distribooty.lock;

import java.io.Serializable;

import org.distribooty.util.LamportClock;

public class TimestampedEntity implements Comparable<TimestampedEntity>, Serializable {
	
	private static final long serialVersionUID = -6210134018975497287L;
	
	public static final long DELTA = 1_000_000_000L;
	
	private int srcPid;
	private long timestamp;
	
	public TimestampedEntity(int srcPid, long timestamp) {
		this.srcPid = srcPid;
		this.timestamp = timestamp;
	}
	
	public TimestampedEntity(TimestampedEntity timestampedEntity) {
		srcPid = timestampedEntity.srcPid;
		timestamp = timestampedEntity.timestamp;
	}
	
	public TimestampedEntity() {
		srcPid = -1;
		timestamp = -1L;
	}
	
	public int getSrcPid() {
		return srcPid;
	}
	
	public long getTimestamp() {
		return timestamp;
	}

	@Override
	public int compareTo(TimestampedEntity o) {
		int cmp = LamportClock.compare(timestamp, o.timestamp, DELTA);
		if(cmp != 0)
			return cmp;
		else
			return Integer.signum(srcPid - o.srcPid);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + srcPid;
		result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TimestampedEntity))
			return false;
		TimestampedEntity other = (TimestampedEntity) obj;
		if (srcPid != other.srcPid)
			return false;
		if (timestamp != other.timestamp)
			return false;
		return true;
	}
	
	public TimestampedEntity clone(long newTimestamp) {
		try {
			TimestampedEntity cloned = (TimestampedEntity) clone();
			cloned.timestamp = newTimestamp;
			return cloned;
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}

	
}
