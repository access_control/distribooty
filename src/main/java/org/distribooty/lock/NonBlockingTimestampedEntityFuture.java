package org.distribooty.lock;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class NonBlockingTimestampedEntityFuture implements Future<TimestampedEntity> {
	
	private TimestampedEntity timestampedEntity;
	private boolean canceled = false;

	public NonBlockingTimestampedEntityFuture(TimestampedEntity timestampedEntity) {
		this.timestampedEntity = timestampedEntity;
	}

	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		return canceled = true;
	}

	@Override
	public boolean isCancelled() {
		return canceled;
	}

	@Override
	public boolean isDone() {
		return canceled;
	}
	
	@Override
	public TimestampedEntity get() throws InterruptedException, ExecutionException {
		return this.timestampedEntity;
	}

	@Override
	public TimestampedEntity get(long timeout, TimeUnit unit)
			throws InterruptedException, ExecutionException, TimeoutException {
		return this.timestampedEntity;
	}

}
