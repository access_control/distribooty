package org.distribooty.lock.util;

import java.util.HashMap;
import java.util.Map;

import org.distribooty.lock.util.PositiveCounter.PositiveCounterState;

/**
 * This instance of this class is used to track all
 * threads, which is reentrant, which were allowed to enter
 * the critical section.
 * Typical usage: <br />
 * <pre>
 *  // Let guard be the object of type ReentrantCriticalSectionAwareGuard
 *  if(guard.enter())
 *  	{
 *  		// Critical section begin
 *  		// ...
 *  		// Critical section end
 *  		guard.leave();
 *  	}
 * </pre>
 * After creation (calling constructor) the guard is unlocked.
 * The successfully method call of {@link #enter()} is then when it returns true.
 * This means the calling thread was allowed to enter the critical section.
 * If it returns false, the critical section must not be entered.
 * If it was allowed to enter the critical section the current thread must release
 * the critical section by calling {@link #leave()} which means the leaving of the 
 * critical section. After the successful call of {@link #enter()} and before 
 * the call of appropriate the {@link #leave()} method, they say the thread is holding the guard.
 * If this instance become locked by calling of {@link #lock} the entering of a
 * critical section by threads not holding this guard is not permitted so the {@link #enter()}
 * will return false, so in this situation only the threads which are already holding this guard,
 * are allowed to enter an appropriate critical section (reentrance).
 * 
 */
public class ReentrantCriticalSectionAwareGuard {
	
	
	
	private PositiveCounter entered = new PositiveCounter();
	/**
	 * Determines if the instance of this class is locked.
	 */
	private volatile boolean locked;
	private volatile boolean shutdownFlag = false;
	/**
	 * Tracks the calling {@link #enter()} threads.
	 */
	private Map<Thread, PositiveCounter> threads = new HashMap<>();
	
	/**
	 * Creates unlocked guard.
	 */
	public ReentrantCriticalSectionAwareGuard() {
		this(false);
	}
	
	public ReentrantCriticalSectionAwareGuard(boolean locked) {
		this.locked = locked;
	}
	
	/**
	 * Locks this guard.
	 */
	public synchronized void lock() {
		if(!locked)
		{
			locked = true;
			if(entered.getCounter() == 0)
				notifyAll();		
		}
	}
	
	public synchronized void unlock() {
		if(!shutdownFlag)
			locked = false;
	}
	
	/**
	 * Registers this thread as entered if it returns true.
	 * @return true if the guard was unlocked or the current 
	 * thread has already entered the guard, else false.
	 * @throws IllegalStateException if a counter value has exceed Integer.MAX_VALUE.
	 */
	public synchronized boolean enter() throws IllegalStateException {
		PositiveCounter c = threads.computeIfAbsent(Thread.currentThread(), key -> new PositiveCounter());
		if(!locked || c.getCounter() > 0)
		{
			c.increment();
			entered.increment();
			return true;
		}
		else
			return false;
	}
	
	/**
	 * Unregisters this thread which must match with previously called {@link #enter()} method. 
	 * @throws IllegalStateException if the call is not matched to the previously called {@link #enter()}
	 */
	public synchronized void leave() throws IllegalStateException {
		Thread t = Thread.currentThread();
		PositiveCounter c = threads.get(t);
		PositiveCounterState state = null;
		if(c == null || (state = c.decrement()) == PositiveCounterState.ALREADY_ZERO)
			throw new IllegalStateException("The current thread has not called the method enter before");
		else if(state == PositiveCounterState.JUST_ZERO)
			threads.remove(t);
		if(entered.decrement() == PositiveCounterState.JUST_ZERO && locked)
			notifyAll();
	}
	
	/**
	 * This method blocks until this instance is locked ({@link #lock()} called) and for all
	 * call of method {@link #enter()} appropriate method {@link #leave()} has been called
	 * in the same thread.
	 * The method returns immediately if the condition above is met.
	 * @param exitIfInterrupted if the waiting thread is interrupted during
	 * waiting by calling this method, the method exits if this parameter is true,
	 * else than it continues to wait
	 */
	public synchronized void waitUntilAllLeaveAfterLocked(boolean exitIfInterrupted) {
		while(!locked || entered.getCounter() > 0)
			try {
				wait();
			} catch (InterruptedException e) {
				if(exitIfInterrupted)
					return;
			}
	}
	
	/**
	 * 
	 * @return true if this instance is locked else false.
	 */
	public boolean isLocked() {
		return locked;
	}
	
	/**
	 * 
	 * @return number of successfully called {@link #enter()} method threads
	 * which returned true but has not called {@link #leave()} already.
	 */
	public int getEntered() {
		return entered.getCounter();
	}
	
}
