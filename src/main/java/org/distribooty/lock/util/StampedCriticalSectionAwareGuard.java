package org.distribooty.lock.util;

import java.util.concurrent.locks.StampedLock;

public class StampedCriticalSectionAwareGuard {

	private StampedLock stampedLock = new StampedLock();
	
	private volatile boolean locked = false;
	
	public synchronized void lock() {
		if(!locked)
		{
			locked = true;
			if(!stampedLock.isReadLocked())
				notifyAll();
		}
	}
	
	public synchronized long enter() {
		if(!locked)		
			return stampedLock.readLock();
		else
			return 0L;
	}
	
	public synchronized void leave(long stamp) throws IllegalMonitorStateException {
		stampedLock.unlockRead(stamp);
		if(!stampedLock.isReadLocked() && locked)
			notifyAll();
	}
	
	public boolean isLocked() {
		return locked;
	}
	
	public synchronized void waitUntilAllLeaveAfterLocked(boolean exitIfInterrupted) {
		while(!locked || stampedLock.isReadLocked())
			try {
				wait();
			} catch (InterruptedException e) {
				if(exitIfInterrupted)
					return;
			}
	}
	
	public int getEntered() {
		return stampedLock.getReadLockCount();
	}
}
