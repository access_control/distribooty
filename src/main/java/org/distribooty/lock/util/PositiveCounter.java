package org.distribooty.lock.util;

/**
 * Class which represents a counter which can not go below 0.
 */
public class PositiveCounter {
	
	/**
	 * The state of counter after decrement.
	 */
	public enum PositiveCounterState {
		/**
		 * If the counter already was 0 in the moment of calling.
		 */
		ALREADY_ZERO,
		
		/**
		 * If the counter just became 0 after the decrement.
		 */
		JUST_ZERO,
		
		/**
		 * If the counter is above 0 after the decrement.
		 */
		ABOVE_ZERO;
	}
	/**
	 * The counter.
	 */
	private volatile int counter = 0;
	
	/**
	 * Increments the counter.
	 * @throws IllegalStateException if counter value exceeds Integer.MAX_VALUE.
	 */
	public void increment() throws IllegalStateException {
		if(counter == Integer.MAX_VALUE)
			throw new IllegalStateException("Counter\'s value exceed Integer.MAX_VALUE");
		counter++;
	}
	
	/**
	 * Decrements this counter, but the counter doesn't go below 0.
	 * @return the state of counter.
	 */
	public PositiveCounterState decrement() {
		return counter > 0 && --counter == 0 ? PositiveCounterState.JUST_ZERO: counter == 0 
				? PositiveCounterState.ALREADY_ZERO : PositiveCounterState.ABOVE_ZERO;
	}
	
	/**
	 * 
	 * @return counter's value.
	 */
	public int getCounter() {
		return counter;
	}
}