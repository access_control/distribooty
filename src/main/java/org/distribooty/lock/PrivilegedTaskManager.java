package org.distribooty.lock;

import java.util.concurrent.ScheduledExecutorService;

import org.distribooty.RemotePeerFailureListener;
import org.distribooty.lock.centralized.intern.LocalCLockAcquisitor;
import org.distribooty.lock.centralized.intern.LocalCLockRequestHandler;
import org.distribooty.lock.decentralized.intern.LocalDLockAware;
import org.distribooty.util.Cleaner;

public interface PrivilegedTaskManager extends 
PrivilegedTaskService, LocalCLockAcquisitor, LocalDLockAware,
LocalCLockRequestHandler, Cleaner, RemotePeerFailureListener {
	ScheduledExecutorService getExecutorService();
	void updateTerm(long newTerm);
	void shutdownCleanerService();
	void shutdownExecutorService();
}
