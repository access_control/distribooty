package org.distribooty.lock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.locks.Lock;
import java.util.logging.Logger;

import org.distribooty.TransactionID;
import org.distribooty.connector.function.ExecutorUtils;
import org.distribooty.connector.function.ExecutorUtils.Container;
import org.distribooty.debug.RemoteLogger;
import org.distribooty.connector.function.SimpleBooleanAwareExecutor;
import org.distribooty.connector.function.SimpleExceptionAwareExecutor;
import org.distribooty.lock.centralized.CLockHandshake;
import org.distribooty.lock.centralized.CLockManager;
import org.distribooty.lock.centralized.CLockRequest;
import org.distribooty.lock.centralized.CPrivilegedTask;
import org.distribooty.lock.centralized.CPrivilegedTaskExecutionState;
import org.distribooty.lock.centralized.CPrivilegedTaskWorker;
import org.distribooty.lock.centralized.CRangeLock;
import org.distribooty.lock.centralized.CSimpleLock;
import org.distribooty.lock.centralized.CSimpleLockGraphVertexKeeper;
import org.distribooty.lock.centralized.CSimpleLockGraphVertexKeeperMode;
import org.distribooty.lock.centralized.TimestampedCRangeLocks;
import org.distribooty.lock.centralized.TimestampedCSimpleLocks;
import org.distribooty.lock.centralized.TransactionalCLockRequest;
import org.distribooty.lock.centralized.intern.LocalCLockApproveSender;
import org.distribooty.lock.centralized.intern.LocalCLockProvider;
import org.distribooty.lock.decentralized.DLockUnicastRequest;
import org.distribooty.lock.decentralized.DLockUnicastResponse;
import org.distribooty.lock.decentralized.DPrivilegedTask;
import org.distribooty.lock.decentralized.DPrivilegedTaskWorker;
import org.distribooty.lock.decentralized.DPrivilegedTaskWrapper;
import org.distribooty.lock.decentralized.intern.LocalDLockRequestSender;
import org.distribooty.lock.intern.SyncCPrivilegedTaskWorkerCreator;
import org.distribooty.lock.util.ReentrantCriticalSectionAwareGuard;
import org.distribooty.util.Cleanable;
import org.distribooty.util.ConcurrentUtils;
import org.distribooty.util.LamportClock;

public class DefaultPrivilegedTaskManager implements PrivilegedTaskManager, SyncCPrivilegedTaskWorkerCreator {
	
	private int pid;
	private ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(Integer.MAX_VALUE);
	private LamportClock sequenceNumberClock;
	private LamportClock termClock;
	private LamportClock timestampedEntityClock;
	private Map<Long, CPrivilegedTaskWorker> cLockWorkers = new HashMap<>();
	private Map<String, DPrivilegedTaskWorker> dLockWorkers = new HashMap<>();
	private ReentrantCriticalSectionAwareGuard cLockLocalGuard, dLockLocalGuard;
	private CLockManager cLockManager;
	private LocalCLockApproveSender localCLockApproveSender;
	private LocalDLockRequestSender localDLockRequestSender;
	private LocalCLockProvider localCLockProvider;
	private CSimpleLockGraphVertexKeeperMode mode;
	private DefaultCleanerService cleanerService;
	private boolean dLockWorkersShutdown = false;
	private long dLockWorkerLiveTime;
	private long dLockWorkerTimeout;
	private long simpleLockKeeperLiveTIme;
	
	public DefaultPrivilegedTaskManager(int pid, CSimpleLockGraphVertexKeeperMode mode,
			long cleanerPeriod, long dLockWorkerLiveTime, long dLockWorkerTimeout,
			long simpleLockKeeperLiveTime, LamportClock timestampedEntityClock, LamportClock sequenceNumberClock,
			LamportClock termClock, ReentrantCriticalSectionAwareGuard cLockLocalGuard,
			ReentrantCriticalSectionAwareGuard dLockLocalGuard, LocalCLockApproveSender localCLockApproveSender,
			LocalDLockRequestSender localDLockRequestSender, LocalCLockProvider localCLockProvider) {
		this.pid = pid;
		this.mode = mode;
		this.dLockWorkerLiveTime = dLockWorkerLiveTime;
		this.dLockWorkerTimeout = dLockWorkerTimeout;
		this.simpleLockKeeperLiveTIme = simpleLockKeeperLiveTime;
		this.timestampedEntityClock = timestampedEntityClock;
		this.sequenceNumberClock = sequenceNumberClock;
		this.termClock = termClock;
		this.cLockLocalGuard = cLockLocalGuard;
		this.dLockLocalGuard = dLockLocalGuard;
		this.localCLockApproveSender = localCLockApproveSender;
		this.localDLockRequestSender = localDLockRequestSender;
		this.localCLockProvider = localCLockProvider;
		
		this.executorService = new ScheduledThreadPoolExecutor(Integer.MAX_VALUE);
		this.cleanerService = new DefaultCleanerService(this, cleanerPeriod, pid);
		this.executorService.execute(this.cleanerService);
	}

	
	@Override
	public Future<TimestampedEntity> addPrivilegedTask(CSimpleLock lock, CPrivilegedTask privilegedTask) {
		return addPrivilegedTask(new CSimpleLock[] {lock}, privilegedTask);
	}
	
	
	private CPrivilegedTaskWorker getCWorker(CPrivilegedTask privilegedTask, TimestampedEntity timestampedEntity, boolean async) {
		CPrivilegedTaskWorker worker = new CPrivilegedTaskWorker(privilegedTask, timestampedEntity, localCLockProvider, this, async);
		executeObjectLockAware(cLockWorkers, () -> {
			cLockWorkers.put(timestampedEntity.getTimestamp(), worker);			
		});
		if(async)
			executorService.execute(worker);
		return worker;
	}

	@Override
	public Future<TimestampedEntity> addPrivilegedTask(CSimpleLock[] locks, CPrivilegedTask privilegedTask) {
		Container<Future<TimestampedEntity>> container = new Container<>();
		ExecutorUtils.executeGuardAware(cLockLocalGuard, () -> {
			container.setValue(getCWorker(privilegedTask, new TimestampedCSimpleLocks(this.pid, timestampedEntityClock.getAndIncrement(), locks), true));
		});
		return container.getValue();
	}

	@Override
	public Future<TimestampedEntity> addPrivilegedTask(CRangeLock lock, CPrivilegedTask privilegedTask) {
		return addPrivilegedTask(new CRangeLock[] {lock}, privilegedTask);
	}

	@Override
	public Future<TimestampedEntity> addPrivilegedTask(CRangeLock[] locks, CPrivilegedTask privilegedTask) {
		Container<Future<TimestampedEntity>> container = new Container<>();
		ExecutorUtils.executeGuardAware(cLockLocalGuard, () -> {
			container.setValue(getCWorker(privilegedTask, new TimestampedCRangeLocks(this.pid, timestampedEntityClock.getAndIncrement(), locks), true));
		});
		return container.getValue();
	}

	@Override
	public Lock getLock(CSimpleLock lock) {
		return getLock(new CSimpleLock[] {lock});
	}

	@Override
	public Lock getLock(CSimpleLock[] locks) {
		return new SyncCLock(this, new TimestampedCSimpleLocks(pid, timestampedEntityClock.getAndIncrement(), locks));
	}

	@Override
	public Lock getLock(CRangeLock lock) {
		return getLock(new CRangeLock[] {lock});
	}

	@Override
	public Lock getLock(CRangeLock[] locks) {
		return new SyncCLock(this, new TimestampedCRangeLocks(pid, timestampedEntityClock.getAndIncrement(), locks));
	}

	@Override
	public Future<TimestampedEntity> addPrivilegedTask(String lockId, DPrivilegedTask privilegedTask) {
		Container<DPrivilegedTaskWrapper> container = new Container<>();
		ExecutorUtils.executeGuardAware(dLockLocalGuard, () -> {
			DPrivilegedTaskWorker worker = getDWorker(lockId);
			if(worker != null)
				container.setValue(localDLockRequestSender.sendDLockRequests(lockId, privilegedTask, worker));
		});
		return container.getValue();
	}

	@Override
	public boolean receiveCLockApprove(long timestamp) {
		Container<CPrivilegedTaskWorker> container = new Container<>();
		executeObjectLockAware(cLockWorkers, () -> {
			container.setValue(this.cLockWorkers.get(timestamp));
		});
		CPrivilegedTaskWorker worker = container.getValue();
		return worker != null && worker.onLockApproved();
	}

	@Override
	public List<CLockHandshake> getCLockHandshakes(boolean createEmptyContainerIfEmpty) {
		List<CLockHandshake> result = new ArrayList<>();
		boolean empty = this.cLockWorkers.isEmpty();		
		if(!empty)
		{
			executeObjectLockAware(this.cLockWorkers, () -> {
				this.cLockWorkers.forEach((k, v) -> {
					CPrivilegedTaskExecutionState state = v.getState();
					if(state.compareTo(CPrivilegedTaskExecutionState.LOCK_AWAITING_APPROVE) >= 0 &&
							state.compareTo(CPrivilegedTaskExecutionState.LOCK_RELEASING) <= 0)
						result.add(new CLockHandshake(v.getTimestampedEntity(), v.getLockResult().getTransactionId().getSequenceNumber(), v.isLockApproved()));
				});			
			});
			if(result.isEmpty() && !createEmptyContainerIfEmpty)
				return null;
			else
				return result;
		}
		else if(createEmptyContainerIfEmpty)
			return result;
		else
			return null;
	}
	
	private DPrivilegedTaskWorker getDWorker(String lockId) {
		synchronized(this.dLockWorkers)
		{
			DPrivilegedTaskWorker worker = this.dLockWorkers.get(lockId);
			if(!this.dLockWorkersShutdown && (worker == null || !worker.resetCleanableStatus()))
			{
				worker = new DPrivilegedTaskWorker(lockId, this.localDLockRequestSender, this, this.dLockWorkerLiveTime, this.dLockWorkerTimeout);
				this.executorService.execute(worker);
				this.dLockWorkers.put(lockId, worker);
			}
			return worker;
		}
	}

	@Override
	public boolean receiveDLockRequest(DLockUnicastRequest request) {
		timestampedEntityClock.updateClock(request.getTimestamp());
		DPrivilegedTaskWorker worker = getDWorker(request.getLockId());
		return worker != null && worker.receiveDLockRequest(request);
	}

	@Override
	public void receiveDLockResponse(DLockUnicastResponse response) {
		DPrivilegedTaskWorker worker = getDWorker(response.getLockId());
		if(worker != null)
			worker.receiveDLockResponse(response);
	}

	@Override
	public void initCLockRequestHandler(List<CLockHandshake> cLockHandshakes) {
		int s = cLockHandshakes.size();
		Collections.sort(cLockHandshakes);
		if(s > 0)
			sequenceNumberClock.updateClock(cLockHandshakes.get(s - 1).getSequenceNumber());
		cLockManager = new CLockManager(cLockHandshakes, 
				this.localCLockApproveSender, 
				this.executorService, 
				this.mode, 				
				this,
				this.pid,
				this.simpleLockKeeperLiveTIme);
	}
	
	@Override
	public void activateApprovedHandshakes(List<CLockHandshake> cLockHandshakes) {
		cLockManager.activateApprovedCLockHanshakes(cLockHandshakes);		
	}

	@Override
	public TransactionID handleCLockRequest(CLockRequest request) {
		Container<TransactionID> container = new Container<>();
		executeObjectLockAware(sequenceNumberClock, () -> {
			TimestampedEntity entity = request.getTimestampedEntity();
			TransactionID transactionId = new TransactionID(entity.getSrcPid(), entity.getTimestamp(), termClock.getClock(), sequenceNumberClock.getAndIncrement());
			cLockManager.onCLockRequest(new TransactionalCLockRequest(request, transactionId));
			container.setValue(transactionId);
		});
		return container.getValue();
	}

	@Override
	public void handleCLockRelease(TimestampedEntity timestampedEntity) {
		this.cLockManager.onCLockRelease(timestampedEntity);
	}

	@Override
	public void waitUntilCLocksDrained() {
		if(this.cLockManager != null)
			this.cLockManager.waitForCLocksEmpty();

	}
	
	private boolean executeObjectLockAware(Object lock, SimpleBooleanAwareExecutor executor) {
		synchronized(lock)
		{
			return executor.execute();
		}
	}
	
	private Exception executeObjectLockAware(Object lock, SimpleExceptionAwareExecutor executor) {
		synchronized(lock)
		{
			try {
				executor.execute();
				return null;
			} catch (Exception e) {
				return e;
			}
		}
	}

	@Override
	public boolean clean(Cleanable cleanableObject) {
		Logger logger = RemoteLogger.getInstance(pid);
		if(cleanableObject instanceof CPrivilegedTaskWorker)
		{
			return executeObjectLockAware(cLockWorkers, () -> 
				{
					cLockWorkers.remove(((CPrivilegedTaskWorker) cleanableObject).getTimestampedEntity().getTimestamp());
					if(cLockWorkers.isEmpty())
						cLockWorkers.notifyAll();
					return true;
				}
			);
		}
		else if(cleanableObject instanceof DPrivilegedTaskWorker)
		{
			return executeObjectLockAware(dLockWorkers, () -> {
				if(cleanableObject.isCleanable())
					{
						logger.info("Cleaner service cleaned DPrivilegedTaskWorker with lockId" + ((DPrivilegedTaskWorker) cleanableObject).getLockId());
						DPrivilegedTaskWorker worker = dLockWorkers.remove(((DPrivilegedTaskWorker) cleanableObject).getLockId());
						if(worker != null)
							worker.shutdown(true);
						if(dLockWorkers.isEmpty())
							dLockWorkers.notifyAll();
						return true;
					}
				return false;
			});
		}
		else if(cleanableObject instanceof CSimpleLockGraphVertexKeeper)
		{
			logger.info("Cleaner service cleaned CSimpleLockGraphVertexKeeper with lockId" + ((CSimpleLockGraphVertexKeeper) cleanableObject).getLockId());
			cLockManager.onKeeperClean((CSimpleLockGraphVertexKeeper) cleanableObject);
			return true;
		}
		return false;
	}

	@Override
	public void reportUsage(Cleanable cleanableObject) {
		this.cleanerService.reportUsage(cleanableObject);
	}
	
	@Override
	public void forceAllCPrivilegedTasksShutdown() {
		executeObjectLockAware(this.cLockWorkers, () -> {
			for(Future<TimestampedEntity> entity : this.cLockWorkers.values())			
				entity.cancel(true);			
		});
	}

	@Override
	public void waitUntilAllCPrivilegedTasksDrained() {
		ConcurrentUtils.waitUninterruptible(cLockWorkers);		
	}

	@Override
	public void waitUntilAllDPrivilegedTaskWorkersDrained() {
		ConcurrentUtils.waitUninterruptible(dLockWorkers);		
	}

	@Override
	public ScheduledExecutorService getExecutorService() {
		return executorService;
	}

	@Override
	public void shutdownCLockRequestHandler() {
		if(this.cLockManager != null)
			this.cLockManager.shutdownCLockHandlerAndApproveSender();
	}

	@Override
	public void shutdownAllDPrivilegedTaskWorkers() {
		executeObjectLockAware(dLockWorkers, () -> {
			for(DPrivilegedTaskWorker worker : dLockWorkers.values())
				worker.shutdown(true);
			this.dLockWorkersShutdown  = true;
		});		
	}

	@Override
	public void updateTerm(long newTerm) {
		termClock.updateClock(newTerm);
	}

	@Override
	public void onRemotePeerFail(Set<Integer> unavailablePids) {
		if(cLockManager != null)
			cLockManager.onRemotePeersFail(unavailablePids);
		executeObjectLockAware(dLockWorkers, () -> {
			for(DPrivilegedTaskWorker worker : dLockWorkers.values())			
				worker.onRemotePeerFail(unavailablePids);			
		});
	}

	@Override
	public CPrivilegedTaskWorker getCPrivilegedTaskWorker(TimestampedEntity oldTimestampedEntity, CPrivilegedTask privilegedTask) {
		Container<CPrivilegedTaskWorker> container = new Container<>();
		ExecutorUtils.executeGuardAware(cLockLocalGuard, () -> {
			container.setValue(getCWorker(privilegedTask, oldTimestampedEntity.clone(timestampedEntityClock.getAndIncrement()), false));
		});
		return container.getValue();
	}


	@Override
	public void shutdownCleanerService() {
		this.cleanerService.shutdown(true);
		this.cleanerService.waitForShutdownComplete();
	}


	@Override
	public void shutdownExecutorService() {
		this.executorService.shutdown();		
	}
	
	public void dumpCPrivilegedTasks() {
		RemoteLogger.getInstance(pid).info("CPrivilegedTasksWorker dump command received");
		System.out.println("Size " + this.cLockWorkers.size());
			this.cLockWorkers.forEach((k, v) -> {
				System.out.println("CPrivilegedTaskWorker " +
						v.getExecutingThread().getName() + " " + 
						v.getState().name() + " " + (v.isLockApproved() ? "approved" : "unapproved") + " " + v.getLockResult().getLockRequestStatus().name());
			});			
		
		System.out.println("");
	}
	
	public void dumpCLocks() {
		cLockManager.dumpCLocks();
	}

}
