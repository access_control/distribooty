package org.distribooty.lock.centralized;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.distribooty.lock.TimestampedEntity;

public class DefaultCLockGraphVertex extends CLockGraphVertex implements Comparable<DefaultCLockGraphVertex> {
	
	protected final TimestampedEntity timeStampedEntity;
	protected final long sequenceNumber;
	
	public DefaultCLockGraphVertex(CLockGraphVertexListener listener, TimestampedEntity timeStampedEntity, long sequenceNumber) {
		super(listener);
		this.timeStampedEntity = timeStampedEntity;
		this.sequenceNumber = sequenceNumber;
	}	
	
	@Override
	public int compareTo(DefaultCLockGraphVertex o) {
		return Long.compare(sequenceNumber, o.sequenceNumber);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (sequenceNumber ^ (sequenceNumber >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DefaultCLockGraphVertex other = (DefaultCLockGraphVertex) obj;
		if (sequenceNumber != other.sequenceNumber)
			return false;
		return true;
	}
	
	private Collection<CLockGraphVertex> getInTraversalOrder(Set<CLockGraphVertex> vertices) {
		Set<CLockGraphVertex> sortedVertices = new TreeSet<>();
		Set<CLockGraphVertex> unsortedVertices = new HashSet<>();
		if(vertices != null)
		{
			for(CLockGraphVertex v : vertices)
				if(v instanceof DefaultCLockGraphVertex)
					sortedVertices.add(v);
				else
					unsortedVertices.add(v);
			List<CLockGraphVertex> resultVertices = new ArrayList<>(sortedVertices);
			resultVertices.addAll(unsortedVertices);
			return resultVertices;
		}
		else
			return null;
	}
	
	@Override
	public Collection<CLockGraphVertex> getIncommingInTraversalOrder() {
		return getInTraversalOrder(super.getIncommingVertices());
	}

	@Override
	public Collection<CLockGraphVertex> getOutcommingInTraversalOrder() {
		return getInTraversalOrder(super.getOutcommingVertices());
	}


	public TimestampedEntity getTimeStampedEntity() {
		return timeStampedEntity;
	}

	public long getSequenceNumber() {
		return sequenceNumber;
	}

	@Override
	public CLockCollisionStatus getCollisionStatus(String lockId) {
		if(timeStampedEntity instanceof TimestampedCSimpleLocks)
		{
			CSimpleLock lock = ((TimestampedCSimpleLocks) timeStampedEntity).getMappedCSimpleLocks().get(lockId);
			if(lock == null)
				return CLockCollisionStatus.NON_COLLIDED;
			else if(lock.isExclusive())
				return CLockCollisionStatus.EXCLUSIVE;
			else
				return CLockCollisionStatus.SHARED;			
		}
		else if(timeStampedEntity instanceof TimestampedCRangeLocks)
		{
			TimestampedCRangeLocks lock = (TimestampedCRangeLocks) timeStampedEntity;
			if(lock.conflictsExclusive(lockId))
				return CLockCollisionStatus.EXCLUSIVE;
			else if(lock.conflictsShared(lockId))
				return CLockCollisionStatus.SHARED;
			else
				return CLockCollisionStatus.NON_COLLIDED;
		}
		return null;
	}	
}
