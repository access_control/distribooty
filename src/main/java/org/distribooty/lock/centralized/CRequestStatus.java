package org.distribooty.lock.centralized;

public enum CRequestStatus {
	CLOCK_REQUEST_SENT,
	CLOCK_REQUEST_NOT_SENT,
	MASTER_UNAVAILABLE;
}
