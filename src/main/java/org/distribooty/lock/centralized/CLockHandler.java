package org.distribooty.lock.centralized;

import java.util.Set;
import java.util.logging.Level;

import org.distribooty.debug.RemoteLogger;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.util.AbstractConsumer;


public class CLockHandler extends AbstractConsumer<Object> {

	private CLockManager cLockManager;
	
	public CLockHandler(CLockManager cLockManager, int pid) {
		super("Exit", "CLockHandler-Thread-" + pid);
		this.cLockManager = cLockManager;
	}

	@Override
	public void run() {
		onRunEnter();
		while(true)
		{
			try {
					Object obj = dequeue();
					if(obj instanceof String)
						break;
					else if(obj instanceof TransactionalCLockRequest)
					{
						TransactionalCLockRequest request = (TransactionalCLockRequest) obj;
						cLockManager.addCLock(request.getCLockRequest().getTimestampedEntity(), 
								request.getTransactionId().getSequenceNumber());
					}
					else if(obj instanceof TimestampedEntity)
					{
						TimestampedEntity entity = (TimestampedEntity) obj;
						cLockManager.releaseCLock(entity);
					}
					else if(obj instanceof Set<?>)
					{
						@SuppressWarnings("unchecked")
						Set<Integer> failedPids = (Set<Integer>) obj;
						cLockManager.removeCLocks(failedPids);
					}
					else if(obj instanceof CSimpleLockGraphVertexKeeper)
					{
						CSimpleLockGraphVertexKeeper keeper = (CSimpleLockGraphVertexKeeper) obj;
						cLockManager.removeCLockKeeper(keeper);
					}
			} catch (Exception e) {
				RemoteLogger.getInstance(pid).log(Level.SEVERE, "Exception at " + name, e);
			}
		}
		onRunLeave();
	}

	
}
