package org.distribooty.lock.centralized;

import org.distribooty.TransactionID;

public class TransactionalCLockRequest {

	private CLockRequest cLockRequest;
	private TransactionID transactionId;
	public TransactionalCLockRequest(CLockRequest cLockRequest, TransactionID transactionId) {
		this.cLockRequest = cLockRequest;
		this.transactionId = transactionId;
	}
	
	public CLockRequest getCLockRequest() {
		return cLockRequest;
	}
	
	public TransactionID getTransactionId() {
		return transactionId;
	}
	
}
