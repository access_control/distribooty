package org.distribooty.lock.centralized;

import org.distribooty.util.Cleaner;

public class CSimpleLockGraphVertexKeeperFactory {
	
	private CSimpleLockGraphVertexKeeperMode mode;
	private Cleaner cleaner;
	private long liveTime;
	
	public CSimpleLockGraphVertexKeeperFactory(CSimpleLockGraphVertexKeeperMode mode, Cleaner cleaner, long liveTime) {
		this.mode = mode;
		this.cleaner = cleaner;
		this.liveTime = liveTime;
	}
	
	public CSimpleLockGraphVertexKeeper getCSimpleLockGraphVertexKeeper(String lockId) {
		switch(mode) {
		case MEMORY_SAVING:
			return new CSimpleLockGraphVertexKeeperMemorySaving(lockId, cleaner, liveTime);
		case FAST_APPEND:
			return new CSimpleLockGraphVertexKeeperFastAppend(lockId, cleaner, liveTime);
		default:
			return null;
		}
	}
}
