package org.distribooty.lock.centralized;

import org.distribooty.util.Cleaner;

public abstract class AbstractCSimpleLockGraphVertexKeeper implements CSimpleLockGraphVertexKeeper {

	protected final String lockId;
	protected Cleaner cleaner;
	private long liveTime;
	
	public AbstractCSimpleLockGraphVertexKeeper(String lockId, Cleaner cleaner, long liveTime) {
		this.lockId = lockId;
		this.cleaner = cleaner;
		this.liveTime = liveTime;
	}

	@Override
	public boolean resetCleanableStatus() {
		return true;
	}

	@Override
	public String getLockId() {
		return lockId;
	}

	@Override
	public long getLiveTime() {
		return liveTime;
	}
	
}
