package org.distribooty.lock.centralized.intern;

import org.distribooty.MasterChangeListener;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.lock.centralized.CRequestResult;

/**
 * 
 * Should be implemented by remote interface
 *
 */
public interface LocalCLockProvider {
	CRequestResult sendCLockRequest(TimestampedEntity timestampedEntity, MasterChangeListener masterChangeListener);
	boolean sendCLockRelease(long timestamp, MasterChangeListener masterChangeListener);
}
