package org.distribooty.lock.centralized.intern;

import java.util.List;

import org.distribooty.lock.centralized.CLockHandshake;

public interface LocalCLockAcquisitor {
	boolean receiveCLockApprove(long timestamp);
	List<CLockHandshake> getCLockHandshakes(boolean createEmptyContainerIfEmpty);
	void waitUntilAllCPrivilegedTasksDrained();
	void forceAllCPrivilegedTasksShutdown();
}
