package org.distribooty.lock.centralized.intern;

import java.util.List;

import org.distribooty.TransactionID;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.lock.centralized.CLockHandshake;
import org.distribooty.lock.centralized.CLockRequest;

public interface LocalCLockRequestHandler {
	void initCLockRequestHandler(List<CLockHandshake> cLockHandshakes);
	void activateApprovedHandshakes(List<CLockHandshake> cLockHandshakes);
	TransactionID handleCLockRequest(CLockRequest request);
	void handleCLockRelease(TimestampedEntity timestampedEntity);
	void waitUntilCLocksDrained();
	void shutdownCLockRequestHandler();
}
