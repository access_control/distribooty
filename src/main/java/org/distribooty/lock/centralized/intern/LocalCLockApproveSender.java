package org.distribooty.lock.centralized.intern;

import org.distribooty.lock.TimestampedEntity;

public interface LocalCLockApproveSender {
	boolean sendCLockApprove(TimestampedEntity timestampedEntity);
}
