package org.distribooty.lock.centralized;

import org.distribooty.lock.LockEntity;
import org.distribooty.util.Cleanable;

public interface CSimpleLockGraphVertexKeeper extends Cleanable {
	void acquire(LockEntity lockEntity, CLockGraphVertex vertex);
	String getLockId();
	public void recover();
}
