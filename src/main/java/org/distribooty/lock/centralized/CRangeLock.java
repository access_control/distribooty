package org.distribooty.lock.centralized;

import org.distribooty.lock.LockEntity;
import org.distribooty.util.ArrayUtils;
import org.distribooty.util.Range;

public class CRangeLock extends LockEntity implements Comparable<CRangeLock> {

	private static final long serialVersionUID = 5228737029950214176L;
	
	private Range<String> range;

	public CRangeLock(Range<String> range, boolean exclusive) {
		super(exclusive);
		this.range = range;
	}

	public Range<String> getRange() {
		return range;
	}

	@Override
	public int compareTo(CRangeLock o) {
		return range.compareTo(o.range);
	}
	
	public static CRangeLock[] computeRangeLocks(CSimpleLock[] locks) {
		try {
			ArrayUtils.checkArrayForEmptiness(locks);
		} catch(Exception e) {
			return null;
		}
		CRangeLock[] result = new CRangeLock[locks.length];
		int i = 0;
		for(CSimpleLock lock : locks)
			result[i++] = new CRangeLock(new Range<String>(lock.getLockId()), lock.isExclusive());
		return null;
	}
	
}
