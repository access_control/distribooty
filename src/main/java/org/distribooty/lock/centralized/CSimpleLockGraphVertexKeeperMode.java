package org.distribooty.lock.centralized;

public enum CSimpleLockGraphVertexKeeperMode {
	MEMORY_SAVING, FAST_APPEND;
}