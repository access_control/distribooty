package org.distribooty.lock.centralized;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.distribooty.lock.TimestampedEntity;
import org.distribooty.util.ArrayUtils;

public class TimestampedCSimpleLocks extends TimestampedEntity {

	private static final long serialVersionUID = 6256248782984205744L;
	
	private CSimpleLock cLocks[];
	
	public TimestampedCSimpleLocks(int srcPid, long timestamp, CSimpleLock cLocks[]) {
		super(srcPid, timestamp);
		this.cLocks = cLocks;
	}
	
	public TimestampedCSimpleLocks(int srcPid, long timestamp, CSimpleLock cLock) {
		this(srcPid , timestamp, new CSimpleLock[] {cLock});
	}
	
	public CSimpleLock[] getCSimpleLocks() {
		return cLocks;
	}
	
	public Map<String, CSimpleLock> getMappedCSimpleLocks() {
		Map<String, CSimpleLock> result = new HashMap<>();
		for(CSimpleLock lock : cLocks)
			result.put(lock.getLockId(), lock);
		return result;
	}
	
	public static CSimpleLock[] prepareForSending(CSimpleLock[] locks) {
		ArrayUtils.checkArrayForEmptiness(locks);
		Map<String, CSimpleLock> exclusiveLocks = new HashMap<>();
		Map<String, CSimpleLock> sharedLocks = new HashMap<>();
		for(CSimpleLock lock : locks)
		{
			String lockId = lock.getLockId();
			if(lock.isExclusive())
			{
				exclusiveLocks.put(lockId, lock);
				sharedLocks.remove(lockId);
			}
			else if(!exclusiveLocks.containsKey(lockId))
				sharedLocks.put(lockId, lock);
		}
		List<CSimpleLock> preparedLocks = new ArrayList<>(exclusiveLocks.values());
		preparedLocks.addAll(sharedLocks.values());
		return preparedLocks.toArray(new CSimpleLock[0]);
	}
	
	
}
