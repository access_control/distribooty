package org.distribooty.lock.centralized;

public class DefaultCLockGraphListener implements CLockGraphVertexListener {

	@Override
	public void onRelease(CLockGraphVertex vertex) {

	}

	@Override
	public void onIncommingVertexAdded(CLockGraphVertex incommingVertex, CLockGraphVertex targetVertex, boolean replacing) {

	}

	@Override
	public void onIncommingVertexRemoved(CLockGraphVertex incommingVertex, CLockGraphVertex targetVertex, boolean replacing) {

	}

	@Override
	public void onIncommingVertexReplaced(CLockGraphVertex oldIncommingVertex, CLockGraphVertex newIncommingVertex,
			CLockGraphVertex targetVertex) {

	}

	@Override
	public void onIncommingVertexReleased(CLockGraphVertex releasedIncommingVertex, CLockGraphVertex targetVertex) {

	}

	@Override
	public void onOutcommingVertexAdded(CLockGraphVertex outcommingVertex, CLockGraphVertex targetVertex, boolean replacing) {

	}

	@Override
	public void onOutcommingVertexRemoved(CLockGraphVertex outcommingVertex, CLockGraphVertex targetVertex, boolean replacing) {

	}

	@Override
	public void onOutcommingVertexReplaced(CLockGraphVertex oldOutcommingVertex, CLockGraphVertex newOutcommingVertex,
			CLockGraphVertex targetVertex) {

	}

	@Override
	public void onAllIncommingVerticesReleased(CLockGraphVertex vertex) {

	}

	@Override
	public void onRemove(CLockGraphVertex vertex) {

	}

}
