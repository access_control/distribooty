package org.distribooty.lock.centralized;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ScheduledExecutorService;

import org.distribooty.lock.LockEntity;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.lock.centralized.intern.LocalCLockApproveSender;
import org.distribooty.util.Cleaner;
import org.distribooty.util.ConcurrentUtils;

public class CLockManager {
	
	private CSimpleLockGraphVertexKeeperFactory factory;
	private Map<String, CSimpleLockGraphVertexKeeper> simpleCLockKeepers = new HashMap<>();
	private Map<TimestampedEntity, DefaultCLockGraphVertex> allLocks = new HashMap<>();
	private Set<DefaultCLockGraphVertex> rangeLocks = new TreeSet<>();
	private Set<DefaultCLockGraphVertex> simpleLocks = new TreeSet<>();
	private Set<DefaultCLockGraphVertex> approveCandidates = new TreeSet<>();
	private Map<Integer, Set<DefaultCLockGraphVertex>> pidToLockMap = new HashMap<>();
	private DefaultCLockGraphListener listener = new CLockManagerCLockVertexListener();
	private CLockHandler cLockHandler;
	private CLockApproveSender cLockApproveSender;
	private final LockEntity exclusiveLock = new LockEntity(true), sharedLock = new LockEntity(false);
	
	private class CLockManagerCLockVertexListener extends DefaultCLockGraphListener {

		@Override
		public void onIncommingVertexRemoved(CLockGraphVertex incommingVertex, CLockGraphVertex targetVertex, boolean replacing) {
			if(!replacing && targetVertex.isIncommingEmpty())
				CLockManager.this.approveCandidates.add((DefaultCLockGraphVertex) targetVertex);
		}

		@Override
		public void onAllIncommingVerticesReleased(CLockGraphVertex vertex) {
			DefaultCLockGraphVertex v = (DefaultCLockGraphVertex) vertex;
			CLockManager.this.cLockApproveSender.enqueue(v.getTimeStampedEntity());
		}

		@Override
		public void onRemove(CLockGraphVertex vertex) {
			remove((DefaultCLockGraphVertex) vertex);
		}
		
		@Override
		public void onRelease(CLockGraphVertex vertex) {
			remove((DefaultCLockGraphVertex) vertex);
		}

		private void remove(DefaultCLockGraphVertex v) {
			CLockManager.this.rangeLocks.remove(v);
			CLockManager.this.simpleLocks.remove(v);
			CLockManager.this.approveCandidates.remove(v);
			TimestampedEntity timestampedEntity = v.getTimeStampedEntity();
			CLockManager.this.allLocks.remove(timestampedEntity);
			Set<DefaultCLockGraphVertex> vertices = CLockManager.this.pidToLockMap.get(timestampedEntity.getSrcPid());
			if(vertices != null)
				vertices.remove(v);
			synchronized(CLockManager.this.allLocks)
			{
				if(CLockManager.this.allLocks.isEmpty())
					CLockManager.this.allLocks.notifyAll();
			}
		}
	}
	
	public CLockManager(List<CLockHandshake> cLockHandshakes,
			LocalCLockApproveSender localCLockApproveSender,
			ScheduledExecutorService executorService,
			CSimpleLockGraphVertexKeeperMode factoryMode,
			Cleaner cleaner,
			int pid,
			long simpleLockKeeperLiveTime) {
		this.factory = new CSimpleLockGraphVertexKeeperFactory(factoryMode, cleaner, simpleLockKeeperLiveTime);
		this.cLockHandler = new CLockHandler(this, pid);
		this.cLockApproveSender = new CLockApproveSender(localCLockApproveSender, this.cLockHandler, pid);
		executorService.execute(this.cLockHandler);
		executorService.execute(this.cLockApproveSender);
		for(CLockHandshake handshake : cLockHandshakes)		
			addCLock(handshake.getTimestampedEntity(), handshake.getSequenceNumber(), false);		
	}
	
	public void activateApprovedCLockHanshakes(List<CLockHandshake> cLockHandshakes) {
		for(CLockHandshake handshake : cLockHandshakes)
			if(!handshake.isApproved())
				this.allLocks.get(handshake.getTimestampedEntity()).tryIfAllIncommingVerticesReleased();
			else
				break;
	}
	
	public void onCLockRequest(TransactionalCLockRequest request) {
		this.cLockHandler.enqueue(request);
	}
	
	public void onCLockRelease(TimestampedEntity entity) {
		this.cLockHandler.enqueue(entity);
	}
	
	public void onRemotePeersFail(Set<Integer> failedPids) {
		this.cLockHandler.enqueue(failedPids);
	}
	
	public void onKeeperClean(CSimpleLockGraphVertexKeeper keeper) {
		this.cLockHandler.enqueue(keeper);
	}
	
	void addCLock(TimestampedEntity entity, long sequenceNumber) {
		addCLock(entity, sequenceNumber, true);
	}
	
	private void addCLock(TimestampedEntity entity, long sequenceNumber, boolean tryAllIncommingRelease) {
		DefaultCLockGraphVertex newVertex;
		if(entity instanceof TimestampedCSimpleLocks)
		{
			TimestampedCSimpleLocks locks = (TimestampedCSimpleLocks) entity;
			newVertex = new DefaultCLockGraphVertex(this.listener, entity, sequenceNumber);
			if(!this.rangeLocks.isEmpty())
			{
				Set<String> included = this.simpleCLockKeepers.keySet();
				Set<CSimpleLock> excluded = null;
				for(CSimpleLock lock : locks.getCSimpleLocks())
					if(!included.contains(lock.getLockId()))
					{
						if(excluded == null)
							excluded = new HashSet<>();
						excluded.add(lock);
					}
				if(excluded != null)
				{
					TimestampedCRangeLocks timestampedRangeLocks = new TimestampedCRangeLocks(excluded.toArray(new CSimpleLock[0]));
					for(DefaultCLockGraphVertex vertex : this.rangeLocks)				
						if(TimestampedCRangeLocks.isConflicted((TimestampedCRangeLocks) vertex.getTimeStampedEntity(), timestampedRangeLocks))
							vertex.addOutcommingVertex(newVertex);				
				}				
			}
			for(CSimpleLock lock : locks.getCSimpleLocks())			
				this.simpleCLockKeepers.computeIfAbsent(lock.getLockId(), key -> this.factory.getCSimpleLockGraphVertexKeeper(key)).acquire(lock, newVertex);
			addVertex(newVertex, tryAllIncommingRelease);
		}
		else if(entity instanceof TimestampedCRangeLocks)
		{
			TimestampedCRangeLocks locks = (TimestampedCRangeLocks) entity;
			newVertex = new DefaultCLockGraphVertex(this.listener, entity, sequenceNumber);
			for(DefaultCLockGraphVertex vertex : this.rangeLocks)			
				if(TimestampedCRangeLocks.isConflicted(locks, (TimestampedCRangeLocks) vertex.getTimeStampedEntity()))
					vertex.addOutcommingVertex(newVertex);			
			for(Map.Entry<String, CSimpleLockGraphVertexKeeper> entry : this.simpleCLockKeepers.entrySet())
			{
				String lockId = entry.getKey();
				if(locks.conflictsExclusive(lockId))
					entry.getValue().acquire(this.exclusiveLock, newVertex);
				else if(locks.conflictsShared(lockId))
					entry.getValue().acquire(this.sharedLock, newVertex);					
			}
			addVertex(newVertex, tryAllIncommingRelease);
		}
	}
	
	private void addVertex(DefaultCLockGraphVertex vertex, boolean tryAllIncommingRelease) {
		TimestampedEntity entity = vertex.getTimeStampedEntity();
		if(entity instanceof TimestampedCSimpleLocks)
			this.simpleLocks.add(vertex);
		else if(entity instanceof TimestampedCRangeLocks)
			this.rangeLocks.add(vertex);
		else
			return;
		this.pidToLockMap.computeIfAbsent(entity.getSrcPid(), key -> new HashSet<>()).add(vertex);
		this.allLocks.put(entity, vertex);
		if(tryAllIncommingRelease)
			vertex.tryIfAllIncommingVerticesReleased();
	}
	
	void releaseCLock(TimestampedEntity entity) {
		DefaultCLockGraphVertex vertex = this.allLocks.get(entity);
		if(vertex != null)
			vertex.release();
		else
			System.out.println("FALSE RELEASE pid = " + entity.getSrcPid() + " timestamp = " + entity.getTimestamp());
	}
	
	void removeCLocks(Set<Integer> failedPids) {
		Set<DefaultCLockGraphVertex> verticesToRemove = new HashSet<>();
		for(int pid : failedPids)
		{
			Set<DefaultCLockGraphVertex> vertices = this.pidToLockMap.remove(pid);
			if(vertices != null)
				verticesToRemove.addAll(vertices);
		}
		for(DefaultCLockGraphVertex vertex : verticesToRemove)
			vertex.remove();
		for(CSimpleLockGraphVertexKeeper keeper : this.simpleCLockKeepers.values())
			keeper.recover();
		for(DefaultCLockGraphVertex vertex : this.approveCandidates)
			vertex.tryIfAllIncommingVerticesReleased();
		this.approveCandidates.clear();
	}
	
	void removeCLockKeeper(CSimpleLockGraphVertexKeeper keeper) {
		String lockId = keeper.getLockId();
		CSimpleLockGraphVertexKeeper currentKeeper = this.simpleCLockKeepers.get(lockId);
		if(currentKeeper != null && currentKeeper.isCleanable())
			this.simpleCLockKeepers.remove(lockId);
	}
	
	public void waitForCLocksEmpty() {
		ConcurrentUtils.waitUninterruptible(allLocks);
	}
	
	public void shutdownCLockHandlerAndApproveSender() {
		this.cLockHandler.shutdown(true);
		this.cLockApproveSender.shutdown(true);
	}
	
	public void waitForCLockHandlerAndApproveSender() {
		this.cLockHandler.waitForShutdownComplete();
		this.cLockApproveSender.waitForShutdownComplete();
	}
	
	public void dumpCLocks() {
		if(!allLocks.values().isEmpty())
			for(DefaultCLockGraphVertex v : allLocks.values())
			{
				Set<CLockGraphVertex> in = v.getIncommingVertices();
				Set<CLockGraphVertex> out = v.getOutcommingVertices();
				int inc, outc;
				if(in != null)
					inc = in.size();
				else
					inc = 0;
				if(out != null)
					outc = out.size();
				else
					outc = 0;
				System.out.println("Waiting pid = " + v.getTimeStampedEntity().getSrcPid() + " timestamp = " + v.getTimeStampedEntity().getTimestamp() + " incommingCount = " + inc + " outcommingCount = " + outc);
			}
		else
			System.out.println("CLocks Empty");
	}
}
