package org.distribooty.lock.centralized;

import java.io.Serializable;

import org.distribooty.lock.TimestampedEntity;
import org.distribooty.util.LamportClock;

public class CLockHandshake implements Serializable, Comparable<CLockHandshake> {
	
	private static final long serialVersionUID = 2022130346316363300L;
	
	public static final long DELTA = 1_000_000_000L;
	private TimestampedEntity timestampedEntity;
	private long sequenceNumber;
	private boolean approved;
	
	public CLockHandshake(TimestampedEntity timestampedEntity, long  sequenceNumber, boolean approved) {
		this.timestampedEntity = timestampedEntity;
		this.sequenceNumber = sequenceNumber;
		this.approved = approved;
	}
	
	public TimestampedEntity getTimestampedEntity() {
		return timestampedEntity;
	}
	
	public Long getSequenceNumber() {
		return sequenceNumber;
	}
		
	public boolean isApproved() {
		return approved;
	}
	
	@Override
	public int compareTo(CLockHandshake o) {
		if(approved ^ o.approved)
		{
			if(approved)
				return -1;
			else
				return 1;
		}
		else
			return LamportClock.compare(sequenceNumber, o.sequenceNumber, DELTA);
	}		
}
