package org.distribooty.lock.centralized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.distribooty.lock.LockEntity;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.util.ArrayUtils;
import org.distribooty.util.Range;

public class TimestampedCRangeLocks extends TimestampedEntity {
	
	private static final long serialVersionUID = -7977523402187103823L;
	
	private CRangeLock[] exclusiveCRangeLocks;
	private CRangeLock[] sharedCRangeLocks;
	
	private void init(CRangeLock[][] locks) {
		CRangeLock[][] prepared = prepareForSending(locks[0], locks[1]);
		this.exclusiveCRangeLocks = prepared[0];
		this.sharedCRangeLocks = prepared[1];
	}

	public TimestampedCRangeLocks(int srcPid, long timestamp,
			CRangeLock[] exclusiveCRangeLocks, 
			CRangeLock[] sharedCRangeLocks) {
		super(srcPid, timestamp);
		if(ArrayUtils.isArrayEmpty(exclusiveCRangeLocks) && ArrayUtils.isArrayEmpty(sharedCRangeLocks))
			throw new IllegalArgumentException("both exclusive and/or shared CRangeLocks are null or empty");
		CRangeLock[][] prepared = prepareForSending(exclusiveCRangeLocks, sharedCRangeLocks);
		this.exclusiveCRangeLocks = prepared[0];
		this.sharedCRangeLocks = prepared[1];
	}
	
	public TimestampedCRangeLocks(int srcPid, long timestamp, CRangeLock cRangeLock) {
		this(srcPid, timestamp, 
				cRangeLock.isExclusive() ? new CRangeLock[] {cRangeLock} : null, 
				!cRangeLock.isExclusive() ? new CRangeLock[] {cRangeLock} : null);
	}
	
	public TimestampedCRangeLocks(CSimpleLock[] locks) {
		if(ArrayUtils.isArrayEmpty(locks))
			throw new IllegalArgumentException("The provided array is empty");
		locks = TimestampedCSimpleLocks.prepareForSending(locks);
		var sortedLocks = LockEntity.sortedLockEntitiesExclusiveAndShared(locks);
		var preparedLocks = prepareForSending(CRangeLock.computeRangeLocks(sortedLocks[0]), CRangeLock.computeRangeLocks(sortedLocks[1]));
		this.exclusiveCRangeLocks = preparedLocks[0];
		this.sharedCRangeLocks = preparedLocks[1];
	}
	
	public TimestampedCRangeLocks(int srcPid, long timestamp, CRangeLock[] cRangeLocks) {
		super(srcPid, timestamp);
		init(LockEntity.sortedLockEntitiesExclusiveAndShared(cRangeLocks));
	}
	
	public boolean conflictsExclusive(String lockId) {
		return isConflicted(exclusiveCRangeLocks, lockId);
	}
	
	public boolean conflictsShared(String lockId) {
		return isConflicted(sharedCRangeLocks, lockId);
	}
	
	public CRangeLock[] getExclusiveCRangeLocks() {
		return exclusiveCRangeLocks;
	}
	
	public CRangeLock[] getSharedCRangeLocks() {
		return sharedCRangeLocks;
	}
	
	/**
	 * Merge the overlapping ranges
	 * @param locks
	 * @return
	 */
	private static List<Range<String>> merge(CRangeLock[] locks) {
		if(ArrayUtils.isArrayEmpty(locks))
			return null;
		List<Range<String>> locksCopy = new ArrayList<>(), result = new ArrayList<>();
		for(CRangeLock lock : locks)
			locksCopy.add(lock.getRange());
		Iterator<Range<String>> it;
		while(!locksCopy.isEmpty())
		{
			Range<String> merged = locksCopy.remove(0);
			it = locksCopy.iterator();
			while(it.hasNext())
			{
				Range<String> t = it.next().merge(merged, false);
				if(t != null)
					{
						merged = t;
						it.remove();
					}
			}
			result.add(merged);
		}
		return result;
	}
	
	public static CRangeLock[][] prepareForSending(CRangeLock[] exclusiveCRangeLocks, CRangeLock[] sharedCRangeLocks) {
		
		boolean exclusiveEmpty = ArrayUtils.isArrayEmpty(exclusiveCRangeLocks);
		boolean inclusiveEmpty = ArrayUtils.isArrayEmpty(sharedCRangeLocks);		
		
		if(exclusiveEmpty && inclusiveEmpty)
			return null;
		
		List<Range<String>> exclusiveRanges = merge(exclusiveCRangeLocks),
				sharedRanges = merge(sharedCRangeLocks);
		
		if(!exclusiveEmpty && !inclusiveEmpty)
		{
			List<Range<String>> splittedRanges = new ArrayList<>();
			for(Range<String> exclusiveRange : exclusiveRanges)
			{
				Iterator<Range<String>> it = sharedRanges.iterator();
				while(it.hasNext())
				{
					Range<String>[] split = it.next().cut(exclusiveRange);
					if(split != null)
					{
						splittedRanges.addAll(Arrays.asList(split));
						it.remove();
					}				
				}
				sharedRanges.addAll(splittedRanges);
				splittedRanges.clear();
			}			
		}
		List<CRangeLock> _exclusiveCRangeLocks = null;
		if(!exclusiveEmpty)
		{
			Collections.sort(exclusiveRanges);
			_exclusiveCRangeLocks = new ArrayList<>();
			for(Range<String> range : exclusiveRanges)
				_exclusiveCRangeLocks.add(new CRangeLock(range, true));			
		}
		List<CRangeLock> _sharedCRangeLocks = null;
		if(!inclusiveEmpty)
		{
			_sharedCRangeLocks = new ArrayList<>();
			Collections.sort(sharedRanges);
			for(Range<String> range : sharedRanges)
				_sharedCRangeLocks.add(new CRangeLock(range, false));			
		}
		CRangeLock[][] result = new CRangeLock[2][];
		if(!exclusiveEmpty)
			result[0] = _exclusiveCRangeLocks.toArray(new CRangeLock[0]);
		if(!inclusiveEmpty)
			result[1] = _sharedCRangeLocks.toArray(new CRangeLock[0]);
		return result;
	}
	
	public static boolean conflict(CRangeLock[] locks1, CRangeLock[] locks2) {
		if(locks1 != null && locks2 != null && locks1.length > 0 && locks2.length > 0)
		{
			CRangeLock[] searchLocation;
			CRangeLock[] searchKeys;
			if(locks1.length > locks2.length)
			{
				searchLocation = locks1;
				searchKeys = locks2;
			}
			else
			{
				searchLocation = locks2;
				searchKeys = locks1;
			}
			for(CRangeLock searchKey : searchKeys)
				if(Arrays.binarySearch(searchLocation, searchKey) >= 0)
					return true;			
		}
		return false;
	}
	
	
	public static boolean isConflicted(TimestampedCRangeLocks locks1, TimestampedCRangeLocks locks2) {
		CRangeLock[] wLocks1 = locks1.getExclusiveCRangeLocks();
		CRangeLock[] wLocks2 = locks2.getExclusiveCRangeLocks();
		if(conflict(wLocks1, wLocks2))
			return true;
		CRangeLock[] rLocks2 = locks2.getSharedCRangeLocks();
		if(conflict(wLocks1, rLocks2))
			return true;
		CRangeLock[] rLocks1 = locks1.getSharedCRangeLocks();
		if(conflict(rLocks1, wLocks2))
			return true;
		return false;
	}
	
	public static boolean isConflicted(TimestampedCRangeLocks locks1, TimestampedCSimpleLocks locks2) {
		return isConflicted(locks1, new TimestampedCRangeLocks(locks2.getCSimpleLocks()));
	}
	
	private static boolean isConflicted(CRangeLock[] sortedRangeLocks, String lockId) {
		if(ArrayUtils.isArrayEmpty(sortedRangeLocks) || lockId == null)
			return false;
		else
			return Arrays.binarySearch(sortedRangeLocks, new Range<String>(lockId)) >= 0;
	}
	
}
