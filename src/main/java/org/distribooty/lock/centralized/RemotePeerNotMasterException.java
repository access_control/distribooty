package org.distribooty.lock.centralized;

public class RemotePeerNotMasterException extends Exception {

	private static final long serialVersionUID = 2144829744859312768L;

	public RemotePeerNotMasterException() {}

	public RemotePeerNotMasterException(String message) {
		super(message);
	}

	public RemotePeerNotMasterException(Throwable cause) {
		super(cause);
	}

	public RemotePeerNotMasterException(String message, Throwable cause) {
		super(message, cause);
	}

	public RemotePeerNotMasterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
