package org.distribooty.lock.centralized;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteCLockAcquisitor extends Remote {
	boolean receiveCLockApprove(long timestamp) throws RemoteException;
}
