package org.distribooty.lock.centralized;

import java.util.Collection;

import org.distribooty.lock.LockEntity;
import org.distribooty.util.Cleaner;

public class CSimpleLockGraphVertexKeeperFastAppend extends AbstractCSimpleLockGraphVertexKeeper {

	private TransparentLockGraphVertex head, tail;	
	
	@Override
	public void recover() {		
			TransparentLockGraphVertex t = head;
			do {
				if(!t.isIncommingEmpty())
					{
						Collection<CLockGraphVertex> incommingCopy = head.getIncommingInTraversalOrder();
						for(CLockGraphVertex v : incommingCopy)
						{
							t.exclusive = false;
							switch(v.getCollisionStatus(lockId))
							{
								case NON_COLLIDED:
									t.removeIncommingVertex(v);
									break;
								case EXCLUSIVE:
									t.exclusive = true;
								default:
									break;
							}
						}
					}				
				t = t.next;
			}while(t != null);		
	}

	private class TransparentLockGraphVertex extends DefaultCLockGraphVertexAndListener {
		
		private TransparentLockGraphVertex next, prev;
		boolean exclusive;		
		
		@Override
		public void onAllIncommingVerticesReleased(CLockGraphVertex vertex) {
			if(next != null)
			{
				release();
				head = next;
				head.prev = null;			
			}
			else
				exclusive = false;
		}		
	}
	
	public CSimpleLockGraphVertexKeeperFastAppend(String lockId, Cleaner cleaner, long liveTime) {
		super(lockId, cleaner, liveTime);
		head = tail = new TransparentLockGraphVertex();
	}
	
	@Override
	public void acquire(LockEntity lockEntity, CLockGraphVertex vertex) {
		TransparentLockGraphVertex t;
		if(lockEntity.isExclusive())
		{
			if(tail.exclusive || !tail.isIncommingEmpty())
			{
				t = new TransparentLockGraphVertex();
				tail.next = t;
				tail.addOutcommingVertex(vertex);
				vertex.addOutcommingVertex(t);
				tail = t;
			}
			else			
				vertex.addOutcommingVertex(tail);			
			tail.exclusive = true;
		}
		else
		{
			if(tail.exclusive)
			{
				t = new TransparentLockGraphVertex();
				tail.next = t;
				t.prev = tail;
				tail = t;
			}
			if(tail.prev != null)
				tail.prev.addOutcommingVertex(vertex);
			vertex.addOutcommingVertex(tail);
		}
		cleaner.reportUsage(this);
	}
	
	@Override
	public boolean isCleanable() {
		return !tail.exclusive && tail == head;
	}

}
