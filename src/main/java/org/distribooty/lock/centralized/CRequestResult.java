package org.distribooty.lock.centralized;

import java.io.Serializable;

import org.distribooty.TransactionID;

public class CRequestResult implements Serializable {

	private static final long serialVersionUID = -1787206329645710605L;

	private CRequestStatus lockRequestStatus;
	private TransactionID transactionId;
	
	public CRequestResult(CRequestStatus lockRequestStatus, TransactionID transactionId) {
		this.lockRequestStatus = lockRequestStatus;
		this.transactionId = transactionId;
	}
	
	public CRequestStatus getLockRequestStatus() {
		return lockRequestStatus;
	}
	
	public TransactionID getTransactionId() {
		return transactionId;
	}
}
