package org.distribooty.lock.centralized;

import java.util.concurrent.Future;

import org.distribooty.lock.PrivilegedTask;
import org.distribooty.lock.TimestampedEntity;

public interface CPrivilegedTask extends PrivilegedTask {
	void onStateChange(CPrivilegedTaskExecutionState currentState, 
			Future<TimestampedEntity> context);
	void onCancel(TimestampedEntity timestampedEntity, CPrivilegedTaskExecutionState lastState);
}
