package org.distribooty.lock.centralized;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public abstract class CLockGraphVertex {

	protected Set<CLockGraphVertex> incomming, outcomming;
	private boolean released = false;
	protected CLockGraphVertexListener listener;
	
	private Set<CLockGraphVertex> createVertexContainer() {
		return new HashSet<>();
	}
	
	protected Set<CLockGraphVertex> getIncommingVertices(boolean forceCreation) {
		if(forceCreation && incomming == null)
			incomming = createVertexContainer();
		return incomming;
	}
	
	protected Set<CLockGraphVertex> getOutcommingVertices(boolean forceCreation) {
		if(forceCreation && outcomming == null)
			outcomming = createVertexContainer();
		return outcomming;
	}
	
	protected CLockGraphVertex() {}
	
	public CLockGraphVertex(CLockGraphVertexListener listener) {
		Objects.requireNonNull(listener);
		this.listener = listener;
	}
	
	public boolean isIncommingEmpty() {
		return incomming == null || incomming.isEmpty();
	}

	public void addOutcommingVertex(CLockGraphVertex vertex) {
		Set<CLockGraphVertex> outcomming = getOutcommingVertices(true);
		if(outcomming.add(vertex))
			{
				listener.onOutcommingVertexAdded(vertex, this, false);
				vertex.addIncommingVertex(this);
			}
	}

	public void addIncommingVertex(CLockGraphVertex vertex) {
		Set<CLockGraphVertex> incomming = getIncommingVertices(true);
		if(incomming.add(vertex))
			{
				listener.onIncommingVertexAdded(vertex, this, false);
				vertex.addOutcommingVertex(this);
			}
	}
	
	public void removeIncommingVertex(CLockGraphVertex vertex) {
		if(incomming != null && incomming.remove(vertex))
			{
				listener.onIncommingVertexRemoved(vertex, this, false);
				vertex.removeOutcommingVertex(this);
			}
	}

	public void removeOutcommingVertex(CLockGraphVertex vertex) {
		if(outcomming != null && outcomming.remove(vertex))
			{
				listener.onOutcommingVertexRemoved(vertex, this, false);
				vertex.removeIncommingVertex(this);		
			}
	}
	
	public void replaceIncommingVertex(CLockGraphVertex vertexToReplace, CLockGraphVertex vertexToReplaceWith) {
		Set<CLockGraphVertex> in = getIncommingVertices(true);
		if(in.contains(vertexToReplace))
		{
			vertexToReplace.getOutcommingVertices(true).remove(this);
			getIncommingVertices(true).remove(vertexToReplace);
			vertexToReplaceWith.getOutcommingVertices(true).add(this);
			incomming.add(vertexToReplaceWith);
			listener.onOutcommingVertexRemoved(this, vertexToReplace, true);
			listener.onOutcommingVertexAdded(this, vertexToReplaceWith, true);
			listener.onIncommingVertexReplaced(vertexToReplace, vertexToReplaceWith, this);
		}
	}
	
	public void replaceOutcommingVertex(CLockGraphVertex vertexToReplace, CLockGraphVertex vertexToReplaceWith) {
		Set<CLockGraphVertex> out = getOutcommingVertices(true);
		if(out.contains(vertexToReplace))
		{
			vertexToReplace.getOutcommingVertices(true).remove(this);
			getOutcommingVertices(true).remove(vertexToReplace);
			vertexToReplaceWith.getIncommingVertices(true).add(this);
			outcomming.add(vertexToReplaceWith);
			listener.onIncommingVertexRemoved(this, vertexToReplace, true);
			listener.onIncommingVertexAdded(this, vertexToReplaceWith, true);
			listener.onOutcommingVertexReplaced(vertexToReplace, vertexToReplaceWith, this);
		}
	}

	private void onIncommingVertexReleased(CLockGraphVertex releasedVertex) {
		try {
			incomming.remove(releasedVertex);
			listener.onIncommingVertexReleased(releasedVertex, this);
		} catch (NullPointerException e) {
			
		}
		if(isIncommingEmpty())		
			listener.onAllIncommingVerticesReleased(this);		
	}
	
	public void release() {		
		if(isReleasable())
		{
			Collection<CLockGraphVertex> outcomming = getOutcommingInTraversalOrder();
			if(outcomming != null)
			{
				for(CLockGraphVertex vertex : outcomming)			
					vertex.onIncommingVertexReleased(this);	
				this.outcomming.clear();					
			}
			listener.onRelease(this);
			released = true;
		}
	}
	
	public void tryIfAllIncommingVerticesReleased() {
		onIncommingVertexReleased(null);
	}
	
	public void remove() {
		
		Set<CLockGraphVertex> in = getIncommingVertices(false), out = getOutcommingVertices(false);
		Set<CLockGraphVertex> copyIn = null, copyOut = null;
		if(in != null)
			copyIn = new HashSet<>(in);
		if(out != null)
			copyOut = new HashSet<>(out);
		
		if(copyIn != null && copyOut != null)
			for(CLockGraphVertex o : copyOut)		
				for(CLockGraphVertex i : copyIn)			
					i.addOutcommingVertex(o);
		
		if(copyIn != null)
			for(CLockGraphVertex i : copyIn)				
				removeIncommingVertex(i);
				
		
		if(copyOut != null)
			for(CLockGraphVertex o : copyOut)				
				removeOutcommingVertex(o);
		
		listener.onRemove(this);						
	}
	
	public Set<CLockGraphVertex> getIncommingVertices() {
		return incomming;
	}

	public Set<CLockGraphVertex> getOutcommingVertices() {
		return outcomming;
	}
	
	public Collection<CLockGraphVertex> getOutcommingInTraversalOrder() {
		return outcomming;
	}
	
	public Collection<CLockGraphVertex> getIncommingInTraversalOrder() {
		return incomming;
	}
	
	public boolean isReleasable() {
		return !released && isIncommingEmpty();
	}
	
	public boolean isReleased() {
		return released;
	}
	
	public abstract CLockCollisionStatus getCollisionStatus(String lockId);
	
}
