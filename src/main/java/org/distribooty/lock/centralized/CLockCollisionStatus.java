package org.distribooty.lock.centralized;

public enum CLockCollisionStatus {
	EXCLUSIVE, SHARED, NON_COLLIDED, NOT_REMOVE;
}
