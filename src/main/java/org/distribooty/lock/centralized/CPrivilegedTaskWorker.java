package org.distribooty.lock.centralized;

import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

import org.distribooty.MasterChangeListener;
import org.distribooty.debug.RemoteLogger;
import org.distribooty.lock.NonBlockingTimestampedEntityFuture;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.lock.centralized.intern.LocalCLockProvider;
import org.distribooty.util.AbstractWorker;
import org.distribooty.util.Cleanable;
import org.distribooty.util.Cleaner;

public class CPrivilegedTaskWorker extends AbstractWorker implements MasterChangeListener, Future<TimestampedEntity>, Cleanable {
	
	public static interface CPrivilegedTaskWorkerListener {
		void onCPrivilegedTaskWorker(CPrivilegedTaskWorker worker);
	}
	
	private CPrivilegedTask privilegedTask;
	private TimestampedEntity timestampedEntity;
	private CRequestResult requestResult;
	private LocalCLockProvider lockRequestor;
	private Cleaner cleaner;
	private volatile CPrivilegedTaskExecutionState state = CPrivilegedTaskExecutionState.LOCK_REQUESTING;
	private boolean lockApproved = false;
	private boolean async;
	private int reentrancyCounter = 0;
	private AwaitStatus awaitStatus = null;
	private CountDownLatch endLatch;
	private Throwable exception;
	
	public CPrivilegedTaskWorker(CPrivilegedTask privilegedTask, TimestampedEntity timeStampedCLocks,
			LocalCLockProvider lockRequestor, Cleaner cleaner, boolean async) {
		super(true, "CPrivilegedTaskWorker-" + timeStampedCLocks.getTimestamp());
		this.privilegedTask = privilegedTask;
		this.timestampedEntity = timeStampedCLocks;
		this.lockRequestor = lockRequestor;
		this.cleaner = cleaner;
		this.async = async;
		if(async)
			this.endLatch = new CountDownLatch(1);
	}
	
	public boolean onLockApproved() {
		synchronized(lock)
		{
			if(isDone())
				return false;
			lockApproved = true;
			wakeUp();
			return true;
		}
	}	
	
	public void incrementReentrancyCounter() {
		reentrancyCounter++;
	}
	
	public void decrementReentrancyCounter() {
		if(reentrancyCounter == 0)
			throw new IllegalStateException("Reentrancy counter is already 0");
		reentrancyCounter--;
	}
	
	public int getReentrancyCounter() {
		return reentrancyCounter;
	}
	
	public AwaitStatus getLastAwaitStatus() {
		return awaitStatus;
	}
	
	@Override
	public void run() {
		
		if(async)
			onRunEnter();
		boolean skipAwait = false;
		NonBlockingTimestampedEntityFuture decorator = new NonBlockingTimestampedEntityFuture(this.timestampedEntity);
		end_the_loop:
		while(true)
		{
			try {
				synchronized(lock)
				{
					if(isCancelled())
					{
						privilegedTask.onCancel(timestampedEntity, state);
						break;
					}
					
					if(!skipAwait)
						awaitStatus = await(getTimeout());
						
					if(awaitStatus != AwaitStatus.NOTIFIED && !async)
						break;
								
					switch(state) {				
						case LOCK_REQUESTING:
							privilegedTask.onStateChange(state, decorator);
							requestResult = lockRequestor.sendCLockRequest(timestampedEntity, this);
							switch(requestResult.getLockRequestStatus())
							{
								case CLOCK_REQUEST_SENT:
									privilegedTask.onStateChange(state = CPrivilegedTaskExecutionState.LOCK_AWAITING_APPROVE, decorator);
									break;
								case MASTER_UNAVAILABLE:
									privilegedTask.onStateChange(CPrivilegedTaskExecutionState.MASTER_UNAVAILABLE, decorator);
									break;
								case CLOCK_REQUEST_NOT_SENT:
									privilegedTask.onStateChange(CPrivilegedTaskExecutionState.LOCK_REQUEST_NOT_SENT, decorator);
									break;
							}					
							break;
							
						case LOCK_AWAITING_APPROVE:
							if(lockApproved)						
								privilegedTask.onStateChange(state = CPrivilegedTaskExecutionState.TASK_EXECUTING, decorator);						
							break;
							
						case TASK_EXECUTING:
							privilegedTask.onStateChange(state = CPrivilegedTaskExecutionState.LOCK_RELEASING, decorator);
							skipAwait = false;
						case LOCK_RELEASING:
							if(lockRequestor.sendCLockRelease(timestampedEntity.getTimestamp(), this))
							{
								privilegedTask.onStateChange(state = CPrivilegedTaskExecutionState.TERMINATED, decorator);
								break end_the_loop;
							}
							privilegedTask.onStateChange(CPrivilegedTaskExecutionState.LOCK_RELEASING_FAILED, decorator);						
							break;
							
						default:
							break;
					}
					if(state != CPrivilegedTaskExecutionState.TASK_EXECUTING)
						continue;
					else if(!async)					
						return;					
				}
				privilegedTask.execute(timestampedEntity, requestResult.getTransactionId());
				skipAwait = true;
			} catch (Throwable e) {
				exception = e;
				RemoteLogger.getInstance(pid).log(Level.SEVERE, "", e);
			}
		}
		if(awaitStatus != AwaitStatus.NOTIFIED && !async)
			return;
		onRunLeave();
	}

	public TimestampedEntity getTimestampedEntity() {
		return timestampedEntity;
	}
	
	public boolean isLockApproved() {
		synchronized(lock)
		{
			return lockApproved;
		}
	}

	@Override
	public void shutdown(boolean graceful) {
		cancel(false);
	}

	@Override
	public void onMasterLost(int masterPid) {
		RemoteLogger.getInstance(0).info(name + " master lost detected on " + masterPid);
	}

	@Override
	public void onMasterChange(int masterPid) {
		RemoteLogger.getInstance(0).info(name + " master change detected on " + masterPid);
		wakeUp();
	}
	
	public CPrivilegedTaskExecutionState getState() {
		synchronized(lock)
		{
			return state;			
		}
	}

	public CRequestResult getLockResult() {
		return requestResult;
	}

	/**
	 * Call this method only if the master is unavailable and there is no chance a new master will be charged in near time.
	 */
	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		synchronized(lock)
		{
			if(		isDone() || 
					!mayInterruptIfRunning && 
					state.compareTo(CPrivilegedTaskExecutionState.LOCK_AWAITING_APPROVE) >= 0 && 
					lockApproved && 
					state.compareTo(CPrivilegedTaskExecutionState.TASK_EXECUTING) <= 0)
				return false;
			else
			{
				shutdownFlag = true;
				wakeUp();
				return true;
			}
		}		
	}

	@Override
	public boolean isCancelled() {
		synchronized(lock)
		{
			return shutdownFlag;
		}
	}

	@Override
	public boolean isDone() {
		synchronized(lock)
		{
			return isCancelled() || state == CPrivilegedTaskExecutionState.TERMINATED;
		}
	}

	@Override
	public TimestampedEntity get() throws InterruptedException, ExecutionException {
		if(async)
			this.endLatch.await();
		if(isCancelled())
			throw new CancellationException("Task has been cancelled");
		if(exception != null)
			throw new ExecutionException(exception);
		return timestampedEntity;		
	}
	
	@Override
	public TimestampedEntity get(long timeout, TimeUnit unit)
			throws InterruptedException, ExecutionException, TimeoutException {
		if(async && !endLatch.await(timeout, unit))
			throw new TimeoutException("Timeout elapsed");
		if(isCancelled())
			throw new CancellationException("Task has been cancelled");
		if(exception != null)
			throw new ExecutionException(exception);
		return timestampedEntity;		
	}

	@Override
	public boolean isCleanable() {
		synchronized(lock)
		{
			return state == CPrivilegedTaskExecutionState.TERMINATED;			
		}
	}

	@Override
	public boolean resetCleanableStatus() {
		synchronized(lock)
		{
			if(state == CPrivilegedTaskExecutionState.TERMINATED)
				return false;
			else
				return true;			
		}
	}
	
	public void forceClean() {
		this.cleaner.clean(this);
	}
	
	@Override
	protected void onRunLeave() {
		forceClean();
		super.onRunLeave();
		if(async)
			this.endLatch.countDown();
	}

	@Override
	public long getLiveTime() {
		return Long.MAX_VALUE;
	}

}
