package org.distribooty.lock.centralized;

import java.io.Serializable;

import org.distribooty.lock.TimestampedEntity;

public class CLockRequest implements Serializable {

	private static final long serialVersionUID = -8389905625839775011L;
	
	private int dstPid;
	private TimestampedEntity timestampedEntity;
	
	public CLockRequest(int dstPid, TimestampedEntity timestampedEntity) {
		this.dstPid = dstPid;
		this.timestampedEntity = timestampedEntity;
	}
	
	public int getSrcPid() {
		return timestampedEntity.getSrcPid();
	}
	
	public int getDstPid() {
		return dstPid;
	}
	
	public TimestampedEntity getTimestampedEntity() {
		return timestampedEntity;
	}
}
