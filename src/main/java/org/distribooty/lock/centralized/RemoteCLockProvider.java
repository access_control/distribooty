package org.distribooty.lock.centralized;

import java.rmi.RemoteException;

import org.distribooty.TransactionID;
import org.distribooty.lock.TimestampedEntity;

public interface RemoteCLockProvider {
	TransactionID receiveCLockRequest(CLockRequest cLockRequest) throws RemoteException;
	void receiveCLockRelease(TimestampedEntity timestampedEntity) throws RemoteException;
}
