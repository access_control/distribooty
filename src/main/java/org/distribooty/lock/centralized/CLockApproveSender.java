package org.distribooty.lock.centralized;

import java.util.logging.Level;

import org.distribooty.debug.RemoteLogger;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.lock.centralized.intern.LocalCLockApproveSender;
import org.distribooty.util.AbstractConsumer;

public class CLockApproveSender extends AbstractConsumer<TimestampedEntity> {

	private LocalCLockApproveSender lockApproveSender;
	private AbstractConsumer<Object> cLockHandlerConsumer;
	
	public CLockApproveSender(LocalCLockApproveSender lockApproveSender, AbstractConsumer<Object> cLockHandlerConsumer, int pid) {
		super(new TimestampedEntity(), "CLockApproveSender-Thread-" + pid);
		this.lockApproveSender = lockApproveSender;
		this.cLockHandlerConsumer = cLockHandlerConsumer;
	}

	
	@Override
	public void run() {
		onRunEnter();		
		while(true)
		{
			try {
					TimestampedEntity entity = dequeue();
					if(entity.getTimestamp() == -1L)
						break;
					if(!lockApproveSender.sendCLockApprove(entity))
						cLockHandlerConsumer.enqueue(entity);
			} catch (Exception e) {
				RemoteLogger.getInstance(pid).log(Level.FINEST, "Exception at " + name, e);
			}
		}
		onRunLeave();
	}		
}
