package org.distribooty.lock.centralized;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.distribooty.lock.LockEntity;
import org.distribooty.util.Cleaner;

public class CSimpleLockGraphVertexKeeperMemorySaving extends AbstractCSimpleLockGraphVertexKeeper {
	
	private CLockGraphVertexHolder head;
	
	@Override
	public void recover() {		
			if(!head.isIncommingEmpty())
			{
				head.exclusiveLock = null;
				Collection<CLockGraphVertex> incommingCopy = head.getIncommingInTraversalOrder();
				for(CLockGraphVertex v : incommingCopy)
					switch(v.getCollisionStatus(lockId))
					{
						case EXCLUSIVE:
							head.exclusiveLock = v;
							break;
						case SHARED:
						case NOT_REMOVE:
							break;
						case NON_COLLIDED:
							head.removeIncommingVertex(v);
							break;
					}			
			}	
	}
	
	private class CLockGraphVertexHolder extends DefaultCLockGraphVertexAndListener {
		
		private CLockGraphVertex exclusiveLock;
		
		@Override
		public void onIncommingVertexReleased(CLockGraphVertex releasedIncommingVertex, CLockGraphVertex targetVertex) {
			if(releasedIncommingVertex == exclusiveLock)
				exclusiveLock = null;
		}

		public void addExclusive(CLockGraphVertex vertex) {
			if(!isIncommingEmpty())
			{
				Set<CLockGraphVertex> incommingCopy = new HashSet<>(incomming);
				for(CLockGraphVertex v : incommingCopy)				
					v.replaceOutcommingVertex(this, vertex);
				
			}
			vertex.addOutcommingVertex(this);
			exclusiveLock = vertex;			
		}
		
		public void addShared(CLockGraphVertex vertex) {
			if(exclusiveLock != null)
				exclusiveLock.addOutcommingVertex(vertex);			
			vertex.addOutcommingVertex(this);
		}
		
	}

	public CSimpleLockGraphVertexKeeperMemorySaving(String lockId, Cleaner cleaner, long liveTime) {
		super(lockId, cleaner, liveTime);
		head = new CLockGraphVertexHolder();
	}
	
	@Override
	public void acquire(LockEntity lockEntity, CLockGraphVertex vertex) {
		if(lockEntity.isExclusive())		
			head.addExclusive(vertex);		
		else		
			head.addShared(vertex);
		cleaner.reportUsage(this);
	}

	@Override
	public boolean isCleanable() {
		return head.isIncommingEmpty();
	}
}
