package org.distribooty.lock.centralized;

import org.distribooty.lock.LockEntity;

public class CSimpleLock extends LockEntity {
	
	private static final long serialVersionUID = -3465695968597254479L;
	
	private String lockId;
	
	public CSimpleLock(String lockId, boolean exclusive) {
		super(exclusive);
		this.lockId = lockId;
	}

	public String getLockId() {
		return lockId;
	}
		
}
