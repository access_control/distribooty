package org.distribooty.lock.centralized;

public interface CLockGraphVertexListener {
	void onRelease(CLockGraphVertex vertex);
	void onIncommingVertexAdded(CLockGraphVertex incommingVertex, CLockGraphVertex targetVertex, boolean replacing);
	void onIncommingVertexRemoved(CLockGraphVertex incommingVertex, CLockGraphVertex targetVertex, boolean replacing);
	void onIncommingVertexReplaced(CLockGraphVertex oldIncommingVertex, CLockGraphVertex newIncommingVertex, CLockGraphVertex targetVertex);
	void onIncommingVertexReleased(CLockGraphVertex releasedIncommingVertex, CLockGraphVertex targetVertex);
	void onOutcommingVertexAdded(CLockGraphVertex outcommingVertex, CLockGraphVertex targetVertex, boolean replacing);
	void onOutcommingVertexRemoved(CLockGraphVertex outcommingVertex, CLockGraphVertex targetVertex, boolean replacing);
	void onOutcommingVertexReplaced(CLockGraphVertex oldOutcommingVertex, CLockGraphVertex newOutcommingVertex, CLockGraphVertex targetVertex);
	void onAllIncommingVerticesReleased(CLockGraphVertex vertex);
	void onRemove(CLockGraphVertex vertex);
}
