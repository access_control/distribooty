package org.distribooty.lock;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

import org.distribooty.TransactionID;
import org.distribooty.lock.centralized.CPrivilegedTask;
import org.distribooty.lock.centralized.CPrivilegedTaskExecutionState;
import org.distribooty.lock.centralized.CPrivilegedTaskWorker;
import org.distribooty.lock.intern.SyncCPrivilegedTaskWorkerCreator;
import org.distribooty.util.AbstractWorker.AwaitStatus;
import org.distribooty.util.ConcurrentUtils;

public class SyncCLock implements Lock {
	
	private static final CPrivilegedTask NO_OP_TASK = new CPrivilegedTask() {
		
		@Override
		public void execute(TimestampedEntity timestampedEntity, TransactionID transactionId) throws Exception {
			
		}
		
		@Override
		public void onStateChange(CPrivilegedTaskExecutionState currentState, Future<TimestampedEntity> context) {
			
		}
		
		@Override
		public void onCancel(TimestampedEntity timestampedEntity, CPrivilegedTaskExecutionState lastState) {
			
		}
	};
	
	private SyncCPrivilegedTaskWorkerCreator creator;
	private TimestampedEntity timestampedEntity;
	private CPrivilegedTaskWorker worker;
	private Thread currentThread;
	
	
	public SyncCLock(SyncCPrivilegedTaskWorkerCreator creator, TimestampedEntity timestampedEntity) {
		this.creator = creator;
		this.timestampedEntity = timestampedEntity;
	}
	
	private boolean tryCancelWorker() {
		if(this.worker.cancel(false))
		{
			this.worker.forceClean();
			this.worker = null;
			this.currentThread = null;
			return true;
		}
		else
			return false;
	}
	
	private synchronized boolean acquireLock(long time, TimeUnit unit, boolean interruptible, boolean trying) throws InterruptedException {
		Thread thread = Thread.currentThread();
		while(true)
		{			
			if(this.currentThread == thread)
			{
				if(this.worker.getState() == CPrivilegedTaskExecutionState.TASK_EXECUTING)
				{
					this.worker.incrementReentrancyCounter();
					return true;
				}
				else
				{
					this.worker.setTimeout(ConcurrentUtils.computeMillis(time, unit));
					this.worker.run();
					AwaitStatus awaitStatus = this.worker.getLastAwaitStatus();
					if(awaitStatus == AwaitStatus.INTERRUPTED && interruptible)
						{
							if(tryCancelWorker())
								throw new InterruptedException();
						}
					else if(awaitStatus == AwaitStatus.TIMEOUTED)
						{
							if(tryCancelWorker())
								return false;
						}
					else if(awaitStatus == AwaitStatus.NOTIFIED)
					{
						this.worker.incrementReentrancyCounter();
						return true;
					}
				}
			} else if(this.currentThread == null)
			{
				this.worker = this.creator.getCPrivilegedTaskWorker(timestampedEntity, NO_OP_TASK);
				if(this.worker == null)
					throw new IllegalStateException("Lock is no more acquireble");
				this.currentThread = thread;
			} else
			{
				if(trying)
					return false;
				try {
					this.wait(ConcurrentUtils.computeMillis(time, unit));
				} catch (InterruptedException e) {
					if(interruptible)
						throw e;
				}
			}
		}
	}
	
	@Override
	public void lock() {
		try {
			acquireLock(0L, null, false, false);
		} catch (InterruptedException e) {
			// Should never happens
		}		
	}

	@Override
	public void lockInterruptibly() throws InterruptedException {
		acquireLock(0L, null, true, false);
	}

	@Override
	public boolean tryLock() {
		try {
			return acquireLock(0L, null, false, true);
		} catch (InterruptedException e) {
			return false; // Should never happens
		}
	}

	@Override
	public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
		return acquireLock(time, unit, true, false);
	}

	@Override
	public void unlock() {
		synchronized(this)
		{
			Thread thread = Thread.currentThread();
			if(this.currentThread == thread)
			{
				this.worker.decrementReentrancyCounter();
				if(this.worker.getReentrancyCounter() == 0)
					{
						this.worker.setTimeout(0L);
						AwaitStatus awaitStatus = null;
						while(awaitStatus != AwaitStatus.NOTIFIED)
							{
								this.worker.run();
								awaitStatus = this.worker.getLastAwaitStatus();
							}
						this.currentThread = null;
						this.notify();
					}
			}
			else			
				throw new IllegalStateException("The calling thread is not owning the lock");
			
		}
	}

	@Override
	public Condition newCondition() {
		throw new UnsupportedOperationException("No conditions on SyncCLock object possible");
	}

}
