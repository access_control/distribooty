package org.distribooty.lock;

import java.io.Serializable;

import org.distribooty.util.ArrayUtils;

public class LockEntity implements Serializable {

	private static final long serialVersionUID = -2673812618863808077L;
	
	private boolean exclusive;
	
	public LockEntity(boolean exclusive) {
		this.exclusive = exclusive;
	}
	
	public boolean isExclusive() {
		return exclusive;
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends LockEntity> T[][] sortedLockEntitiesExclusiveAndShared(T[] locks) {
		try {
			ArrayUtils.checkArrayForEmptiness(locks);
		} catch(Exception e) {
			return null;
		}
		int exclusive = 0, shared = 0;
		Object[][] result = new Object[2][];
		for(T lock : locks)
			if(lock.isExclusive())
				exclusive++;
			else
				shared++;
		if(exclusive > 0)
			result[0] = new Object[exclusive];
		if(shared > 0)
			result[1] = new Object[shared];
		exclusive = shared = 0;
		for(T lock : locks)
			if(lock.isExclusive())
				result[0][exclusive++] = lock;
			else
				result[1][shared++] = lock;
		return (T[][]) result;
	}

}
