package org.distribooty.lock;

import org.distribooty.TransactionID;

public interface PrivilegedTask {
	void execute(TimestampedEntity timestampedEntity, TransactionID transactionId) throws Exception;
}
