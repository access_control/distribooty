package org.distribooty.lock.decentralized;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.logging.Level;

import org.distribooty.RemotePeerFailureListener;
import org.distribooty.TransactionID;
import org.distribooty.debug.RemoteLogger;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.lock.decentralized.intern.LocalDLockReceiver;
import org.distribooty.lock.decentralized.intern.LocalDLockResponseSender;
import org.distribooty.lock.decentralized.intern.LocalDLockSenderListener;
import org.distribooty.util.AbstractWorker;
import org.distribooty.util.Cleanable;
import org.distribooty.util.Cleaner;

public class DPrivilegedTaskWorker extends AbstractWorker implements LocalDLockReceiver, LocalDLockSenderListener, Cleanable, RemotePeerFailureListener {
	
	//public static final long WAIT_TIMEOUT = 5000L;
	
	private class DLockResponseSender implements DPrivilegedTask {

		private DLockUnicastRequest request;		
		
		public void setDLockUnicastRequest(DLockUnicastRequest request) {
			this.request = request;
		}

		@Override
		public void execute(TimestampedEntity timestampedEntity, TransactionID transactionId) throws Exception {
			dLockResponser.sendDLockResponse(new DLockUnicastResponse(request));			
		}

		@Override
		public void onTimeout(TimestampedEntity timestampedEntity, Future<TimestampedEntity> context) {
			
		}

		@Override
		public void onCancel(TimestampedEntity timestampedEntity) {
			
		}		
	}
	
	private String lockId;	
	private Queue<DPrivilegedTaskWrapper> pendingPrivilegedTasks = new ArrayDeque<>();
	private Queue<DLockMulticastRequest> pendingSentDLockRequests = new ArrayDeque<>();
	private Map<Long, DLockMulticastRequest> trackedPendingSentDLockRequests = new HashMap<>();
	private PriorityQueue<DLockUnicastRequest> pendingReceivedDLockRequests = new PriorityQueue<>();
	private DLockResponseSender responseSender = new DLockResponseSender();
	private LocalDLockResponseSender dLockResponser;
	private Cleaner cleaner;
	private boolean shutdown = false;
	private DPrivilegedTask privilegedTask;
	private long liveTime;
	private long timeout;

	public DPrivilegedTaskWorker(String lockId, LocalDLockResponseSender dLockResponser, Cleaner cleaner, long liveTime, long timeout) {
		super(false, "DPrivilegedTaskWorker-" + lockId);
		this.lockId = lockId;
		this.dLockResponser = dLockResponser;
		this.cleaner = cleaner;
		this.liveTime = liveTime;
		this.timeout = timeout;
	}
	
	

	@Override
	public void run() {
		
		onRunEnter();
		
		DPrivilegedTaskWrapper privilegedTaskWrapper = null;
		DLockUnicastRequest lockUnicastRequest = null;
		DLockMulticastRequest lockMulticastRequest = null;
		long timeout = 0L;
		boolean skipAwait = false;
		while(true)
		{
			try {
				synchronized(lock)
				{
					privilegedTask = null;
					AwaitStatus awaitStatus = null;
					if(!skipAwait)
						awaitStatus = await(timeout);
					// No pending and sent and received requests
					if(isEmpty())
					{
						if(shutdownFlag)							
						{
							shutdown = true;
							break;
						}
						timeout = 0L;
						skipAwait = false;
						continue;
					}
					else if(pendingSentDLockRequests.isEmpty() || 
							(!pendingReceivedDLockRequests.isEmpty() && 
							pendingReceivedDLockRequests.peek().compareTo(pendingSentDLockRequests.peek()) < 0))
					{
						lockUnicastRequest = pendingReceivedDLockRequests.remove();
						responseSender.setDLockUnicastRequest(lockUnicastRequest);
						privilegedTask = responseSender;
						skipAwait = true;
						//timeout = WAIT_TIMEOUT;
						timeout = this.timeout;
					}
					else if(pendingSentDLockRequests.peek().allResponsesRetrieved() ||
							pendingPrivilegedTasks.peek().isCancelled())
					{
						lockMulticastRequest = pendingSentDLockRequests.remove();
						privilegedTaskWrapper = pendingPrivilegedTasks.remove();
						trackedPendingSentDLockRequests.remove(lockMulticastRequest.getTimestamp());
						skipAwait = true;
						//timeout = WAIT_TIMEOUT;
						timeout = this.timeout;
						if(!privilegedTaskWrapper.isCancelled())						
							privilegedTask = privilegedTaskWrapper;					
						else
						{
							privilegedTaskWrapper.onCancel(lockMulticastRequest);
							continue;
						}
					}
					else
					{
						// If timeout
						if(awaitStatus == AwaitStatus.TIMEOUTED)
						{
							Iterator<DLockMulticastRequest> lockRequestIterator = pendingSentDLockRequests.iterator();
							Iterator<DPrivilegedTaskWrapper> privilegedTaskWrapperIterator = pendingPrivilegedTasks.iterator();
							for(; lockRequestIterator.hasNext() && privilegedTaskWrapperIterator.hasNext();)
							{
								lockMulticastRequest = lockRequestIterator.next();
								privilegedTaskWrapper = privilegedTaskWrapperIterator.next();
								privilegedTask = privilegedTaskWrapper;
								privilegedTask.onTimeout(lockMulticastRequest, privilegedTaskWrapper);
							}
							RemoteLogger.getInstance(pid).info("Timeout Deadlock DWorkerTask");
						}
						//timeout = WAIT_TIMEOUT;
						timeout = this.timeout;
						skipAwait = false;
						continue;
					}
				}
				cleaner.reportUsage(this);
				if(privilegedTask != null)
					privilegedTask.execute(lockMulticastRequest, null);
			} catch (Exception e) {
				RemoteLogger.getInstance(pid).log(Level.SEVERE, "Exception at " + name, e);
			}
		}
		onRunLeave();
	}
	
	@Override
	protected void onRunLeave() {
		cleaner.clean(this);
		super.onRunLeave();
	}

	@Override
	public void receiveDLockResponse(DLockUnicastResponse lockResponse) {
		synchronized(lock)
		{
			try {
				DLockMulticastRequest request = trackedPendingSentDLockRequests.get(lockResponse.getTimestamp());
				request.registerResponse(lockResponse.getSrcPid());
				if(request.allResponsesRetrieved())
					wakeUp();
			} catch(NullPointerException e) {
				
			}
		}
	}
	
	@Override
	public boolean receiveDLockRequest(DLockUnicastRequest lockRequest) {
		synchronized(lock)
		{
			if(shutdownFlag && pendingSentDLockRequests.isEmpty() && (privilegedTask == null || privilegedTask instanceof DLockResponseSender))
				return false;
			else
			{
				pendingReceivedDLockRequests.add(lockRequest);
				wakeUp();
				return true;
			}
		}
	}

	@Override
	public void onRemotePeerFail(Set<Integer> unavailablePids) {
		synchronized(lock)
		{
			pendingSentDLockRequests.forEach(request -> {
				request.registerResponses(unavailablePids);
			});
			wakeUp();
		}		
	}
	
	public String getLockId() {
		return lockId;
	}
	
	private boolean isEmpty() {
		synchronized(lock)
		{
			return pendingSentDLockRequests.isEmpty() && pendingReceivedDLockRequests.isEmpty();
		}
	}

	@Override
	public void onBeforeSendDLockRequest(DLockMulticastRequest request, DPrivilegedTaskWrapper privilegedTask) {
		synchronized(lock)
		{
			pendingPrivilegedTasks.add(new DPrivilegedTaskWrapper(privilegedTask));
			pendingSentDLockRequests.add(request);
			trackedPendingSentDLockRequests.put(request.getTimestamp(), request);
			wakeUp();
		}
		
	}

	@Override
	public void onAfterSendDLockRequest(Set<Integer> pidsResponsesFrom, long timestamp) {
		synchronized(lock)
		{
			try {
				DLockMulticastRequest request = trackedPendingSentDLockRequests.get(timestamp);
				request.registerResponses(pidsResponsesFrom);
				if(request.allResponsesRetrieved())
					wakeUp();
			} catch(NullPointerException e) {
				
			}
		}	
		
	}

	@Override
	public boolean isCleanable() {
		synchronized(lock)
		{
			return shutdown || isEmpty();
		}
	}

	@Override
	public boolean resetCleanableStatus() {
		synchronized(lock)
		{
			if(shutdown)
				return false;
			if(shutdownFlag)
				shutdownFlag = false;
			return true;
		}
	}

	@Override
	public long getLiveTime() {
		return liveTime;
	}
	
	

}
