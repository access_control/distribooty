package org.distribooty.lock.decentralized;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteDLockAware extends Remote {
	boolean receiveDLockRequest(DLockUnicastRequest dLockRequest) throws RemoteException;
	void receiveDLockResponse(DLockUnicastResponse dLockResponse) throws RemoteException;	
}
