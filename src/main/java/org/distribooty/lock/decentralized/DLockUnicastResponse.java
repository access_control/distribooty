package org.distribooty.lock.decentralized;

public class DLockUnicastResponse extends DLockUnicastRequest {

	private static final long serialVersionUID = -5107235951090558287L;

	public DLockUnicastResponse(DLockUnicastRequest lockRequest) {
		super(lockRequest.getLockId(), lockRequest.getTimestamp(), lockRequest.getDstPid(), lockRequest.getSrcPid());
	}
}
