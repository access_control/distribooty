package org.distribooty.lock.decentralized;

import java.util.concurrent.Future;

import org.distribooty.lock.PrivilegedTask;
import org.distribooty.lock.TimestampedEntity;

public interface DPrivilegedTask extends PrivilegedTask {
	void onTimeout(TimestampedEntity timestampedEntity, Future<TimestampedEntity> context);
	void onCancel(TimestampedEntity timestampedEntity);
}
