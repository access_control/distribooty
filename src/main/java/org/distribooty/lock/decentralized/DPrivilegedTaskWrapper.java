package org.distribooty.lock.decentralized;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.distribooty.TransactionID;
import org.distribooty.lock.TimestampedEntity;

public class DPrivilegedTaskWrapper implements Future<TimestampedEntity>, DPrivilegedTask {
	
	// TODO Future correct
	
	private volatile boolean canceled = false;
	private volatile boolean done = false;
	
	private DPrivilegedTask dPrivilegedTask;
	
	public DPrivilegedTaskWrapper(DPrivilegedTask privilegedTask) {
		dPrivilegedTask = privilegedTask;
	}
		
	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		if(canceled || done)
			return false;
		canceled = true;
		return true;
	}

	@Override
	public boolean isCancelled() {
		return canceled;
	}

	@Override
	public boolean isDone() {
		return done || canceled;
	}
	
	public void done() {
		done = true;
	}

	@Override
	public TimestampedEntity get() throws InterruptedException, ExecutionException {
		return null;
	}

	@Override
	public TimestampedEntity get(long timeout, TimeUnit unit)
			throws InterruptedException, ExecutionException, TimeoutException {
		return null;
	}

	@Override
	public void execute(TimestampedEntity timestampedEntity, TransactionID transactionId) throws Exception {
		dPrivilegedTask.execute(timestampedEntity, transactionId);
		done();		
	}

	@Override
	public void onTimeout(TimestampedEntity timestampedEntity, Future<TimestampedEntity> context) {
		dPrivilegedTask.onTimeout(timestampedEntity, context);		
	}

	@Override
	public void onCancel(TimestampedEntity timestampedEntity) {
		dPrivilegedTask.onCancel(timestampedEntity);		
	}

}
