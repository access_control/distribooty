package org.distribooty.lock.decentralized.intern;

import java.util.Set;

import org.distribooty.lock.decentralized.DLockMulticastRequest;
import org.distribooty.lock.decentralized.DPrivilegedTaskWrapper;

public interface LocalDLockSenderListener {
	void onBeforeSendDLockRequest(DLockMulticastRequest request, DPrivilegedTaskWrapper dPrivilegedTaskWrapper);
	void onAfterSendDLockRequest(Set<Integer> pidsResponsesFrom, long timestamp);
}