package org.distribooty.lock.decentralized.intern;

import org.distribooty.lock.decentralized.DLockUnicastRequest;
import org.distribooty.lock.decentralized.DLockUnicastResponse;

public interface LocalDLockReceiver {
	boolean receiveDLockRequest(DLockUnicastRequest request);
	void receiveDLockResponse(DLockUnicastResponse response);
}
