package org.distribooty.lock.decentralized.intern;

import org.distribooty.lock.decentralized.DLockUnicastResponse;

public interface LocalDLockResponseSender {
	void sendDLockResponse(DLockUnicastResponse response);
}
