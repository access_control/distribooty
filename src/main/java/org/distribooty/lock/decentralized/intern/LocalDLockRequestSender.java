package org.distribooty.lock.decentralized.intern;

import org.distribooty.lock.decentralized.DPrivilegedTask;
import org.distribooty.lock.decentralized.DPrivilegedTaskWrapper;

public interface LocalDLockRequestSender extends LocalDLockResponseSender {
	DPrivilegedTaskWrapper sendDLockRequests(String lockId, DPrivilegedTask dPrivilegedTask, LocalDLockSenderListener localDLockReceiver);
}
