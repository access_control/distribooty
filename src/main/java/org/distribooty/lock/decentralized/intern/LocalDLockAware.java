package org.distribooty.lock.decentralized.intern;

public interface LocalDLockAware extends LocalDLockReceiver {
	void waitUntilAllDPrivilegedTaskWorkersDrained();
	void shutdownAllDPrivilegedTaskWorkers();
}
