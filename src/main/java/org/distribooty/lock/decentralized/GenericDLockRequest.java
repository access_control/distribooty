package org.distribooty.lock.decentralized;

import org.distribooty.lock.TimestampedEntity;

public abstract class GenericDLockRequest extends TimestampedEntity {

	private static final long serialVersionUID = 3051853571819996486L;
	
	private String lockId;
	
	public GenericDLockRequest(String lockId, long timeStamp, int srcPid) {
		super(srcPid, timeStamp);
		this.lockId = lockId;		
	}

	public String getLockId() {
		return lockId;
	}	
}
