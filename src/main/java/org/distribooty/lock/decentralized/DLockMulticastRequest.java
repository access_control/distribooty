package org.distribooty.lock.decentralized;

import java.util.HashSet;
import java.util.Set;

public class DLockMulticastRequest extends GenericDLockRequest {

	private static final long serialVersionUID = 2714349688017914719L;
	
	private HashSet<Integer> dstPids = new HashSet<>();

	public DLockMulticastRequest(String lockId, long timestamp, int srcPid, Set<Integer> dstPids) {
		super(lockId, timestamp, srcPid);
		this.dstPids.addAll(dstPids);
	}
	
	void registerResponse(int pid) {
		dstPids.remove(pid);
	}
	
	void registerResponses(Set<Integer> pids) {
		if(pids != null)
			dstPids.removeAll(pids);
	}
	
	boolean allResponsesRetrieved() {
		return dstPids.isEmpty();
	}

}
