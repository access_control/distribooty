package org.distribooty.lock.decentralized;

public class DLockUnicastRequest extends GenericDLockRequest {

	private static final long serialVersionUID = 4959055629070738432L;
	
	private int dstPid;

	public DLockUnicastRequest(String lockId, long timeStamp, int srcPid, int dstPid) {
		super(lockId, timeStamp, srcPid);
		this.dstPid = dstPid;
	}

	public int getDstPid() {
		return dstPid;
	}
}
