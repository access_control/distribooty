package org.distribooty.lock;

import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

import org.distribooty.lock.centralized.CPrivilegedTask;
import org.distribooty.lock.centralized.CRangeLock;
import org.distribooty.lock.centralized.CSimpleLock;

public interface PrivilegedTaskService extends BasicPrivilegedTaskService {
	Future<TimestampedEntity> addPrivilegedTask(CSimpleLock lock, CPrivilegedTask privilegedTask);
	Future<TimestampedEntity> addPrivilegedTask(CSimpleLock[] locks, CPrivilegedTask privilegedTask);
	Future<TimestampedEntity> addPrivilegedTask(CRangeLock lock, CPrivilegedTask privilegedTask);
	Future<TimestampedEntity> addPrivilegedTask(CRangeLock[] locks, CPrivilegedTask privilegedTask);
	Lock getLock(CSimpleLock lock);
	Lock getLock(CSimpleLock[] locks);
	Lock getLock(CRangeLock lock);
	Lock getLock(CRangeLock[] locks);
}