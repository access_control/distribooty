package org.distribooty.lock;

import org.distribooty.lock.centralized.intern.LocalCLockProvider;
import org.distribooty.lock.decentralized.intern.LocalDLockRequestSender;

public interface LocalLockProvider extends LocalCLockProvider, LocalDLockRequestSender {

}
