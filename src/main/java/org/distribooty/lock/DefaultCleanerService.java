package org.distribooty.lock;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;

import org.distribooty.debug.RemoteLogger;
import org.distribooty.util.AbstractWorker;
import org.distribooty.util.Cleanable;
import org.distribooty.util.Cleaner;

public class DefaultCleanerService extends AbstractWorker {

	private Map<Cleanable, Long> cleanableTimestampes = new HashMap<>();
	private Cleaner cleaner;
	private long cleanerServicePeriod;
	
	public DefaultCleanerService(Cleaner cleaner, long cleanerServicePeriod, int pid) {
		super(false, "DefaultCleanerService_Thread-" + pid);
		this.cleaner = cleaner;
		this.cleanerServicePeriod = cleanerServicePeriod;
	}

	@Override
	public void run() {
		
		onRunEnter();
		
		while(true)
		{
			try {
				synchronized(this.lock)
				{
					await(this.cleanerServicePeriod);
					if(this.shutdownFlag)
						break;
					long currentTime = System.currentTimeMillis();
					Iterator<Map.Entry<Cleanable, Long>> iterator = this.cleanableTimestampes.entrySet().iterator();
					while(iterator.hasNext())
					{
						Map.Entry<Cleanable, Long> entry = iterator.next();
						Cleanable key = entry.getKey();
						if(currentTime - entry.getValue() > key.getLiveTime() && cleaner.clean(key))
							iterator.remove();				
					}
				}
			} catch (Exception e) {
				RemoteLogger.getInstance(pid).log(Level.SEVERE, "Exception at " + name, e);
			}		
		}
		onRunLeave();
	}

	public void reportUsage(Cleanable cleanable) {
		synchronized(this.lock)
		{
			this.cleanableTimestampes.put(cleanable, System.currentTimeMillis());
		}
	}

}
