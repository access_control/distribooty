package org.distribooty.lock.intern;

import org.distribooty.lock.TimestampedEntity;
import org.distribooty.lock.centralized.CPrivilegedTask;
import org.distribooty.lock.centralized.CPrivilegedTaskWorker;

public interface SyncCPrivilegedTaskWorkerCreator {
	CPrivilegedTaskWorker getCPrivilegedTaskWorker(TimestampedEntity oldTimestampedEntity, CPrivilegedTask privilegedTask);
}
