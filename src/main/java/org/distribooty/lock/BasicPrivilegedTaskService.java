package org.distribooty.lock;

import java.util.concurrent.Future;

import org.distribooty.lock.decentralized.DPrivilegedTask;

public interface BasicPrivilegedTaskService {
	Future<TimestampedEntity> addPrivilegedTask(String lockId, DPrivilegedTask privilegedTask);
}
