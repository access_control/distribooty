package org.distribooty;

public enum StartupMode {
	FIRST_STARTUP, NORMAL_STARTUP, AFTER_FAILURE_STARTUP;
}
