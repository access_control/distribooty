package org.distribooty;

import java.io.Serializable;

public interface VotingMachineAwareRemotePeerHandler<E extends Serializable & Comparable<E>, H extends Serializable> 
extends LocalPeerHandler<E, H>, VotingMachine<E> {

}
