package org.distribooty;

/**
 * 
 * Represents the state of the remote peer in its lifecycle.
 * All state changes should be persistent saved, so the system after a crash can
 * make the recovery decision.
*/
public enum RemoteState {
	/**
	 * If the remote peer instantiated.
	 */
	NOT_STARTED,
	
	/**
	 * If the remote peer is synchronizing (trying to synchronize), with other ACTIVE or SNAPSHOTED peers..
	 */
	STARTING,
	
	/**
	 * If the remote peer is ready to accept writing and reading operations.
	 */
	ACTIVE,
	
	/**
	 * If the remote peer is shutdowning: no more new requests are accepted. The running requests and transactions have
	 * the chance to finish its operations.
	 */
	SHUTDOWNING,
	
	/**
	 * The remote peer is not accepting new requests and no more running requests and transactons.
	 */
	SHUTDOWN;
}
