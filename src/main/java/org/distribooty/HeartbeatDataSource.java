package org.distribooty;

import java.io.Serializable;

public interface HeartbeatDataSource<H extends Serializable> {
	H getHeartbeatData();
}
