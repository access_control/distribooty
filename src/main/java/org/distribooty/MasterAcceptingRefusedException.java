package org.distribooty;

public class MasterAcceptingRefusedException extends Exception {

	private static final long serialVersionUID = 1346738286181890119L;

	public MasterAcceptingRefusedException() {
	}

	public MasterAcceptingRefusedException(String message) {
		super(message);
	}

	public MasterAcceptingRefusedException(Throwable cause) {
		super(cause);
	}

	public MasterAcceptingRefusedException(String message, Throwable cause) {
		super(message, cause);
	}

	public MasterAcceptingRefusedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
