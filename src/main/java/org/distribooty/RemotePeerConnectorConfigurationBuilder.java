package org.distribooty;

import java.lang.reflect.Field;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.distribooty.lock.centralized.CSimpleLockGraphVertexKeeperMode;

public class RemotePeerConnectorConfigurationBuilder {
	
	private static class DefaultRemotePeerConnectorConfiguration implements RemotePeerConnectorConfiguration {
		private int pid = -1;
		private String shutdownPassword;
		private RemotePeerContact[] contacts;
		private Map<Integer, RemotePeerContact> mappedRemotePeerContacts; 
		private Path persistenceFile;
		private RMISecuritySpecs rmiSecuritySpecs;
		private boolean startElectionAutomatically = true;
		private boolean quorumAware = false;
		private CSimpleLockGraphVertexKeeperMode mode = CSimpleLockGraphVertexKeeperMode.FAST_APPEND;
		private long heartbeatPeriod = 10000L;
		private long neighbourDiscoveryPeriod = 5000L;
		private long cleanerPeriod = 60000L;
		private long dLockWorkerLiveTime = 60000L;
		private long dLockWorkerTimeout = 5000L;
		private long cSimpleLockKeeperLiveTime = 60000L;
		private Level loggingLevel = Level.INFO;
		
		private void checkFields() throws IllegalArgumentException, IllegalAccessException {
			Field[] fields = this.getClass().getDeclaredFields();
			for(Field field : fields)
			{
				field.setAccessible(true);
				if(field.getType().equals(Long.class) && field.getLong(this) <= 0L)
					throw new IllegalStateException("Field " + field.getName() + " must be greater than 0L");
				else if(field.getType().equals(CSimpleLockGraphVertexKeeperMode.class) && 
						field.get(this) == null)
					throw new IllegalStateException("Field " + field.getName() + " must be defined");
				else if(field.getType().equals(Path.class) && field.get(this) == null)
					throw new IllegalStateException("Field " + field.getName() + " must be defined");
				else if(field.getType().equals(Integer.class) && field.getInt(this) < 0)
					throw new IllegalStateException("Field " + field.getName() + " must be greater than or equal 0");
				else if(field.getType().equals(String.class) && (field.get(this) == null || field.get(this).equals("")))
					throw new IllegalStateException("Field " + field.getName() + " must exists and must not be empty");
					
			}
		}
		
		@Override
		public int getPid() {
			return pid;
		}
		
		@Override
		public String getShutdownPassword() {
			return shutdownPassword;
		}
		
		@Override
		public RemotePeerContact[] getContacts() {
			return contacts;
		}
		
		@Override
		public Path getPersistenceFile() {
			return persistenceFile;
		}
		
		@Override
		public RMISecuritySpecs getRmiSecuritySpecs() {
			return rmiSecuritySpecs;
		}
		
		@Override
		public boolean isStartElectionAutomatically() {
			return startElectionAutomatically;
		}
		
		@Override
		public boolean isQuorumAware() {
			return quorumAware;
		}
		
		@Override
		public CSimpleLockGraphVertexKeeperMode getMode() {
			return mode;
		}
		
		@Override
		public long getHeartbeatPeriod() {
			return heartbeatPeriod;
		}
		
		@Override
		public long getNeighbourDiscoveryPeriod() {
			return neighbourDiscoveryPeriod;
		}
		
		@Override
		public long getCleanerPeriod() {
			return cleanerPeriod;
		}
		
		@Override
		public Level getLoggingLevel() {
			return loggingLevel;
		}

		@Override
		public Map<Integer, RemotePeerContact> getMappedRemotePeerContacts() {
			return mappedRemotePeerContacts;
		}

		@Override
		public long getDLockWorkerLiveTime() {
			return dLockWorkerLiveTime;
		}
		
		@Override
		public long getDLockWorkerTimeout() {			
			return dLockWorkerTimeout;
		}

		@Override
		public long getCSimpleLockKeeperLiveTime() {
			return cSimpleLockKeeperLiveTime;
		}

		public void setPid(int pid) {
			this.pid = pid;
		}
		
		public void setShutdownPassword(String shutdownPassword) {
			this.shutdownPassword = shutdownPassword;
		}

		public void setContacts(RemotePeerContact[] contacts) {
			if(contacts == null || contacts.length == 0)
				throw new IllegalStateException("No cluster contacts provided");
			this.contacts = contacts;
			this.mappedRemotePeerContacts = new HashMap<>();
			for(RemotePeerContact contact : contacts)
				this.mappedRemotePeerContacts.put(contact.getPid(), contact);
			
		}

		public void setPersistenceFile(Path persistenceFile) {
			this.persistenceFile = persistenceFile;
		}

		public void setRmiSecuritySpecs(RMISecuritySpecs rmiSecuritySpecs) {
			this.rmiSecuritySpecs = rmiSecuritySpecs;
		}
		
		public void setStartElectionAutomatically(boolean startElectionAutomatically) {
			this.startElectionAutomatically = startElectionAutomatically;
		}

		public void setQuorumAware(boolean quorumAware) {
			this.quorumAware = quorumAware;
		}

		public void setMode(CSimpleLockGraphVertexKeeperMode mode) {
			this.mode = mode;
		}

		public void setHeartbeatPeriod(long heartbeatPeriod) {
			this.heartbeatPeriod = heartbeatPeriod;
		}

		public void setNeighbourDiscoveryPeriod(long neighbourDiscoveryPeriod) {
			this.neighbourDiscoveryPeriod = neighbourDiscoveryPeriod;
		}

		public void setCleanerPeriod(long cleanerPeriod) {
			this.cleanerPeriod = cleanerPeriod;
		}

		public void setDLockWorkerLiveTime(long dLockWorkerLiveTime) {
			this.dLockWorkerLiveTime = dLockWorkerLiveTime;
		}
		
		public void setDLockWorkerTimeout(long dLockWorkerTimeout) {
			this.dLockWorkerTimeout = dLockWorkerTimeout;
		}

		public void setCSimpleLockKeeperLiveTime(long cSimpleLockKeeperLiveTime) {
			this.cSimpleLockKeeperLiveTime = cSimpleLockKeeperLiveTime;
		}

		public void setLoggingLevel(Level loggingLevel) {
			this.loggingLevel = loggingLevel;
		}	
		
	}
	
	private List<RemotePeerContact> contacts = new ArrayList<>();
	
	public RemotePeerConnectorConfiguration build() {
		getConfiguration().setContacts(contacts.toArray(new RemotePeerContact[0]));
					
		try {
			configuration.checkFields();
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new IllegalStateException(e);
		} 			
		
		return configuration; 
	}
	
	private DefaultRemotePeerConnectorConfiguration configuration;
	
	private DefaultRemotePeerConnectorConfiguration getConfiguration() {
		if(configuration == null)
			configuration = new DefaultRemotePeerConnectorConfiguration();
		return configuration;
	}
	
	public RemotePeerConnectorConfigurationBuilder setPid(int pid) {
		getConfiguration().setPid(pid);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder setShutdownPassword(String shutdownPassword) {
		getConfiguration().setShutdownPassword(shutdownPassword);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder addContact(RemotePeerContact contact) {
		contacts.add(contact);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder setPersistenceFile(Path persistenceFile) {
		getConfiguration().setPersistenceFile(persistenceFile);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder setRmiSecuritySpecs(RMISecuritySpecs rmiSecuritySpecs) {
		getConfiguration().setRmiSecuritySpecs(rmiSecuritySpecs);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder setStartElectionAutomatically(boolean startElectionAutomatically) {
		getConfiguration().setStartElectionAutomatically(startElectionAutomatically);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder setQuorumAware(boolean quorumAware) {
		getConfiguration().setQuorumAware(quorumAware);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder setMode(CSimpleLockGraphVertexKeeperMode mode) {
		getConfiguration().setMode(mode);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder setHeartbeatPeriod(long heartbeatPeriod) {
		getConfiguration().setHeartbeatPeriod(heartbeatPeriod);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder setNeighbourDiscoveryPeriod(long neighbourDiscoveryPeriod) {
		getConfiguration().setNeighbourDiscoveryPeriod(neighbourDiscoveryPeriod);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder setCleanerPeriod(long cleanerPeriod) {
		getConfiguration().setCleanerPeriod(cleanerPeriod);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder setDLockWorkerLiveTime(long dLockWorkerLiveTime) {
		getConfiguration().setDLockWorkerLiveTime(dLockWorkerLiveTime);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder setDLockWorkerTimeout(long dLockWorkerTimeout) {
		getConfiguration().setDLockWorkerTimeout(dLockWorkerTimeout);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder setCSimpleLockKeeperLiveTime(long cSimpleLockKeeperLiveTime) {
		getConfiguration().setCSimpleLockKeeperLiveTime(cSimpleLockKeeperLiveTime);
		return this;
	}
	
	public RemotePeerConnectorConfigurationBuilder setLoggingLevel(Level loggingLevel) {
		getConfiguration().setLoggingLevel(loggingLevel);
		return this;
	}	


}
