package org.distribooty.connector;

import java.rmi.Remote;
import java.rmi.registry.Registry;

public interface RemotePeerConnectorExporter {
	Remote export(Remote localObjectImplementation);
	void unexport(Remote localObjectImplementation);
	Registry getRegistry();
}