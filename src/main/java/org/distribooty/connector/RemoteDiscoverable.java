package org.distribooty.connector;

import java.rmi.RemoteException;

import org.distribooty.RemoteIdentifiable;

public interface RemoteDiscoverable extends RemoteIdentifiable {
	RemoteObjectContext getRemoteObjectContext() throws RemoteException;
}
