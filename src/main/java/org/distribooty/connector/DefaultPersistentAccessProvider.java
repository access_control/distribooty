package org.distribooty.connector;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;

public class DefaultPersistentAccessProvider implements PersistentAccessProvider {

	private Path path;
	
	public DefaultPersistentAccessProvider(Path path) {
		this.path = path;
	}
	
	@Override
	public Serializable readObject() throws Exception {
		if(!Files.exists(this.path))
			return null;
			
		Serializable ser = null;
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(this.path.toFile())))
		{
			ser = (Serializable) ois.readObject();
		} 
		return ser;
	}

	@Override
	public void writeObject(Serializable obj) throws Exception {
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(this.path.toFile())))
		{
			oos.writeObject(obj);			
		}
	}

}
