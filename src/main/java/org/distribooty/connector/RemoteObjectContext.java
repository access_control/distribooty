package org.distribooty.connector;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteObjectContext extends Remote {
	Remote getRemoteObject(String objectId) throws RemoteException;
	void close() throws RemoteException;
}
