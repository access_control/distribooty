package org.distribooty.connector.function;

import java.rmi.RemoteException;

@FunctionalInterface
public interface RemoteAwareExecutor {
	void execute() throws RemoteException;
}
