package org.distribooty.connector.function;

import java.rmi.RemoteException;

@FunctionalInterface
public interface RemoteAwarePeerSingleExecutor<T> {
	void execute(int pid, T remoteObject) throws RemoteException;
}
