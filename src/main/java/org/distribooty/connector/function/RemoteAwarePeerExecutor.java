package org.distribooty.connector.function;

import java.rmi.Remote;
import java.rmi.RemoteException;

@FunctionalInterface
public interface RemoteAwarePeerExecutor<T extends Remote> {
	void execute(int pid, T remote) throws RemoteException, AbortRemoteAwarePeerExecutionException;
}
