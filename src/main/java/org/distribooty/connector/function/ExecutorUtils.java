package org.distribooty.connector.function;

import java.util.concurrent.locks.Lock;

import org.distribooty.lock.util.ReentrantCriticalSectionAwareGuard;

public final class ExecutorUtils {
	
	public static void executeLockAware(Lock lock, SimpleExecutor executor) {
		lock.lock();
		try {
			executor.execute();
		} finally {
			lock.unlock();
		}
	}
	
	public static void executeGuardAware(ReentrantCriticalSectionAwareGuard guard, SimpleExecutor executor) {
		if(guard.enter())
			try {
				executor.execute();
			} finally {
				guard.leave();
			}
	}
	
	public static class Container<T> {
		private T value;

		public T getValue() {
			return value;
		}

		public void setValue(T value) {
			this.value = value;
		}		
	}	
	
}
