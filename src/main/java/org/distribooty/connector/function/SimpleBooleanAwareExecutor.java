package org.distribooty.connector.function;

@FunctionalInterface
public interface SimpleBooleanAwareExecutor {
	boolean execute();
}
