package org.distribooty.connector.function;

public class AbortRemoteAwarePeerExecutionException extends Exception {

	private static final long serialVersionUID = 5836063645888567205L;

	public AbortRemoteAwarePeerExecutionException() {
	}

	public AbortRemoteAwarePeerExecutionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public AbortRemoteAwarePeerExecutionException(String message, Throwable cause) {
		super(message, cause);
	}

	public AbortRemoteAwarePeerExecutionException(String message) {
		super(message);
	}

	public AbortRemoteAwarePeerExecutionException(Throwable cause) {
		super(cause);
	}

	
}
