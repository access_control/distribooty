package org.distribooty.connector.function;

import java.rmi.RemoteException;

@FunctionalInterface
public interface RemoteAwareBooleanExecutor {
	boolean execute() throws RemoteException;
}
