package org.distribooty.connector.function;

@FunctionalInterface
public interface SimpleExceptionAwareExecutor {
	void execute() throws Exception;
}
