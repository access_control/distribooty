package org.distribooty.connector.function;

@FunctionalInterface
public interface SimpleExceptionAwareBooleanExecutor {
	boolean execute() throws Exception;
}
