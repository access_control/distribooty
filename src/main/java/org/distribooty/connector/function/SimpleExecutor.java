package org.distribooty.connector.function;

@FunctionalInterface
public interface SimpleExecutor {
	void execute();
}
