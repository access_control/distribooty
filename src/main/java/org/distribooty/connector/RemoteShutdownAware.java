package org.distribooty.connector;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteShutdownAware extends Remote {
	void onShutdown(int srcPid) throws RemoteException;
}
