package org.distribooty.connector;

import org.distribooty.RemotePeerContact;

public interface RemotePeerConnectorProvider extends RemotePeerConnectorExporter {
	<T extends RemotePeerConnector<?, ?>> T retrieve(RemotePeerContact contact, Class<T> cls);
}
