package org.distribooty.connector;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import org.distribooty.RMISecuritySpecs;
import org.distribooty.RemotePeerContact;
import org.distribooty.util.RemoteUtils;
import org.distribooty.util.RmiSslHelper;


public class DefaultRemotePeerConnectorProvider implements RemotePeerConnectorProvider {

	private Registry registry;
	private int port;
	private RMIServerSocketFactory ssf;
	private RMIClientSocketFactory csf;
	
	public DefaultRemotePeerConnectorProvider(RemotePeerContact thisContact, RMISecuritySpecs rmiSecuritySpecs) throws UnrecoverableKeyException, KeyManagementException, KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		
		this.port = thisContact.getPort();
		if(rmiSecuritySpecs != null)
		{
			ssf = RmiSslHelper.getSslRMIServerSocketFactory(rmiSecuritySpecs.getKeystorePath(),
					rmiSecuritySpecs.getKeystorePassword(), 
					rmiSecuritySpecs.getEnabledCipherSuites(),
					rmiSecuritySpecs.getEnabledProtocols(), 
					rmiSecuritySpecs.isNeedClientAuth());
			csf = RmiSslHelper.getSslRMIClientSocketFactory(rmiSecuritySpecs.getKeystorePath(),
					rmiSecuritySpecs.getKeystorePassword(), 
					rmiSecuritySpecs.getEnabledCipherSuites(),
					rmiSecuritySpecs.getEnabledProtocols());
			
			this.registry = LocateRegistry.createRegistry(this.port, this.csf, this.ssf);
		}
		else
			this.registry = LocateRegistry.createRegistry(this.port);		
	}
	
	@Override
	public Remote export(Remote localObjectImplementation) {
		try {
			if(this.csf != null && this.ssf != null)
				return UnicastRemoteObject.exportObject(localObjectImplementation, this.port, this.csf, this.ssf);
			else
				return UnicastRemoteObject.exportObject(localObjectImplementation, this.port);
		} catch (RemoteException e) {
			return null;
		}
	}

	@Override
	public void unexport(Remote localObjectImplementation) {
		RemoteUtils.unexport(localObjectImplementation);
	}

	@Override
	public Registry getRegistry() {
		return this.registry;
	}

	@Override
	public <T extends RemotePeerConnector<?, ?>> T retrieve(RemotePeerContact contact, Class<T> cls) {
		Registry registry;
		try {
			if(this.csf != null)
				registry = LocateRegistry.getRegistry(contact.getHost(), contact.getPort(), this.csf);
			else
				registry = LocateRegistry.getRegistry(contact.getHost(), contact.getPort());
			return cls.cast(registry.lookup(contact.getBindName()));
			} catch (RemoteException | NotBoundException | ClassCastException e) {
				return null;
			}
	}

}
