package org.distribooty.connector;

import java.io.Serializable;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.distribooty.ElectionResult;
import org.distribooty.LocalPeerService;
import org.distribooty.MasterAcceptingRefusedException;
import org.distribooty.MasterChangeListener;
import org.distribooty.MasterListener;
import org.distribooty.RemoteIdentifiable;
import org.distribooty.RemoteInfo;
import org.distribooty.RemotePeerConnectorConfiguration;
import org.distribooty.RemotePeerContact;
import org.distribooty.RemotePeerDescriptor;
import org.distribooty.RemotePeerFailureListener;
import org.distribooty.LocalPeerHandler;
import org.distribooty.RemoteState;
import org.distribooty.SlaveStatusHint;
import org.distribooty.StartupMode;
import org.distribooty.TransactionID;
import org.distribooty.VotingMachine;
import org.distribooty.connector.function.AbortRemoteAwarePeerExecutionException;
import org.distribooty.connector.function.ExecutorUtils.Container;
import org.distribooty.connector.function.RemoteAwareBooleanExecutor;
import org.distribooty.connector.function.RemoteAwareExecutor;
import org.distribooty.connector.function.RemoteAwarePeerExecutor;
import org.distribooty.connector.function.RemoteAwarePeerSingleExecutor;
import org.distribooty.connector.function.SimpleExceptionAwareBooleanExecutor;
import org.distribooty.connector.function.SimpleExceptionAwareExecutor;
import org.distribooty.debug.RemoteLogger;
import org.distribooty.lock.DefaultPrivilegedTaskManager;
import org.distribooty.lock.LocalLockProvider;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.lock.centralized.CLockHandshake;
import org.distribooty.lock.centralized.CLockRequest;
import org.distribooty.lock.centralized.CPrivilegedTask;
import org.distribooty.lock.centralized.CRangeLock;
import org.distribooty.lock.centralized.CRequestResult;
import org.distribooty.lock.centralized.CRequestStatus;
import org.distribooty.lock.centralized.CSimpleLock;
import org.distribooty.lock.centralized.intern.LocalCLockApproveSender;
import org.distribooty.lock.decentralized.DLockMulticastRequest;
import org.distribooty.lock.decentralized.DLockUnicastRequest;
import org.distribooty.lock.decentralized.DLockUnicastResponse;
import org.distribooty.lock.decentralized.DPrivilegedTask;
import org.distribooty.lock.decentralized.DPrivilegedTaskWrapper;
import org.distribooty.lock.decentralized.intern.LocalDLockSenderListener;
import org.distribooty.lock.util.ReentrantCriticalSectionAwareGuard;
import org.distribooty.util.AbstractConsumer;
import org.distribooty.util.AbstractWorker;
import org.distribooty.util.ConcurrentUtils;
import org.distribooty.util.Dumpable;
import org.distribooty.util.LamportClock;

public class DefaultRemotePeerConnector<E extends Serializable & Comparable<E>, H extends Serializable>
implements RemotePeerConnector<E, H>, LocalLockProvider, LocalCLockApproveSender, LocalPeerService, Dumpable {
	
	private class HeartbeatService extends AbstractWorker {

		public HeartbeatService() {
			super(false, "HeartbeatService-Thread-" + DefaultRemotePeerConnector.this.pid);
			setPid(DefaultRemotePeerConnector.this.pid);
		}

		@Override
		public void run() {
			
			onRunEnter();
			
			while(true)
			{
				try {
					synchronized(lock)
					{
						await(DefaultRemotePeerConnector.this.heartbeatPeriod);
						if(this.shutdownFlag)
							break;
						H heartbeatData = remotePeerHandler.getHeartbeatData();
						executeLockAwareRemote(remotePeersLock.readLock(), remotePeers, -1, (p, r) -> {
							remotePeerHandler.applyHeartbeatData(p, r.onHeartbeat(pid, heartbeatData));
						});
					}
				} catch (Exception e) {
					RemoteLogger.getInstance(pid).log(Level.SEVERE, "Exception at " + name, e);
				}
			}
			
			onRunLeave();
		}		
	}
	
	private class DefaultRemotePeerDescriptor extends RemotePeerDescriptor {

		public DefaultRemotePeerDescriptor(int pid, RemoteState remoteState) {
			super(pid, remoteState);
		}

		@Override
		public RemoteObjectContext getRemoteObjectContext() {
			Container<RemoteObjectContext> remoteObjectContext = new Container<>();
			executeLockAwareRemote(remotePeersLock.readLock(), remotePeers, getPid(), (p, r) -> {
				remoteObjectContext.setValue(r.getRemoteObjectContext());
			});
			return remoteObjectContext.getValue();
		}
		
	}
	
	private class NeighbourDiscoveryService extends AbstractWorker {

		public NeighbourDiscoveryService() {
			super(true, "NeighbourDiscoveryService-Thread-" + DefaultRemotePeerConnector.this.pid);
			setPid(DefaultRemotePeerConnector.this.pid);
		}

		@SuppressWarnings("unchecked")
		@Override
		public void run() {
			
			onRunEnter();
			
			while(true)
			{
				try {
					synchronized(lock)
					{
						await(DefaultRemotePeerConnector.this.neighbourDiscoveryPeriod);
						if(this.shutdownFlag)
							break;
						Map<Integer, RemotePeerDescriptor> remoteDescriptors = new HashMap<>();
						executeLockAwareRemote(remotePeersLock.readLock(), remotePeers, -1, (p, r) -> {
							remoteDescriptors.put(p, new DefaultRemotePeerDescriptor(p, r.getRemoteState()));
						});
						Map<Integer, RemotePeerContact> contacts = new TreeMap<>(DefaultRemotePeerConnector.this.mappedRemotePeerContacts);
						contacts.keySet().removeAll(remoteDescriptors.keySet());
						contacts.keySet().remove(pid);
						Map<Integer, RemotePeerConnector<E, H>> newRetrievedPeers = new HashMap<>();
						int masterPid = -1;
						for(Map.Entry<Integer, RemotePeerContact> entry : contacts.entrySet())
						{
							RemotePeerContact contact = entry.getValue();
							int pid = entry.getKey();
							RemotePeerConnector<E, H> remote;
							try {
								remote = remotePeerConnectorProvider.retrieve(contact, RemotePeerConnector.class);
								if(remote != null)
									{
										if(remote.isMaster())
											masterPid = pid;
										newRetrievedPeers.put(pid, remote);
									}
							} catch (Exception e) {
								RemoteLogger.getInstance(DefaultRemotePeerConnector.this.pid).log(Level.SEVERE, "", e);
							}
						}
						
						Set<Integer> unavailablePids = executeRemoteAware(newRetrievedPeers, -1, (p, r) -> {
							r.register(exportRemoteConnector);
						});
						if(unavailablePids != null)
							newRetrievedPeers.keySet().removeAll(unavailablePids);
						Container<Integer> allAvailablePidsCount = new Container<>();
						executeLockAwareSimple(remotePeersLock.writeLock(), () -> {
							remotePeers.putAll(newRetrievedPeers);
							allAvailablePidsCount.setValue(remotePeers.size() + 1);
							for(Map.Entry<Integer, RemotePeerConnector<E, H>> entry : newRetrievedPeers.entrySet())
								remotePeerHandler.onRegister(entry.getKey(), entry.getValue());
						});
						Map<Integer, RemotePeerConnector<E, H>> snapshotedRemotePeers = getRemotePeersSnapshot();
						snapshotedRemotePeers.keySet().removeAll(remoteDescriptors.keySet());
						executeRemoteAware(snapshotedRemotePeers, -1, (p, r) -> {
							remoteDescriptors.put(p, new DefaultRemotePeerDescriptor(p, r.getRemoteState()));
						});
						remotePeerHandler.onNeighbourDiscoveryChange(remoteDescriptors, DefaultRemotePeerConnector.this);
						if(masterPid != -1 || startElectionAutomatically && isEnoughPeersAvailableForElection(allAvailablePidsCount.getValue()))
							startElection();
					}
				} catch (Exception e) {
					RemoteLogger.getInstance(pid).log(Level.SEVERE, "Exception at " + name, e);
				}
			}
			
			onRunLeave();
		}		
	}
	
	private enum QueueEntryType {
		FAILED_PIDS, MASTER_CHANGE, MASTER_LOST, MASTER_IN_CHARGE, MASTER_RETIRE, EXIT;
	}
	
	private class QueueEntry {
		
		private QueueEntryType type;
		private Object value;
		
		public QueueEntry(QueueEntryType type, Object value) {
			this.type = type;
			this.value = value;
		}
		public QueueEntryType getType() {
			return type;
		}
		public Object getValue() {
			return value;
		}		
	}
	
	private class DefaultRemotePeerFailureDetectorService extends AbstractConsumer<QueueEntry> implements RemoteFailureDetector {

		private Set<RemotePeerFailureListener> remotePeerFailureListeners = new HashSet<>();
		private Set<MasterChangeListener> permanentMasterChangeListeners = new HashSet<>();
		private Set<MasterChangeListener> ephemeralMasterChangeListeners = new HashSet<>();
		private Set<MasterListener> permanentMasterListeners = new HashSet<>();
		
		public DefaultRemotePeerFailureDetectorService() {
			super(new QueueEntry(QueueEntryType.EXIT, "RemotePeerFailureDetectorService-Thread-" + DefaultRemotePeerConnector.this.pid));
			setPid(DefaultRemotePeerConnector.this.pid);
		}
		
		private void handleFailedPids(Set<Integer> failedPids) {
			synchronized(remotePeerFailureListeners)
			{
				Set<RemotePeerFailureListener> snapshot = new HashSet<>(remotePeerFailureListeners);
				for(RemotePeerFailureListener l : snapshot)
					l.onRemotePeerFail(failedPids);
				DefaultRemotePeerConnector.executeLockAwareSimple(remotePeersLock.writeLock(), () -> {
					remotePeers.keySet().removeAll(failedPids);
				});
			}			
		}
		
		private void handleMasterLost(int previousMaster) {
			synchronized(this.permanentMasterChangeListeners)
			{
				Set<MasterChangeListener> snapshot = new HashSet<>(this.permanentMasterChangeListeners);
				for(MasterChangeListener l : snapshot)
					l.onMasterLost(previousMaster);
			}
			startElection();
		}
		
		private void handleMasterChage(int masterPid) {
			synchronized(this.permanentMasterChangeListeners)
			{
				Set<MasterChangeListener> snapshot = new HashSet<>(this.permanentMasterChangeListeners);
				for(MasterChangeListener l : snapshot)
					l.onMasterChange(masterPid);
			}
			for(MasterChangeListener l : this.ephemeralMasterChangeListeners)
				l.onMasterChange(masterPid);
			this.ephemeralMasterChangeListeners.clear();
		}
		
		private void handleMasterInCharge(boolean before) {
			synchronized(this.permanentMasterChangeListeners)
			{
				Set<MasterListener> snapshot = new HashSet<>(this.permanentMasterListeners);
				if(before)
					for(MasterListener l : snapshot)
						l.onMasterBeforeInCharge();
				else
					for(MasterListener l : snapshot)
						l.onMasterAfterInCharge();
			}
		}
		
		private void handleMasterRetirement(boolean before) {
			synchronized(this.permanentMasterChangeListeners)
			{
				Set<MasterListener> snapshot = new HashSet<>(this.permanentMasterListeners);
				if(before)
					for(MasterListener l : snapshot)
						l.onMasterBeforeRetirement();
				else
					for(MasterListener l : snapshot)
						l.onMasterAfterRetirement();
			}
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public void run() {
			
			onRunEnter();
			
			out:
			while(true)
			{
				try {
					QueueEntry entry = dequeue();
					switch(entry.getType())
					{
					case EXIT:
						break out;
					case MASTER_LOST:
						MasterChangeListener listener = (MasterChangeListener) entry.getValue();
						if(DefaultRemotePeerConnector.this.masterPid != -1)
						{
							RemoteIdentifiable peer = DefaultRemotePeerConnector.this.getRemotePeerConnector(masterPid);
							if(peer != null)
								try {
									peer.getPid();
									listener.onMasterLost(DefaultRemotePeerConnector.this.lastSeenMasterPid);
									listener.onMasterChange(DefaultRemotePeerConnector.this.masterPid);
									break; // false alarm
								} catch (RemoteException e) {
									DefaultRemotePeerConnector.this.masterPid = -1;
									Set<Integer> failedPids = new HashSet<>();
									failedPids.add(DefaultRemotePeerConnector.this.masterPid);
									handleFailedPids(failedPids);
									handleMasterLost(DefaultRemotePeerConnector.this.lastSeenMasterPid);
								}
						}
						listener.onMasterLost(DefaultRemotePeerConnector.this.lastSeenMasterPid);
						this.ephemeralMasterChangeListeners.add(listener);
						break;
					case MASTER_CHANGE:
						int masterPid = (Integer) entry.getValue();
						if(DefaultRemotePeerConnector.this.masterPid == -1)
							DefaultRemotePeerConnector.this.lastSeenMasterPid = DefaultRemotePeerConnector.this.masterPid = masterPid;
						handleMasterChage(masterPid);
						break;
					case MASTER_IN_CHARGE:
						handleMasterInCharge((Boolean) entry.getValue());
						break;
					case MASTER_RETIRE:
						Object value = entry.getValue();
						if(value instanceof Boolean)
							handleMasterRetirement((Boolean) value);
						else if(value instanceof MasterChangeListener)						
							ephemeralMasterChangeListeners.add((MasterChangeListener) value);						
						break;
					case FAILED_PIDS:
						Set<Integer> failedPids;
						Object obj = entry.getValue();
						if(obj instanceof Integer)
						{
							failedPids = new HashSet<>();
							failedPids.add((Integer) obj);
						}
						else if(obj instanceof Set<?>)
							failedPids = (Set<Integer>) obj;
						else
							failedPids = null;
						if(failedPids != null)
							{
								handleFailedPids(failedPids);
								if(failedPids.contains(DefaultRemotePeerConnector.this.masterPid))
									{
										DefaultRemotePeerConnector.this.masterPid = -1;
										handleMasterLost(DefaultRemotePeerConnector.this.lastSeenMasterPid);
									}
							}
						break;
					default:
						break;
					}
				} catch (Exception e) {
					RemoteLogger.getInstance(pid).log(Level.SEVERE, "Exception at " + name, e);
				}
			}
			onRunLeave();
		}

		@Override
		public void reportMasterFailure(MasterChangeListener listener) {
			enqueue(new QueueEntry(QueueEntryType.MASTER_LOST, listener));			
		}

		@Override
		public void reportRemoteFailure(int pid) {
			enqueue(new QueueEntry(QueueEntryType.FAILED_PIDS, pid));
		}

		@Override
		public void reportRemoteFailure(Set<Integer> pids) {
			enqueue(new QueueEntry(QueueEntryType.FAILED_PIDS, pids));
		}

		@Override
		public void reportMasterChange(int masterPid) {
			enqueue(new QueueEntry(QueueEntryType.MASTER_CHANGE, masterPid));
		}

		@Override
		public void addMasterChangeListener(MasterChangeListener listener) {
			synchronized(this.permanentMasterChangeListeners)
			{
				this.permanentMasterChangeListeners.add(listener);
			}
		}

		@Override
		public void removeMasterChangeListener(MasterChangeListener listener) {
			synchronized(this.permanentMasterChangeListeners)
			{
				this.permanentMasterChangeListeners.remove(listener);
			}
		}

		@Override
		public void addRemotePeerFailureListener(RemotePeerFailureListener listener) {
			synchronized(this.remotePeerFailureListeners)
			{
				this.remotePeerFailureListeners.add(listener);
			}
		}

		@Override
		public void removeRemotePeerFailureListener(RemotePeerFailureListener listener) {
			synchronized(this.remotePeerFailureListeners)
			{
				this.remotePeerFailureListeners.remove(listener);
			}
		}

		@Override
		public <T extends Remote> Set<Integer> executeRemoteAware(Map<Integer, T> remotes,
				RemoteAwarePeerExecutor<T> executor) {
			return DefaultRemotePeerConnector.this.executeRemoteAware(remotes, -1, executor);
		}

		@Override
		public <T extends Remote> boolean executeRemoteAware(Entry<Integer, T> remote,
				RemoteAwarePeerSingleExecutor<T> executor) {
			return DefaultRemotePeerConnector.this.executeRemoteAware(remote, executor);
		}
		
		public void dumpFailureDetector() {
			System.out.println("Dump ephemeral listeners");
			System.out.println("Count of listerners is " + ephemeralMasterChangeListeners.size());
		}

		@Override
		public void reportMasterRetirement(MasterChangeListener listener) {
			queue.browse(it -> {
				if(it != null)
				{	
					RemoteLogger.getInstance(pid).info("reportMasterRetirement called it != null");
					while(it.hasNext())
					{
						QueueEntry entry = it.next();
						if(entry.getType() == QueueEntryType.MASTER_CHANGE)
						{
							it.previous();
							it.add(new QueueEntry(QueueEntryType.MASTER_RETIRE, listener));
							return;
						}
					}
					it.add(new QueueEntry(QueueEntryType.MASTER_RETIRE, listener));
					return;
				}
				RemoteLogger.getInstance(pid).info("reportMasterRetirement called it == null");
			});
		}

		@Override
		public void reportMasterInCharge(boolean blocking, boolean before) {
			if(blocking)
				enqueue(new QueueEntry(QueueEntryType.MASTER_IN_CHARGE, before));
			else
				handleMasterInCharge(before);			
		}

		@Override
		public void reportMasterRetirement(boolean blocking, boolean before) {
			if(blocking)
				enqueue(new QueueEntry(QueueEntryType.MASTER_RETIRE, before));
			else
				handleMasterInCharge(before);			
		}

		@Override
		public void addMasterListener(MasterListener listener) {
			synchronized(permanentMasterListeners)
			{
				permanentMasterListeners.add(listener);
			}			
		}

		@Override
		public void removeMasterListener(MasterListener listener) {
			synchronized(permanentMasterListeners)
			{
				permanentMasterListeners.remove(listener);
			}			
		}
		
	}
	
	private LocalPeerHandler<E, H> remotePeerHandler;
	
	private RemotePeerContact[] remotePeerContacts;
	private TreeMap<Integer, RemotePeerContact> mappedRemotePeerContacts = new TreeMap<>();
	private final int pid;
	
	private final String shutdownPassword;

	private volatile int masterPid = -1, lastSeenMasterPid = -1;
	private boolean startElectionAutomatically;
	private boolean quorumAware;
	
	private long heartbeatPeriod, neighbourDiscoveryPeriod;

	private PersistentAccessProvider persistentAccessProvider;
	private LamportClock timestampedEntityClock = new LamportClock(TimestampedEntity.DELTA),
			termClock = new LamportClock(DELTA),
			sequenceNumberClock = new LamportClock(CLockHandshake.DELTA);
	
	private volatile RemoteState remoteState = RemoteState.NOT_STARTED;
	private ReentrantReadWriteLock remotePeersLock = new ReentrantReadWriteLock();
	private TreeMap<Integer, RemotePeerConnector<E, H>> remotePeers = new TreeMap<>();
	private RemotePeerConnector<E, H> exportRemoteConnector;
	private RemotePeerConnectorProvider remotePeerConnectorProvider;
	
	private ReentrantCriticalSectionAwareGuard cLockRemoteGuard = new ReentrantCriticalSectionAwareGuard(true);
	private ReentrantCriticalSectionAwareGuard cLockLocalGuard = new ReentrantCriticalSectionAwareGuard(true);
	private ReentrantCriticalSectionAwareGuard dLockRemoteGuard = new ReentrantCriticalSectionAwareGuard(false);
	private ReentrantCriticalSectionAwareGuard dLockLocalGuard = new ReentrantCriticalSectionAwareGuard(false);
	private ReentrantCriticalSectionAwareGuard connectableGuard = new ReentrantCriticalSectionAwareGuard(false);
	
	private RemotePeerConnectorConfiguration configuration;
	
	private CountDownLatch shutdownLatch = new CountDownLatch(1);
	
	private DefaultRemotePeerFailureDetectorService remoteFailureDetector;
	private HeartbeatService heartbeatService;
	private NeighbourDiscoveryService neighbourDiscoveryService;
	
	private DefaultPrivilegedTaskManager privilegedTaskManager;
	
	protected class MasterCandidate {
		private boolean masterCandidate = true;
		
		public MasterCandidate() {}
		
		public boolean isMasterCandidate() {
			return masterCandidate;
		}
		
		public void noMoreMasterCandidate() {
			masterCandidate = false;
		}
	}
	
	private MasterCandidate masterCandidate = new MasterCandidate();
	
	protected class MasterRetirementAfterReelectionForced {
		
		private boolean retiring = false;
		
		public MasterRetirementAfterReelectionForced() {}

		public boolean isRetiring() {
			return retiring;
		}

		public void setRetiring(boolean retiring) {
			this.retiring = retiring;
		}		
	}
	
	private MasterRetirementAfterReelectionForced masterRetirement = new MasterRetirementAfterReelectionForced();
	
	private class DefaultVotingMachine implements VotingMachine<E> {

		private HashSet<Integer> participantPids;
		private HashSet<Integer> sameStateAsMasterPids;
		private E maxEvaluableObject;
		
		@Override
		public void onVotingBegin() {
			RemoteLogger.getInstance(pid).log(Level.INFO, "Starting the default voting machine with state " + remoteState.name());
			participantPids = new HashSet<>();
			sameStateAsMasterPids = new HashSet<>();
			maxEvaluableObject = null;
		}
		
		@Override
		public void onEvaluableObject(int srcPid, E evaluableObject) throws Exception {
			if(evaluableObject == null)
				throw new NullPointerException("Evaluable Object retrieved from pid \"" + srcPid + "\" is null");
			participantPids.add(srcPid);
			if(maxEvaluableObject == null)
			{
				maxEvaluableObject = evaluableObject;
				sameStateAsMasterPids.add(srcPid);
			}
			else 
			{
				int cmp = evaluableObject.compareTo(maxEvaluableObject);
				if(cmp >= 0)
					{
						if(cmp > 0)
							sameStateAsMasterPids.clear();
						sameStateAsMasterPids.add(srcPid);
						maxEvaluableObject = evaluableObject;
					}				
			}
		}

		@Override
		public ElectionResult onEvaluationEnd() throws Exception {
			int masterPid = sameStateAsMasterPids.stream().min((pid1, pid2) -> pid1 - pid2).get();
			return new ElectionResult(masterPid, sameStateAsMasterPids, participantPids);
		}

		@Override
		public void onEvaluationAbort(int srcPid, Throwable reason) {
			remotePeerHandler.onElectionFailure(srcPid, reason);			
		}		
	}
	
	private boolean isEnoughPeersAvailableForElection(int peersCount) {
		return quorumAware && peersCount > remotePeerContacts.length / 2 || 
				!quorumAware && peersCount == remotePeerContacts.length;
	}
	
	private void switchToActive() {
		if(remoteState.compareTo(RemoteState.ACTIVE) < 0)
			{
				remoteState = RemoteState.ACTIVE;
				neighbourDiscoveryService.shutdown(true);
			}
		cLockLocalGuard.unlock();
		remoteFailureDetector.reportMasterChange(masterPid);		
	}
	
	private void onElectionNotNecessary(boolean thisPeerMaster) {
		switchToActive();
		remotePeerHandler.onElectionNonNeccesary(masterPid, termClock.getClock(), thisPeerMaster, DefaultRemotePeerConnector.this);
	}
	
	private class VotingMachineExecutor implements DPrivilegedTask {
		
		private VotingMachine<E> votingMachine;
		
		public VotingMachineExecutor(VotingMachine<E> votingMachine) {
			this.votingMachine = votingMachine;
		}

		@Override
		public void execute(TimestampedEntity timestampedEntity, TransactionID transactionId) throws Exception {
			RemoteLogger.getInstance(pid).info("Election started started");
			if(masterPid != -1)
			{
				onElectionNotNecessary(masterPid == pid);
				return;
			}
			Map<Integer, RemotePeerConnector<E, H>> startingPeers = new HashMap<>(),
					activePeers = new HashMap<>(), shutdowningPeers = new HashMap<>();
			executeLockAwareRemote(remotePeersLock.readLock(), remotePeers, -1, (p, r) -> {
				RemoteInfo remoteInfo = r.getRemoteInfo();
				if(remoteInfo.isMaster())
				{
					masterPid = p;
					termClock.setClock(remoteInfo.getLastSeenTerm());
					throw new AbortRemoteAwarePeerExecutionException();
				}
				else
				{
					RemoteState state = remoteInfo.getRemoteState();
					if(state == RemoteState.STARTING)
						startingPeers.put(p, r);
					else if(state == RemoteState.ACTIVE)
						activePeers.put(p, r);
					else if(state == RemoteState.SHUTDOWNING)
						shutdowningPeers.put(p, r);
				}
			});
			if(masterPid != -1)
			{
				onElectionNotNecessary(false);
				return;
			}
			if(remoteState == RemoteState.STARTING)
				startingPeers.put(pid, DefaultRemotePeerConnector.this);
			else if(remoteState == RemoteState.ACTIVE)
				activePeers.put(pid, DefaultRemotePeerConnector.this);
			else if(remoteState == RemoteState.SHUTDOWNING)
				shutdowningPeers.put(pid, DefaultRemotePeerConnector.this);
			Map<Integer, RemotePeerConnector<E, H>> participants;
			int startingCount = startingPeers.size();
			if(activePeers.size() > 0)
				participants = activePeers;
			else if(shutdowningPeers.size() > 0)
				participants = shutdowningPeers;
			else if(isEnoughPeersAvailableForElection(startingCount))
				participants = startingPeers;
			else
				{
					remotePeerHandler.onElectionFailure(pid, new Exception("There is no poroper participants"));
					return;
				}
			Container<Throwable> failed = new Container<>();
			while(!participants.isEmpty())
			{
				failed.setValue(null);
				votingMachine.onVotingBegin();
				executeLockAwareRemote(remotePeersLock.readLock(), participants, -1, (p, r) -> {
					try {
						if(failed.getValue() != null)
							throw new AbortRemoteAwarePeerExecutionException();
						votingMachine.onEvaluableObject(p, r.startElection(pid));
					} catch (Exception e) {
						votingMachine.onEvaluationAbort(p, e);
						failed.setValue(e);
						if(e instanceof RemoteException)
							throw (RemoteException) e;
						else
							throw new AbortRemoteAwarePeerExecutionException();
					}
				});
				if(failed.getValue() != null)
					return;
				ElectionResult electionResult = votingMachine.onEvaluationEnd();
				if(electionResult == null)
				{
					remotePeerHandler.onElectionFailure(pid, new NullPointerException("Evaluation end failed"));
					return;
				}
				int mPid = electionResult.getMasterPid();
				try {
					getRemotePeerConnector(mPid).delegateMastership(pid, electionResult);
					break;
				} catch (MasterAcceptingRefusedException e) {
					executeLockAwareRemote(remotePeersLock.readLock(), remotePeers, -1, (p, r) -> {
						r.onElectionFailed(mPid, e);
					});
					participants.remove(mPid);
				}
			}
			
		}

		@Override
		public void onTimeout(TimestampedEntity timestampedEntity, Future<TimestampedEntity> context) {
			
		}

		@Override
		public void onCancel(TimestampedEntity timestampedEntity) {
			
		}
		
	}
	
	private NavigableMap<Integer, RemotePeerConnector<E, H>> getRemotePeersSnapshot() {
		TreeMap<Integer, RemotePeerConnector<E, H>> snapshotedRemotePeers = new TreeMap<>();
		executeLockAwareSimple(remotePeersLock.readLock(), () -> 
			snapshotedRemotePeers.putAll(remotePeers)
		);		
		return snapshotedRemotePeers;
	}
	
	private Map.Entry<Integer, RemotePeerConnector<E, H>> getRemotePeerSnapshot(int pid) {
		if(pid == -1)
			return null;
		else if(this.pid == pid)
			return Map.entry(pid, this);
		Container<RemotePeerConnector<E,H>> container = new Container<>();
		executeLockAwareRemote(pid, remotePeersLock.readLock(), () -> {
			container.setValue(remotePeers.get(pid));
		});		
		RemotePeerConnector<E, H> remotePeerConnector = container.getValue();
		Map.Entry<Integer, RemotePeerConnector<E, H>> snapshotedRemotePeer = null;
		if(remotePeerConnector != null)
			snapshotedRemotePeer = Map.entry(pid, remotePeerConnector);
		return snapshotedRemotePeer;
	}
	
	private RemotePeerConnector<E, H> getRemotePeerConnector(int pid) {
		Map.Entry<Integer, RemotePeerConnector<E, H>> remotePeerConnectorEntry = getRemotePeerSnapshot(pid);
		try {
			return remotePeerConnectorEntry.getValue();
		} catch(NullPointerException e) {
			return null;
		}
			
	}
	
	private RemotePeerConnector<E, H> getRemotePeerMasterConnector() {
		return getRemotePeerConnector(masterPid);
	}
	
	private <T extends Remote> Set<Integer> executeRemoteAware(Map<Integer, T> remoteSource, int pid, RemoteAwarePeerExecutor<T> executor) {
		Set<Integer> unavailablePids = null;
		if(pid == -1)
			for(Map.Entry<Integer, T> entry : remoteSource.entrySet())
			{
				int p = entry.getKey();
				try {
					executor.execute(p, entry.getValue());
				} catch (RemoteException e) {
					if(unavailablePids == null)
						unavailablePids = new HashSet<>();
					unavailablePids.add(p);
				} catch (AbortRemoteAwarePeerExecutionException e) {
					break;
				}			
			}
		else
		{
			try {
				executor.execute(pid, remoteSource.get(pid));
			} catch (RemoteException e) {
				if(unavailablePids == null)
					unavailablePids = new HashSet<>();
				unavailablePids.add(pid);
			} catch (AbortRemoteAwarePeerExecutionException e) {
				
			}
		}
		if(unavailablePids != null)
			remoteFailureDetector.reportRemoteFailure(unavailablePids);
		return unavailablePids;
	}
	
	private <T extends Remote> boolean executeRemoteAware(Map.Entry<Integer, T> remoteSource, RemoteAwarePeerSingleExecutor<T> executor) {
		int pid = remoteSource.getKey();
		try {
			executor.execute(pid, remoteSource.getValue());
			return true;
		} catch (RemoteException e) {
			remoteFailureDetector.reportRemoteFailure(pid);
			return false;
		}
	}
	
	private boolean executeLockAwareRemote(int pid, Lock lock, RemoteAwareExecutor executor) {
		lock.lock();
		try {
			executor.execute();
			return true;
		} catch (RemoteException e) {
			remoteFailureDetector.reportRemoteFailure(pid);
			return false;
		} finally {
			lock.unlock();
		}
	}
	
	private <T extends Remote> Set<Integer> executeLockAwareRemote(Lock lock, Map<Integer, T> source, int pid, RemoteAwarePeerExecutor<T> executor) {
		Container<Set<Integer>> container = new Container<>();
		Exception e = executeLockAwareSimple(lock, () -> {
			container.setValue(executeRemoteAware(source, pid, executor));
		});
		if(e != null)
			e.printStackTrace();
		return container.getValue();
	}
	
	private boolean executeRemoteAware(int pid, MasterChangeListener listener, RemoteAwareExecutor executor) {
		try {
			executor.execute();
			return true;
		} catch(RemoteException | NullPointerException e) {
			if(listener == null)
				remoteFailureDetector.reportRemoteFailure(pid);
			else
				remoteFailureDetector.reportMasterFailure(listener);
		}
		return false;
	}
	
	private static Exception executeLockAwareSimple(Lock lock, SimpleExceptionAwareExecutor executor) {
		lock.lock();
		try {
			executor.execute();
			return null;
		} catch(Exception e) {
			return e;
		} finally {
			lock.unlock();
		}
	}
	
	private static boolean executeGuardAware(ReentrantCriticalSectionAwareGuard guard, boolean overrideGuard, SimpleExceptionAwareBooleanExecutor executor) {
		if(overrideGuard || guard.enter())
		{
			try {
				return executor.execute();
			} catch(Exception e) {
				return false;
			}
			finally {
				if(!overrideGuard)
					guard.leave();
			}
		}
		else
			return false;
	}
	
	private boolean executeRemoteAware(int pid, RemoteAwareBooleanExecutor executor) {
		try {
			return executor.execute();
		} catch(RemoteException | NullPointerException e) {
			remoteFailureDetector.reportRemoteFailure(pid);
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public DefaultRemotePeerConnector(RemotePeerConnectorConfiguration configuration,
			RemotePeerConnectorProvider remotePeerConnectorProvider,
			LocalPeerHandler<E, H> remotePeerHandler,
			PersistentAccessProvider persistenceAccessProvider) throws AccessException, RemoteException {
		this.pid = configuration.getPid();
		this.shutdownPassword = configuration.getShutdownPassword();
		this.remotePeerContacts = configuration.getContacts();
		this.mappedRemotePeerContacts.putAll(configuration.getMappedRemotePeerContacts());
		RemotePeerContact thisContact = this.mappedRemotePeerContacts.get(this.pid);
		Objects.requireNonNull(thisContact);
		this.remotePeerHandler = remotePeerHandler;
		this.persistentAccessProvider = persistenceAccessProvider;
		this.remotePeerConnectorProvider = remotePeerConnectorProvider;
		this.exportRemoteConnector = (RemotePeerConnector<E, H>) remotePeerConnectorProvider.export(this);
		this.remotePeerConnectorProvider.getRegistry().rebind(thisContact.getBindName(), this.exportRemoteConnector);
		this.startElectionAutomatically = configuration.isStartElectionAutomatically();
		this.quorumAware = configuration.isQuorumAware();
		this.heartbeatPeriod = configuration.getHeartbeatPeriod();
		this.neighbourDiscoveryPeriod = configuration.getNeighbourDiscoveryPeriod();
		this.configuration = configuration;
		this.privilegedTaskManager = new DefaultPrivilegedTaskManager(this.pid,
				this.configuration.getMode(), 
				this.configuration.getCleanerPeriod(), 
				this.configuration.getDLockWorkerLiveTime(),
				this.configuration.getDLockWorkerTimeout(),
				this.configuration.getCSimpleLockKeeperLiveTime(), 
				this.timestampedEntityClock, 
				this.sequenceNumberClock, 
				this.termClock, 
				this.cLockLocalGuard, 
				this.dLockLocalGuard, 
				this, 
				this, this);
		this.heartbeatService = new HeartbeatService();
		this.remoteFailureDetector = new DefaultRemotePeerFailureDetectorService();
		this.remoteFailureDetector.addMasterChangeListener(this.remotePeerHandler);
		this.remoteFailureDetector.addMasterListener(this.remotePeerHandler);
		this.remoteFailureDetector.addRemotePeerFailureListener(this.remotePeerHandler);
		this.remoteFailureDetector.addRemotePeerFailureListener(this.privilegedTaskManager);
		this.neighbourDiscoveryService = new NeighbourDiscoveryService();
	}
	
	private StartupMode checkStartupCondition() {
		RemotePeerConnectorPersistentState state;
		
		try {
			state = (RemotePeerConnectorPersistentState)
					persistentAccessProvider.readObject();
			if(state == null)
				return StartupMode.FIRST_STARTUP;
			else if(state.getRemoteState() == RemoteState.SHUTDOWN)
			{
				termClock = state.getTermClock();
				return StartupMode.NORMAL_STARTUP;
			}
			else
				return StartupMode.AFTER_FAILURE_STARTUP;			
		} catch (Exception e) {
			e.printStackTrace();
			return StartupMode.AFTER_FAILURE_STARTUP;
		}
	}
	
	public void start() {
		this.remotePeerHandler.onInit(checkStartupCondition(), this);
		remoteState = RemoteState.STARTING;
		try {
			this.persistentAccessProvider.writeObject(new RemotePeerConnectorPersistentState(remoteState, termClock));
		} catch (Exception e) {
			e.printStackTrace();
		}
		ScheduledExecutorService executorService = this.privilegedTaskManager.getExecutorService();
		executorService.execute(this.heartbeatService);		
		executorService.execute(this.remoteFailureDetector);
		executorService.execute(this.neighbourDiscoveryService);
	}
	
	private static void lockGuard(ReentrantCriticalSectionAwareGuard guard) {
		guard.lock();
		guard.waitUntilAllLeaveAfterLocked(false);
	}

	@Override
	public void shutdown(boolean graceful) {
		
		synchronized(this)
		{
			RemoteState state = this.remoteState;
			if(state == RemoteState.SHUTDOWNING || state == RemoteState.SHUTDOWN)
				return;
			this.remoteState = RemoteState.SHUTDOWNING;
		}
		Thread shutdownThread = new Thread(
				() -> {
					
					Logger logger = RemoteLogger.getInstance(pid);
					
					logger.log(Level.INFO, "Starting shutdowning of " + DefaultRemotePeerConnector.this.getClass().getCanonicalName());
					
					logger.log(Level.INFO, "Shutdowning of Neighbour Discovery Service");
						
					this.neighbourDiscoveryService.shutdown(true);
					this.neighbourDiscoveryService.waitForShutdownComplete();					
					
					logger.log(Level.INFO, "Shutdowning of Cleaner Discovery Service");
					
					this.privilegedTaskManager.shutdownCleanerService();
					
					logger.log(Level.INFO, "Locking of Central Local Lock Guard");
					
					lockGuard(this.cLockLocalGuard);
					
					logger.log(Level.INFO, "Waiting until all CPrivilegedTaskWorkers drained");
					
					this.privilegedTaskManager.waitUntilAllCPrivilegedTasksDrained();
					
					logger.log(Level.INFO, "Locking of Central Remote Lock Guard");

					lockGuard(this.cLockRemoteGuard);	
					
					synchronized(this.masterCandidate)
					{
						this.masterCandidate.noMoreMasterCandidate();
						
						logger.log(Level.INFO, "Waiting of Central Lock drained");
						
						this.privilegedTaskManager.waitUntilCLocksDrained();
						
						logger.log(Level.INFO, "Shutdowning of Central Lock Request/Response Handlers");
						
						this.privilegedTaskManager.shutdownCLockRequestHandler();						
					}
					
					logger.log(Level.INFO, "Locking of Decentral Local Lock Guard");

					lockGuard(this.dLockLocalGuard);
					
					logger.log(Level.INFO, "Shutdowning of Decentral Lock Workers");
					
					this.privilegedTaskManager.shutdownAllDPrivilegedTaskWorkers();
					
					logger.log(Level.INFO, "Wait until all DPrivilegedTakWorkers drained");

					this.privilegedTaskManager.waitUntilAllDPrivilegedTaskWorkersDrained();
					
					logger.log(Level.INFO, "Locking of Decentral Remote Lock Guard");
					
					lockGuard(this.dLockRemoteGuard);
					
					logger.log(Level.INFO, "Shutdowning of Remote Failure Detector");
					
					this.remoteFailureDetector.shutdown(true);
					this.remoteFailureDetector.waitForShutdownComplete();
					
					logger.log(Level.INFO, "Shutdowning of Heartbeat Service");
					
					this.heartbeatService.shutdown(true);
					this.heartbeatService.waitForShutdownComplete();
										
					if(pid == masterPid)
					{
						this.remotePeerHandler.onMasterBeforeRetirement();
						logger.log(Level.INFO, "Delegating mastership");

						DefaultRemotePeerConnector.this.executeLockAwareRemote(this.remotePeersLock.readLock(),
								this.remotePeers, -1, (p, r) -> {
									if(r.getRemoteState() == RemoteState.ACTIVE)
										try {
											r.delegateMastership(pid, null);
											throw new AbortRemoteAwarePeerExecutionException();
										} catch (MasterAcceptingRefusedException e) {
											
										}
								});
						masterPid = -1;
						this.remotePeerHandler.onMasterAfterRetirement();
					}
					
					logger.log(Level.INFO, "Unregistering at remote peers");
					
					Map<Integer, RemotePeerConnector<E, H>> snapshotedPeers = getRemotePeersSnapshot();
					for(Map.Entry<Integer, RemotePeerConnector<E, H>> entry : snapshotedPeers.entrySet())					
						try {
							RemotePeerConnector<E, H> r = entry.getValue();
							r.onShutdown(pid);
							r.unregister(pid);
						} catch (RemoteException e) {
							
						}
			
					logger.log(Level.INFO, "Clearing the remote stubs");
					
					DefaultRemotePeerConnector.executeLockAwareSimple(this.remotePeersLock.writeLock(),
							() -> {
						for(int p : this.remotePeers.keySet())							
							this.remotePeerHandler.onUnregister(p);
							
						this.remotePeers.clear();
					});
					
					logger.log(Level.INFO, "Shutdowning of Executor Service");
					
					this.privilegedTaskManager.shutdownExecutorService();
					ConcurrentUtils.waitUninterruptible(this.privilegedTaskManager.getExecutorService(), 1L, TimeUnit.MILLISECONDS);
					
					logger.log(Level.INFO, "Unbinding of this remote peer");

					try {
						this.remotePeerConnectorProvider.getRegistry().unbind(mappedRemotePeerContacts.get(pid).getBindName());
					} catch (RemoteException | NotBoundException e) {
						logger.log(Level.SEVERE, "", e);
					}
					
					logger.log(Level.INFO, "Unexporting of this remote peer");

					this.remotePeerConnectorProvider.unexport(DefaultRemotePeerConnector.this);
					this.remotePeerHandler.onShutdown();
					this.remoteState = RemoteState.SHUTDOWN;
					
					logger.log(Level.INFO, "Saving of persistence state");
					
					try {
						this.persistentAccessProvider.writeObject(new RemotePeerConnectorPersistentState(remoteState, termClock));
						logger.log(Level.INFO, "Persistence state saved");
					} catch (Exception e) {
						RemoteLogger.getInstance(pid).log(Level.SEVERE, "", e);
					}
					shutdownLatch.countDown();
				}
			);
		shutdownThread.setName("RemotePeerConnectorShutdownThread");
		shutdownThread.start();
	}

	@Override
	public void waitForShutdownComplete() {
		ConcurrentUtils.waitUninterruptible(shutdownLatch);
	}

	@Override
	public int getLocalPid() {
		return pid;
	}

	@Override
	public RemoteState getLocalRemoteState() {
		return remoteState;
	}

	@Override
	public int getMasterPid() {
		return masterPid;
	}

	@Override
	public Future<TimestampedEntity> addPrivilegedTask(String lockId, DPrivilegedTask privilegedTask) {
		return privilegedTaskManager.addPrivilegedTask(lockId, privilegedTask);
	}

	@Override
	public Future<TimestampedEntity> addPrivilegedTask(CSimpleLock lock, CPrivilegedTask privilegedTask) {
		return privilegedTaskManager.addPrivilegedTask(lock, privilegedTask);
	}

	@Override
	public Future<TimestampedEntity> addPrivilegedTask(CSimpleLock[] locks, CPrivilegedTask privilegedTask) {
		return privilegedTaskManager.addPrivilegedTask(locks, privilegedTask);
	}

	@Override
	public Future<TimestampedEntity> addPrivilegedTask(CRangeLock lock, CPrivilegedTask privilegedTask) {
		return privilegedTaskManager.addPrivilegedTask(lock, privilegedTask);
	}

	@Override
	public Future<TimestampedEntity> addPrivilegedTask(CRangeLock[] locks, CPrivilegedTask privilegedTask) {
		return privilegedTaskManager.addPrivilegedTask(locks, privilegedTask);
	}
	
	@Override
	public Lock getLock(CSimpleLock lock) {
		return privilegedTaskManager.getLock(lock);
	}

	@Override
	public Lock getLock(CSimpleLock[] locks) {
		return privilegedTaskManager.getLock(locks);
	}

	@Override
	public Lock getLock(CRangeLock lock) {
		return privilegedTaskManager.getLock(lock);
	}

	@Override
	public Lock getLock(CRangeLock[] locks) {
		return privilegedTaskManager.getLock(locks);
	}

	@Override
	public RemoteObjectContext retrieveRemoteObjectContext(int pid) {
		Map.Entry<Integer, RemotePeerConnector<E, H>> remotePeerConnectorEntry = getRemotePeerSnapshot(pid);
		Container<RemoteObjectContext> container = new Container<>();
		if(remotePeerConnectorEntry != null)		
			executeRemoteAware(remotePeerConnectorEntry, (p, r) -> {
				container.setValue(r.getRemoteObjectContext());

			});		
		return container.getValue();
	}

	@Override
	public Map<Integer, RemoteObjectContext> retrieveRemoteObjectContexts(Set<Integer> pids) {
		Map<Integer, RemotePeerConnector<E, H>> remotePeersSnapshot = getRemotePeersSnapshot();
		remotePeersSnapshot.keySet().retainAll(pids);
		Map<Integer, RemoteObjectContext> remoteObjectsResult = new HashMap<>();
		executeRemoteAware(remotePeersSnapshot, -1, (p, r) -> {
			remoteObjectsResult.put(p, r.getRemoteObjectContext());
		});
		return remoteObjectsResult;
	}

	@Override
	public Map<Integer, RemotePeerContact> getRemoteContacts() {
		return mappedRemotePeerContacts;
	}

	@Override
	public RemoteFailureAwareContext getRemoteFailureAwareContext() {
		return remoteFailureDetector;
	}

	@Override
	public RemoteObjectContext getRemoteObjectContext() throws RemoteException {
		return remotePeerHandler.getRemoteObjectContext(); // TODO change
	}

	@Override
	public int getPid() throws RemoteException {
		return pid;
	}

	@Override
	public RemoteState getRemoteState() throws RemoteException {
		return remoteState;
	}
	
	@Override
	public RemoteInfo getRemoteInfo() throws RemoteException {
		return new RemoteInfo(this.getPid(), 
				this.getRemoteState(), 
				this.isMaster(), 
				this.getCurrentTimestamp(),
				this.getLastSeenTerm());
	}

	@Override
	public boolean register(RemotePeerConnector<E, H> remotePeerConnector) throws RemoteException {
		return executeGuardAware(connectableGuard, false, () -> 
			executeLockAwareRemote(-1, remotePeersLock.writeLock(), () -> 
					{
						int pid = remotePeerConnector.getPid();
						remotePeers.put(pid, remotePeerConnector);
						remotePeerHandler.onRegister(pid, remotePeerConnector);
					}
				)
			);
	}

	@Override
	public void unregister(int srcPid) throws RemoteException {
		executeGuardAware(connectableGuard, false, () ->  
			executeLockAwareRemote(-1, remotePeersLock.writeLock(), () -> 
				{
					if(remotePeers.remove(srcPid) != null)
						remotePeerHandler.onUnregister(srcPid);
				}
			)
		);
	}

	@Override
	public E startElection(int srcPid) throws RemoteException {
		return remotePeerHandler.getEvaluableObject();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void startElection() {
		VotingMachine<E> votingMachine;
		if(remotePeerHandler instanceof VotingMachine<?>)
			votingMachine = (VotingMachine<E>) remotePeerHandler;
		else
			votingMachine = new DefaultVotingMachine();
		addPrivilegedTask(ELECTION_LOCK_ID, new VotingMachineExecutor(votingMachine));
	}

	@Override
	public void startReelection() {
		executeRemoteAware(masterPid, null, () -> getRemotePeerMasterConnector().forceReelection(pid));		
	}

	@Override
	public void delegateMastership(int srcPid, ElectionResult electionResult)
			throws RemoteException, MasterAcceptingRefusedException {
		synchronized(masterCandidate)
		{
			if(!masterCandidate.isMasterCandidate())
				throw new MasterAcceptingRefusedException("Remote Peer with id \"" + pid + "\" refused to accept mastership");
			Logger logger = RemoteLogger.getInstance(pid);
			logger.info("delegateMaster beginning");
			lastSeenMasterPid = masterPid = pid;
			List<CLockHandshake> remoteCLockHandshakes = null;
			if(electionResult != null)
			{
				remoteCLockHandshakes = privilegedTaskManager.getCLockHandshakes(true);
				List<CLockHandshake> _remoteCLockHandshakes = remoteCLockHandshakes;
				logger.info("handshakes at self called");
				executeLockAwareRemote(remotePeersLock.readLock(), remotePeers, -1, (p, r) -> {
					try {
						_remoteCLockHandshakes.addAll(r.getCLockHandshakes());
						logger.info("handshakes from " + p + " called");
					} catch (NullPointerException e) {
						
					}
				});
				privilegedTaskManager.initCLockRequestHandler(remoteCLockHandshakes);				
			}
			logger.info("calling handshakes finsihed");
			remoteFailureDetector.reportMasterInCharge(true, true);
			cLockRemoteGuard.unlock();
			cLockLocalGuard.unlock();
			long term = termClock.incrementAndGet();
			onMasterElected(masterPid, term, SlaveStatusHint.MASTER_SAME_STATE);
			executeLockAwareRemote(remotePeersLock.readLock(), remotePeers, -1, (p, r) -> {
				r.onMasterElected(masterPid, term, electionResult == null 
				? SlaveStatusHint.MASTER_INSTALLED : 
				electionResult.getSameStateAsMasterPids().contains(p) ? SlaveStatusHint.MASTER_SAME_STATE :
				electionResult.getParticipantPids().contains(p) ? SlaveStatusHint.MASTER_DIFFERENT_STATE : 
					SlaveStatusHint.MASTER_INSTALLED);
			});
			if(electionResult != null)
				privilegedTaskManager.activateApprovedHandshakes(remoteCLockHandshakes);
			remoteFailureDetector.reportMasterInCharge(true, false);
			logger.info("end delegateMaster calling");
		}
	}

	@Override
	public void onMasterElected(int masterPid, long term, SlaveStatusHint slaveStatusHint) throws RemoteException {
		if(masterCandidate.isMasterCandidate())
		{
			if(pid != masterPid)
				termClock.setClock(term);
			this.lastSeenMasterPid = this.masterPid = masterPid;
			switchToActive();
			try {
				persistentAccessProvider.writeObject(new RemotePeerConnectorPersistentState(remoteState, termClock));
			} catch (Exception e) {
				e.printStackTrace();
			}
			remotePeerHandler.onElectionEnd(masterPid, termClock.getClock(), slaveStatusHint, this);				
		}
	}

	@Override
	public void onElectionFailed(int srcPid, Throwable reason) throws RemoteException {
		remotePeerHandler.onElectionFailure(srcPid, reason);		
	}

	@Override
	public boolean isMaster() throws RemoteException {
		return pid == masterPid;
	}

	@Override
	public boolean forceReelection(int srcPid) throws RemoteException {
		synchronized(masterRetirement)
		{
			if(pid != masterPid || masterRetirement.isRetiring())
				return false;
			masterRetirement.setRetiring(true);
		}
		remotePeerHandler.onMasterBeforeRetirement();
		cLockLocalGuard.lock();
		cLockLocalGuard.waitUntilAllLeaveAfterLocked(false);
		cLockRemoteGuard.lock();
		cLockRemoteGuard.waitUntilAllLeaveAfterLocked(false);
		privilegedTaskManager.waitUntilCLocksDrained();
		masterPid = -1;
		remotePeerHandler.onMasterAfterRetirement();
		synchronized(masterRetirement)
		{
			masterRetirement.setRetiring(false);			
		}
		startElection();
		return true;
	}

	@Override
	public List<CLockHandshake> getCLockHandshakes() throws RemoteException {
		return privilegedTaskManager.getCLockHandshakes(false);
	}

	@Override
	public H onHeartbeat(int srcPid, H heartbeatData) throws RemoteException {
		remotePeerHandler.applyHeartbeatData(srcPid, heartbeatData);
		return remotePeerHandler.getHeartbeatData();
	}

	@Override
	public boolean receiveDLockRequest(DLockUnicastRequest dLockRequest) throws RemoteException {
		return executeGuardAware(dLockRemoteGuard, false, () -> {
			return privilegedTaskManager.receiveDLockRequest(dLockRequest);
		});
	}

	@Override
	public void receiveDLockResponse(DLockUnicastResponse dLockResponse) throws RemoteException {
		privilegedTaskManager.receiveDLockResponse(dLockResponse);
	}

	@Override
	public TransactionID receiveCLockRequest(CLockRequest cLockRequest) throws RemoteException {
		Container<TransactionID> container = new Container<>();
		executeGuardAware(cLockRemoteGuard, cLockRequest.getSrcPid() == pid, () -> {
			if(isMaster())
			{
				container.setValue(privilegedTaskManager.handleCLockRequest(cLockRequest));
				return true;
			}
			else
				return false;
		});
		return container.getValue();
	}

	@Override
	public void receiveCLockRelease(TimestampedEntity timestampedEntity) throws RemoteException {
		privilegedTaskManager.handleCLockRelease(timestampedEntity);
	}

	@Override
	public boolean receiveCLockApprove(long timestamp) throws RemoteException {
		return privilegedTaskManager.receiveCLockApprove(timestamp);
	}

	@Override
	public DPrivilegedTaskWrapper sendDLockRequests(String lockId, DPrivilegedTask dPrivilegedTask, LocalDLockSenderListener localDLockReceiver) {
		synchronized (localDLockReceiver)
		{
			long timestamp = timestampedEntityClock.getAndIncrement();
			Set<Integer> dstPids = new HashSet<>(), nonResponseblePids;
			Container<Set<Integer>> container = new Container<>();
			Map<Integer, RemotePeerConnector<E, H>> snapshotedPeers = getRemotePeersSnapshot();
			dstPids.addAll(snapshotedPeers.keySet());
			DLockMulticastRequest multicastRequest = new DLockMulticastRequest(lockId, timestamp, pid, dstPids);
			DPrivilegedTaskWrapper wrapper = new DPrivilegedTaskWrapper(dPrivilegedTask);
			localDLockReceiver.onBeforeSendDLockRequest(multicastRequest, wrapper);
			nonResponseblePids = executeRemoteAware(snapshotedPeers, -1, (p, r) -> {
				if(!r.receiveDLockRequest(new DLockUnicastRequest(lockId, timestamp, pid, p)))
				{
					if(container.getValue() == null)
						container.setValue(new HashSet<>());
					container.getValue().add(p);
				}
			});
			Set<Integer> value = container.getValue();
			if(nonResponseblePids != null)
			{
				if(value != null)
					nonResponseblePids.addAll(value);
			}
			else if(value != null)
				nonResponseblePids = value;
			localDLockReceiver.onAfterSendDLockRequest(nonResponseblePids, timestamp);
			return wrapper;
		}
	}

	@Override
	public void sendDLockResponse(DLockUnicastResponse response) {
		int dstPid = response.getDstPid();
		executeRemoteAware(dstPid, null, () -> getRemotePeerConnector(dstPid).receiveDLockResponse(response));
	}

	@Override
	public CRequestResult sendCLockRequest(TimestampedEntity timestampedEntity,
			MasterChangeListener masterChangeListener) {
		Container<TransactionID> container = new Container<>();
		boolean status = executeRemoteAware(masterPid, masterChangeListener, () -> {
				container.setValue(getRemotePeerMasterConnector().receiveCLockRequest(new CLockRequest(masterPid, timestampedEntity)));
				});
		
		CRequestStatus requestStatus;
		TransactionID transactionId = container.getValue();
		
		if(!status)
			requestStatus = CRequestStatus.MASTER_UNAVAILABLE;
		else if(transactionId == null)
			{
				remoteFailureDetector.reportMasterRetirement(masterChangeListener);
				requestStatus = CRequestStatus.CLOCK_REQUEST_NOT_SENT;
			}
		else
			requestStatus = CRequestStatus.CLOCK_REQUEST_SENT;
		
		return new CRequestResult(requestStatus, transactionId);
	}

	@Override
	public boolean sendCLockRelease(long timestamp, MasterChangeListener masterChangeListener) {
		return executeRemoteAware(masterPid, masterChangeListener, () -> {
			getRemotePeerMasterConnector().receiveCLockRelease(new TimestampedEntity(pid, timestamp));
		});
	}

	@Override
	public boolean sendCLockApprove(TimestampedEntity timestampedEntity) {
		int dstPid = timestampedEntity.getSrcPid();
		return executeRemoteAware(dstPid, () -> 
			getRemotePeerConnector(dstPid).receiveCLockApprove(timestampedEntity.getTimestamp())
		);
	}
	
	@Override
	public void onShutdown(int srcPid) {
		this.remotePeerHandler.onRemotePeerShutdown(srcPid);
	}

	@Override
	public long getTerm() {
		return this.termClock.getClock();
	}

	@Override
	public RemotePeerConnectorExporter getRemotePeerConnectorExporter() {
		return remotePeerConnectorProvider;
	}

	@Override
	public void shutdown(String shutdownPassword) throws RemoteException {
		if(this.shutdownPassword.equals(shutdownPassword))
			{
				RemoteLogger.getInstance(pid).info("Remote Shutdown Request received and approved");
				shutdown(true);		
			}
	}

	@Override
	public RemotePeerConnectorConfiguration getRemotePeerConnectorConfiguration() {
		return configuration;
	}

	@Override
	public long getCurrentTimestamp() throws RemoteException {
		return this.timestampedEntityClock.getClock();
	}
	
	@Override
	public long getLastSeenTerm() throws RemoteException {
		return this.termClock.getClock();
	}

	@Override
	public void dump(String command) throws RemoteException {
		RemoteLogger.getInstance(pid).info("Command " + command + " arrived");
		switch(command) {
		case "dumpCPrivilegedTasks":
			privilegedTaskManager.dumpCPrivilegedTasks();
			break;
		case "dumpFailureDetector":
			remoteFailureDetector.dumpFailureDetector();
			break;
		case "dumpCLocks":
			privilegedTaskManager.dumpCLocks();
			break;
		case "shutdownCPrivilegedTasks":
			privilegedTaskManager.forceAllCPrivilegedTasksShutdown();
			break;
		}
	}

		
}
