package org.distribooty.connector;

import java.util.Set;

import org.distribooty.MasterChangeListener;

public interface RemoteFailureDetector extends RemoteFailureAwareContext {
	/**
	 * Calls {@link ds.MasterChangeListener.onMasterLost} at listener. If a new master has been elected meanwhile, call
	 * MasterChangeListener.onMasterChange, if no, the onMasterChange method will be called as
	 * soon a new master is elected. Each method above will be called only once for the established session.
	 * The session is established by calling this method.
	 * @param listener
	 */
	void reportMasterFailure(MasterChangeListener listener);
	void reportMasterRetirement(MasterChangeListener listener);
	void reportRemoteFailure(int pid);
	void reportRemoteFailure(Set<Integer> pids);
	void reportMasterChange(int masterPid);
	void reportMasterInCharge(boolean blocking, boolean before);
	void reportMasterRetirement(boolean blocking, boolean before);	
}
