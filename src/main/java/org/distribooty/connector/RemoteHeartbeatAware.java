package org.distribooty.connector;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteHeartbeatAware<H extends Serializable> extends Remote {
	H onHeartbeat(int srcPid, H heartbeatData) throws RemoteException;
}
