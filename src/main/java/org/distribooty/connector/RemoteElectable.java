package org.distribooty.connector;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.List;

import org.distribooty.ElectionResult;
import org.distribooty.MasterAcceptingRefusedException;
import org.distribooty.RemoteIdentifiable;
import org.distribooty.RemoteInfo;
import org.distribooty.SlaveStatusHint;
import org.distribooty.lock.centralized.CLockHandshake;

public interface RemoteElectable<E extends Serializable & Comparable<E>> extends RemoteIdentifiable {
	long DELTA = 1000L;
	String ELECTION_LOCK_ID = "ds.connector.RemoteElectable-lockId";
	E startElection(int srcPid) throws RemoteException;
	void delegateMastership(int srcPid, ElectionResult electionResult) throws RemoteException, MasterAcceptingRefusedException;
	void onMasterElected(int masterPid, long term, SlaveStatusHint slaveStatusHint) throws RemoteException;
	void onElectionFailed(int srcPid, Throwable reason) throws RemoteException;
	boolean isMaster() throws RemoteException;
	boolean forceReelection(int srcPid) throws RemoteException;
	long getLastSeenTerm() throws RemoteException;
	List<CLockHandshake> getCLockHandshakes() throws RemoteException;
	RemoteInfo getRemoteInfo() throws RemoteException;
}
