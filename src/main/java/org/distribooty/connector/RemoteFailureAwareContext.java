package org.distribooty.connector;

import java.rmi.Remote;
import java.util.Map;
import java.util.Set;

import org.distribooty.MasterChangeListener;
import org.distribooty.MasterListener;
import org.distribooty.RemotePeerFailureListener;
import org.distribooty.connector.function.RemoteAwarePeerExecutor;
import org.distribooty.connector.function.RemoteAwarePeerSingleExecutor;

public interface RemoteFailureAwareContext {

	/**
	 * Adds the listener for permanent methods: onMasterChange and onMasterLost calling.
	 * @param listener
	 */
	void addMasterChangeListener(MasterChangeListener listener);
	
	/**
	 * Removes the listener for permanent methods: onMasterChange and onMasterLost calling.
	 * @param listener
	 */
	void removeMasterChangeListener(MasterChangeListener listener);
	
	void addMasterListener(MasterListener listener);
	void removeMasterListener(MasterListener listener);	
	
	void addRemotePeerFailureListener(RemotePeerFailureListener listener);
	void removeRemotePeerFailureListener(RemotePeerFailureListener listener);
	
	/**
	 * Executes the executor. If RemoteException during execution is thrown, false is returned, else true.
	 * If RemoteException is thrown the executor has not to execute
	 * @param remotes
	 * @param executor
	 * @return failed pids if any, else null
	 */
	<T extends Remote> Set<Integer> executeRemoteAware(Map<Integer, T> remotes, RemoteAwarePeerExecutor<T> executor);
	<T extends Remote> boolean executeRemoteAware(Map.Entry<Integer, T> remote, RemoteAwarePeerSingleExecutor<T> executor);
}
