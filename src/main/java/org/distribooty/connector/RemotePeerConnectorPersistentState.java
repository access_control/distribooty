package org.distribooty.connector;

import java.io.Serializable;

import org.distribooty.RemoteState;
import org.distribooty.util.LamportClock;

public class RemotePeerConnectorPersistentState implements Serializable {
	
	private static final long serialVersionUID = 4473978142790315701L;
	
	private RemoteState remoteState;
	private LamportClock termClock;
	
	public RemotePeerConnectorPersistentState(RemoteState remoteState, LamportClock termClock) {
		this.remoteState = remoteState;
		this.termClock = termClock;
	}

	public RemoteState getRemoteState() {
		return remoteState;
	}

	public LamportClock getTermClock() {
		return termClock;
	}	
}
