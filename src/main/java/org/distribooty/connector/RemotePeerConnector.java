package org.distribooty.connector;

import java.io.Serializable;
import java.rmi.RemoteException;

import org.distribooty.lock.centralized.RemoteCLockAcquisitor;
import org.distribooty.lock.centralized.RemoteCLockProvider;
import org.distribooty.lock.decentralized.RemoteDLockAware;
import org.distribooty.util.RemoteShutdownable;


public interface RemotePeerConnector<E extends Serializable & Comparable<E>, H extends Serializable> 
extends RemoteDiscoverable, RemoteElectable<E>, RemoteHeartbeatAware<H>, 
RemoteDLockAware, RemoteCLockProvider, RemoteCLockAcquisitor, RemoteShutdownable, RemoteShutdownAware {
	boolean register(RemotePeerConnector<E, H> remotePeerConnector) throws RemoteException;
	void unregister(int srcPid) throws RemoteException;
}
