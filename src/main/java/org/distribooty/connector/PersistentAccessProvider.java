package org.distribooty.connector;

import java.io.Serializable;

public interface PersistentAccessProvider {
	Serializable readObject() throws Exception;
	void writeObject(Serializable obj) throws Exception;
}
