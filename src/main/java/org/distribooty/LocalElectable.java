package org.distribooty;

import java.io.Serializable;

public interface LocalElectable<E extends Serializable & Comparable<E>> {
	E getEvaluableObject();
	void onElectionEnd(int masterPid, long term, SlaveStatusHint hint, LocalPeerService localPeerService);
	void onElectionNonNeccesary(int masterPid, long term, boolean thisPeerMaster, LocalPeerService localPeerService);
	void onElectionFailure(int srcPid, Throwable reason);
}
