package test;

import java.rmi.RemoteException;
import java.util.List;

import org.distribooty.RemotePeerContact;
import org.distribooty.util.RemoteShutdownable;
import org.distribooty.util.RemoteUtils;

import com.beust.jcommander.IDefaultProvider;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.converters.IntegerConverter;
import com.beust.jcommander.validators.PositiveInteger;

public class RemoteShutdowner {
	
	private static class DefaultRemoteShutdownerOptions implements IDefaultProvider {
		
		private String defaultBindName;
		
		public DefaultRemoteShutdownerOptions(String defaultBindName) {
			this.defaultBindName = defaultBindName;
		}

		@Override
		public String getDefaultValueFor(String optionName) {
			
			switch(optionName)
			{
			case "-pid":
				return "-1";
			case "-host":
				return "localhost";
			case "-b":
				return defaultBindName;
			default:
				return null;
			}
		}
		
	}
	
	private static class RemoteShutdownerOptions {
		@Parameter(names = "-pid", converter = IntegerConverter.class, description = "pid of the shutdownable object (usually not required)")
		private int pid;
		
		@Parameter(names = "-ports", converter = IntegerConverter.class, validateWith = PositiveInteger.class, required = true, variableArity = true,
				description = "port(s) on which the shutdownable object(s) is (are) running")
		private List<Integer> ports;
		
		@Parameter(names = "-host", description = "host where the shutdownable object is running (default localhost)")
		private String host;
		
		@Parameter(names = "-b", description = "bind name of the shutdownable object which is running")
		private String bindName;
		
		@Parameter(names = "-sp", required = true, description = "the shutdown passoword which must be send to the shutdownable object")
		private String shutdownPassword;

		public int getPid() {
			return pid;
		}

		public List<Integer> getPorts() {
			return ports;
		}

		public String getHost() {
			return host;
		}

		public String getBindName() {
			return bindName;
		}

		public String getShutdownPassword() {
			return shutdownPassword;
		}	
		
	}

	public static void main(String[] args) throws RemoteException {
		RemoteShutdownerOptions options = new RemoteShutdownerOptions();
		DefaultRemoteShutdownerOptions defaultOptions = new DefaultRemoteShutdownerOptions("connect4");
		JCommander jc = null;
		try {
			jc = JCommander.newBuilder().defaultProvider(defaultOptions).addObject(options).build();
			jc.parse(args);
		} catch (ParameterException e) {
			e.printStackTrace();
			if(jc != null)
				jc.usage();
			return;
		}
		RemoteShutdownable rs;
		if(options.getPorts().size() == 1)
		{
			rs = RemoteUtils.retrieveRemoteObject(RemoteShutdownable.class, new RemotePeerContact(options.getPid(), options.getHost(), options.getPorts().get(0), options.getBindName()));
			if(rs != null)
				rs.shutdown(options.getShutdownPassword());
		}
		else
			for(int port : options.getPorts())
			{
				rs = RemoteUtils.retrieveRemoteObject(RemoteShutdownable.class, new RemotePeerContact(-1, "localhost", port, options.getBindName()));
				if(rs != null)
					rs.shutdown(options.getShutdownPassword());
			}
		
		System.out.println("Remote shutdown request is sent");
	}

}
