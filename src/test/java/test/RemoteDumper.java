package test;

import java.rmi.RemoteException;

import org.distribooty.RemotePeerContact;
import org.distribooty.util.Dumpable;
import org.distribooty.util.RemoteUtils;

import com.beust.jcommander.IDefaultProvider;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.converters.IntegerConverter;
import com.beust.jcommander.validators.PositiveInteger;

public class RemoteDumper {

private static class DefaultRemoteShutdownerOptions implements IDefaultProvider {
		
		private String defaultBindName;
		
		public DefaultRemoteShutdownerOptions(String defaultBindName) {
			this.defaultBindName = defaultBindName;
		}

		@Override
		public String getDefaultValueFor(String optionName) {
			
			switch(optionName)
			{
			case "-pid":
				return "-1";
			case "-host":
				return "localhost";
			case "-b":
				return defaultBindName;
			default:
				return null;
			}
		}
		
	}
	
	private static class RemoteShutdownerOptions {
		@Parameter(names = "-pid", converter = IntegerConverter.class, description = "pid of the shutdownable object (usually not required)")
		private int pid;
		
		@Parameter(names = "-port", converter = IntegerConverter.class, validateWith = PositiveInteger.class, required = true,
				description = "port on which the dumpable target is (are) running")
		private int port;
		
		@Parameter(names = "-host", description = "host where the shutdownable object is running (default localhost)")
		private String host;
		
		@Parameter(names = "-b", description = "bind name of the shutdownable object which is running")
		private String bindName;
		
		@Parameter(names = "-c", required = true, description = "the command which must be send to the dumpable target")
		private String command;

		public int getPid() {
			return pid;
		}

		public Integer getPort() {
			return port;
		}

		public String getHost() {
			return host;
		}

		public String getBindName() {
			return bindName;
		}

		public String getCommand() {
			return command;
		}	
		
	}
	
	public static void main(String[] args) {
		RemoteShutdownerOptions options = new RemoteShutdownerOptions();
		DefaultRemoteShutdownerOptions defaultOptions = new DefaultRemoteShutdownerOptions("connect4");
		JCommander jc = null;
		try {
			jc = JCommander.newBuilder().defaultProvider(defaultOptions).addObject(options).build();
			jc.parse(args);
		} catch (ParameterException e) {
			e.printStackTrace();
			if(jc != null)
				jc.usage();
			return;
		}
		Dumpable d;		
		d = RemoteUtils.retrieveRemoteObject(Dumpable.class, new RemotePeerContact(options.getPid(), options.getHost(), options.getPort(), options.getBindName()));
		if(d != null)
			try {
				d.dump(options.getCommand());
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		
				
		System.out.println("Remote shutdown request is sent");

	}

}
