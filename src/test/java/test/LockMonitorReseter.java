package test;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class LockMonitorReseter {

	public LockMonitorReseter() {
	}

	public static void main(String[] args) {
		
		for(String p : args)
		{
			int port;			
			try {
				port = Integer.parseInt(p);
				Registry registry = LocateRegistry.getRegistry(port);
				LockMonitor monitor = (LockMonitor) registry.lookup(LockMonitor.BIND_NAME);
				monitor.reset(null);
			} catch (RemoteException | NotBoundException | NumberFormatException e) {
				System.err.println(e.getMessage());
				continue;
			}
		}

	}

}
