package test.locktester;

import java.util.logging.Logger;

import org.distribooty.lock.centralized.CPrivilegedTask;

import test.LockMonitor;

public class CPrivilegedTaskFactory extends AbstractPrivilegedTaskFactory<CPrivilegedTask> {

	public CPrivilegedTaskFactory(LockMonitor lockMonitor, long duration, Logger logger) {
		super(lockMonitor, duration, logger);
	}

	@Override
	public CPrivilegedTask getNewPrivilegedTask(int taskId) {
		return new CPrivilegedTaskImpl(taskId, lockMonitor, duration, logger);
	}

}
