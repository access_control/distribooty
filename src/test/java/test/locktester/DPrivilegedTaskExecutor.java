package test.locktester;

import java.util.concurrent.Future;
import java.util.logging.Logger;

import org.distribooty.lock.PrivilegedTaskService;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.lock.decentralized.DPrivilegedTask;

public class DPrivilegedTaskExecutor extends PrivilegedTaskExecutor<DPrivilegedTask, String> {

	public DPrivilegedTaskExecutor(String id, Logger logger, PrivilegedTaskService taskService,
			PrivilegedTaskFactory<DPrivilegedTask> factory, long delay, LockSource<String> lockSource,
			int taskStartNumber) {
		super(id, logger, taskService, factory, delay, lockSource, taskStartNumber);
	}

	@Override
	protected Future<TimestampedEntity> launchTask(String lock, DPrivilegedTask privilegedTask) {
		return taskService.addPrivilegedTask(lock, privilegedTask);
	}

}
