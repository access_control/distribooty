package test.locktester;

import java.util.concurrent.Future;
import java.util.logging.Logger;

import org.distribooty.TransactionID;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.lock.decentralized.DLockMulticastRequest;
import org.distribooty.lock.decentralized.DPrivilegedTask;

import test.LockMonitor;

public class DPrivilegedTaskImpl extends PrivilegedTaskImpl implements DPrivilegedTask {
	
	public DPrivilegedTaskImpl(int taskId, LockMonitor lockMonitor, long duration, Logger logger) {
		super(taskId, lockMonitor, duration, logger);
	}

	@Override
	public void execute(TimestampedEntity timestampedEntity, TransactionID transactionId) throws Exception {
		lockMonitor.logLock((DLockMulticastRequest) timestampedEntity, "D-Lock from task-id " + taskId);
		Thread.sleep(duration);
		lockMonitor.logUnlock((DLockMulticastRequest) timestampedEntity, "D-Lock from task-id " + taskId);		
	}

	@Override
	public void onTimeout(TimestampedEntity timestampedEntity, Future<TimestampedEntity> context) {
		logger.info("DPrivilegedTask.onTimeout with id " + taskId + " srcPid " +
				timestampedEntity.getSrcPid() + " timestamp " + timestampedEntity.getTimestamp());
	}

	@Override
	public void onCancel(TimestampedEntity timestampedEntity) {
		logger.info("DPrivilegedTask.onCancel with id " + taskId + " srcPid " +
				timestampedEntity.getSrcPid() + " timestamp " + timestampedEntity.getTimestamp());
		
	}
}