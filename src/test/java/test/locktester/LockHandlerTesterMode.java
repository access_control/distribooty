package test.locktester;

public enum LockHandlerTesterMode {
	ONLY_DISCOVERY, D_LOCKS, ELECTION, C_SIMPLE_LOCKS;
}
