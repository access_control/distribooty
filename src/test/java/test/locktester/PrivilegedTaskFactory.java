package test.locktester;

import org.distribooty.lock.PrivilegedTask;

public interface PrivilegedTaskFactory<T extends PrivilegedTask> {
	T getNewPrivilegedTask(int taskId);
}
