package test.locktester;

import java.util.concurrent.Future;
import java.util.logging.Logger;

import org.distribooty.lock.PrivilegedTaskService;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.lock.centralized.CPrivilegedTask;
import org.distribooty.lock.centralized.CSimpleLock;

public class CPrivilegedTaskExecutor extends PrivilegedTaskExecutor<CPrivilegedTask, CSimpleLock[]> {

	public CPrivilegedTaskExecutor(String id, Logger logger, PrivilegedTaskService taskService,
			PrivilegedTaskFactory<CPrivilegedTask> factory, long delay, LockSource<CSimpleLock[]> lockSource,
			int taskStartNumber) {
		super(id, logger, taskService, factory, delay, lockSource, taskStartNumber);
	}

	@Override
	protected Future<TimestampedEntity> launchTask(CSimpleLock[] lock, CPrivilegedTask privilegedTask) {
		return taskService.addPrivilegedTask(lock, privilegedTask);
	}

}
