package test.locktester;

import java.util.logging.Logger;

import test.LockMonitor;

public abstract class PrivilegedTaskImpl {

	protected final int taskId;
	protected final LockMonitor lockMonitor;
	protected final long duration;
	protected final Logger logger;
	
	public PrivilegedTaskImpl(int taskId, LockMonitor lockMonitor, long duration, Logger logger) {
		this.taskId = taskId;
		this.lockMonitor = lockMonitor;
		this.duration = duration;
		this.logger = logger;
	}
}
