package test.locktester;

import java.util.List;

import org.distribooty.lock.PrivilegedTask;

public class PrivilegedTaskExecutorLauncher<T extends PrivilegedTask, L> {

	private List<? extends PrivilegedTaskExecutor<T, L>> executors;
	private boolean shutdownAfterTasksCommited;
	
	public PrivilegedTaskExecutorLauncher(List<? extends PrivilegedTaskExecutor<T, L>> executors, boolean shutdownAfterTasksCommited) {
		this.executors = executors;
		this.shutdownAfterTasksCommited = shutdownAfterTasksCommited;
	}

	public void launch() {
		Thread[] threads = new Thread[executors.size()];
		int i = 0;
		for(PrivilegedTaskExecutor<T, L> executor : executors)
			{
				Thread t = new Thread(executor);
				t.start();
				threads[i++] = t;
			}
		if(shutdownAfterTasksCommited)
			for(Thread thread : threads)
				try {
					thread.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
	}

	public boolean isShutdownAfterTasksCommited() {
		return shutdownAfterTasksCommited;
	}
}
