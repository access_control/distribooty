package test.locktester;

public interface LockSource<L> {
	L getNextLock();
}
