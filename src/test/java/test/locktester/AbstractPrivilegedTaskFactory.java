package test.locktester;

import java.util.logging.Logger;

import org.distribooty.lock.PrivilegedTask;

import test.LockMonitor;

public abstract class AbstractPrivilegedTaskFactory<T extends PrivilegedTask> implements PrivilegedTaskFactory<T>{

	protected final LockMonitor lockMonitor;
	protected final long duration;
	protected final Logger logger;
	
	public AbstractPrivilegedTaskFactory(LockMonitor lockMonitor, long duration, Logger logger) {
		this.lockMonitor = lockMonitor;
		this.duration = duration;
		this.logger = logger;
	}
}
