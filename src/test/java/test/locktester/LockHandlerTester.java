package test.locktester;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.Objects;

import org.distribooty.BasicRemotePeerService;
import org.distribooty.DefaultHeartbeatData;
import org.distribooty.DefaultRemotePeerHandler;
import org.distribooty.InitRemotePeerService;
import org.distribooty.LocalPeerService;
import org.distribooty.RemotePeerContact;
import org.distribooty.RemotePeerDescriptor;
import org.distribooty.SlaveStatusHint;
import org.distribooty.StartupMode;
import org.distribooty.lock.PrivilegedTaskService;
import org.distribooty.util.RemoteUtils;
import org.distribooty.util.Shutdownable;
import org.hjson.JsonObject;

import test.LockMonitor;

public class LockHandlerTester extends DefaultRemotePeerHandler {

	private boolean lockTasksCommited = false;
	private LockHandlerTesterMode mode;
	private JsonObject config;
	private LockMonitor lockMonitor;
	private int allPeers;
	
	public LockHandlerTester(long value, int pid, int lockMonitorPort, LockHandlerTesterMode mode, JsonObject config) {
		super(value, pid);
		this.mode = mode;
		this.lockMonitor = RemoteUtils.retrieveRemoteObject(LockMonitor.class, new RemotePeerContact(-1, "localhost", lockMonitorPort, LockMonitor.BIND_NAME));
		Objects.requireNonNull(this.lockMonitor);
		this.config = config;
	}
	
	@Override
	public void onInit(StartupMode startupMode, InitRemotePeerService initRemotePeerService) {
		super.onInit(startupMode, initRemotePeerService);
		allPeers = initRemotePeerService.getRemoteContacts().size();
	}
	
	private void launch(PrivilegedTaskExecutorLauncher<?, ?> launcher, Shutdownable service) {
		launcher.launch();
		lockTasksCommited = true;
		if(launcher.isShutdownAfterTasksCommited())
			service.shutdown(true);
	}

	@Override
	public void onNeighbourDiscoveryChange(Map<Integer, RemotePeerDescriptor> retrievedPeers,
			BasicRemotePeerService service) {
		super.onNeighbourDiscoveryChange(retrievedPeers, service);
		if(mode == LockHandlerTesterMode.D_LOCKS && retrievedPeers.size() == allPeers - 1 && !lockTasksCommited)
		{
			launch(PrivilegedTaskLauncherInitializer.getDPrivilegedTaskExecutorLauncher(
							config.get("execDLock").asObject(), 
							lockMonitor, 
							logger, 
							(PrivilegedTaskService) service),
					service);
		}
		if(mode == LockHandlerTesterMode.ELECTION && 
				!service.getRemotePeerConnectorConfiguration().isStartElectionAutomatically() && 
				retrievedPeers.size() == allPeers - 1)
			service.startElection();
	}
	
	private void execCLocks(PrivilegedTaskService taskService) {
		if(mode == LockHandlerTesterMode.C_SIMPLE_LOCKS && !lockTasksCommited)
		{
			launch(PrivilegedTaskLauncherInitializer.getCPrivilegedTaskExecutorLauncher(
							config.get("execCLock").asObject(), 
							lockMonitor, 
							logger, 
							taskService), 
					(Shutdownable) taskService);
		}
	}

	@Override
	public void onElectionEnd(int masterPid, long term, SlaveStatusHint hint, LocalPeerService localPeerService) {
		super.onElectionEnd(masterPid, term, hint, localPeerService);
		execCLocks(localPeerService);
	}

	@Override
	public void onElectionNonNeccesary(int masterPid, long term, boolean thisPeerMaster,
			LocalPeerService localPeerService) {
		super.onElectionNonNeccesary(masterPid, term, thisPeerMaster, localPeerService);
		execCLocks(localPeerService);
	}
	
	
	
	@Override
	public void applyHeartbeatData(int srcPid, DefaultHeartbeatData heartbeatData) {
		// TODO Auto-generated method stub
		//super.applyHeartbeatData(srcPid, heartbeatData);
	}

	@Override
	public DefaultHeartbeatData getHeartbeatData() {
		// TODO Auto-generated method stub
		return null; //super.getHeartbeatData();
	}

	public static LockHandlerTester getInstance(LockHandlerTesterMode mode, JsonObject config) {
		JsonObject handler = config.get("handler").asObject();
		int value = handler.get("votingParams").asInt();
		int lockMonitorPort = handler.get("lockMonitorPort").asInt();
		int pid = config.get("connector").asObject().get("pid").asInt();
		return new LockHandlerTester(value, pid, lockMonitorPort, mode, config);
	}

	@Override
	public void onMasterBeforeInCharge() {
		super.onMasterBeforeInCharge();
		try {
			lockMonitor.reset(null);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	
}
