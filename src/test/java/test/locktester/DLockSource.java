package test.locktester;

public class DLockSource implements LockSource<String> {

	private final String lockId;
	private final int tasksCount;
	private int taskCounter = 0;
	
	public DLockSource(String lockId, int tasksCount) {
		this.lockId = lockId;
		this.tasksCount = tasksCount;
	}

	@Override
	public String getNextLock() {
		if(taskCounter < tasksCount)
		{
			taskCounter++;
			return lockId;
		}
		else 
			return null;
	}

}
