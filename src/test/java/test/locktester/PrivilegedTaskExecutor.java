package test.locktester;

import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.distribooty.lock.PrivilegedTask;
import org.distribooty.lock.PrivilegedTaskService;
import org.distribooty.lock.TimestampedEntity;

public abstract class PrivilegedTaskExecutor<T extends PrivilegedTask, L> implements Runnable {
	
	protected final String id;
	protected final Logger logger;
	protected final PrivilegedTaskService taskService;
	protected final PrivilegedTaskFactory<T> factory;
	protected final long delay;
	protected final LockSource<L> lockSource;
	protected int taskStartNumber;
	
	public PrivilegedTaskExecutor(String id, Logger logger, PrivilegedTaskService taskService, PrivilegedTaskFactory<T> factory,
			long delay, LockSource<L> lockSource, int taskStartNumber) {
		this.id = id;
		this.logger = logger;
		this.taskService = taskService;
		this.factory = factory;
		this.delay = delay;
		this.lockSource = lockSource;
		this.taskStartNumber = taskStartNumber;
	}

	protected abstract Future<TimestampedEntity> launchTask(L lock, T privilegedTask);

	@Override
	public void run() {		
		while(true)
		{
			L lock = lockSource.getNextLock();
			if(lock == null)
			{
				logger.info("The lock source at executor " + id + " is exhausted");
				break;
			}
			if(launchTask(lock, factory.getNewPrivilegedTask(taskStartNumber++)) == null)
			{
				logger.warning("The starting of task at executor " + id + " with taskId " + (taskStartNumber - 1) + " failed"); 
				break;
			}
			
			logger.info("Executor " + id + " started task with taskId " + (taskStartNumber - 1));
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
				logger.log(Level.WARNING, "Executor Id " + id, e);
			}
		}
	}

}
