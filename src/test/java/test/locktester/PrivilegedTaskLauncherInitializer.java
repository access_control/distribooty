package test.locktester;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.distribooty.lock.PrivilegedTaskService;
import org.distribooty.lock.centralized.CPrivilegedTask;
import org.distribooty.lock.centralized.CSimpleLock;
import org.distribooty.lock.decentralized.DPrivilegedTask;
import org.hjson.JsonArray;
import org.hjson.JsonObject;
import org.hjson.JsonValue;

import test.LockMonitor;

public class PrivilegedTaskLauncherInitializer {

	public static PrivilegedTaskExecutorLauncher<DPrivilegedTask, String> 
	getDPrivilegedTaskExecutorLauncher(JsonObject configObject, LockMonitor lockMonitor, Logger logger, PrivilegedTaskService taskService) {
		List<DPrivilegedTaskExecutor> executors = new ArrayList<>();
		int tasksStartNumber = 0;
		int id = 0;
		JsonArray configArray = configObject.get("lockTasks").asArray();
		for(JsonValue v : configArray)
		{
			JsonObject jo = v.asObject();
			int threads = jo.get("threads").asInt();
			int tasksPerThread = jo.get("tasksPerThread").asInt();
			long duration = jo.get("duration").asLong();
			long delay = jo.get("delay").asLong();
			String lockId = jo.get("lockId").asString();
			for(int i = 0; i < threads; i++)
			{
				DPrivilegedTaskFactory factory = new DPrivilegedTaskFactory(lockMonitor, duration, logger);
				DLockSource lockSource = new DLockSource(lockId, tasksPerThread);
				DPrivilegedTaskExecutor executor = new DPrivilegedTaskExecutor(Integer.toString(id++), logger, taskService, factory, delay, lockSource, tasksStartNumber);
				tasksStartNumber += tasksPerThread;
				executors.add(executor);
			}			
		}
		PrivilegedTaskExecutorLauncher<DPrivilegedTask, String> launcher = 
				new PrivilegedTaskExecutorLauncher<>(executors, configObject.get("shutdownAfterTasksCommited").asBoolean());
		return launcher;
	}
	
	public static PrivilegedTaskExecutorLauncher<CPrivilegedTask, CSimpleLock[]>
	getCPrivilegedTaskExecutorLauncher(JsonObject configObject, LockMonitor lockMonitor, Logger logger, PrivilegedTaskService taskService) {
		JsonArray configArray = configObject.get("lockTasks").asArray();
		List<CPrivilegedTaskExecutor> executors = new ArrayList<>();
		List<List<CSimpleLock[]>> collectedLocks = new ArrayList<>();
		for(JsonValue thread : configArray)
		{
			List<CSimpleLock[]> collectedLocksForTasks = new ArrayList<>();
			for(JsonValue task : thread.asArray())
			{
				List<CSimpleLock> locks = new ArrayList<>();
				for(JsonValue lock : task.asArray())
				{
					JsonObject lockData = lock.asObject();
					CSimpleLock cLock = new CSimpleLock(lockData.get("lockId").asString(), lockData.get("exclusive").asBoolean());
					locks.add(cLock);
				}
				collectedLocksForTasks.add(locks.toArray(new CSimpleLock[0]));
			}
			collectedLocks.add(collectedLocksForTasks);
		}
		long duration = configObject.get("duration").asLong();
		long delay = configObject.get("delay").asLong();
		CPrivilegedTaskFactory factory = new CPrivilegedTaskFactory(lockMonitor, duration, logger);
		int taskStartNumber = 0;
		int id = 0;
		for(List<CSimpleLock[]> tasksPerThread : collectedLocks)
		{
			CSimpleLockSource lockSource = new CSimpleLockSource(tasksPerThread);
			CPrivilegedTaskExecutor executor = new CPrivilegedTaskExecutor(Integer.toString(id++), logger, taskService, factory, delay, lockSource, taskStartNumber);
			taskStartNumber += tasksPerThread.size();
			executors.add(executor);
		}		
		PrivilegedTaskExecutorLauncher<CPrivilegedTask, CSimpleLock[]> launcher = 
				new PrivilegedTaskExecutorLauncher<>(executors, configObject.get("shutdownAfterTasksCommited").asBoolean());
		return launcher;
	}

}
