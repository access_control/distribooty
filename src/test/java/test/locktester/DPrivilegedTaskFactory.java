package test.locktester;

import java.util.logging.Logger;

import org.distribooty.lock.decentralized.DPrivilegedTask;

import test.LockMonitor;

public class DPrivilegedTaskFactory extends AbstractPrivilegedTaskFactory<DPrivilegedTask> {

	public DPrivilegedTaskFactory(LockMonitor lockMonitor, long duration, Logger logger) {
		super(lockMonitor, duration, logger);
	}

	@Override
	public DPrivilegedTask getNewPrivilegedTask(int taskId) {
		return new DPrivilegedTaskImpl(taskId, lockMonitor, duration, logger);
	}

}
