package test.locktester;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.distribooty.lock.centralized.CSimpleLock;

public class CSimpleLockSource implements LockSource<CSimpleLock[]> {

	private final List<CSimpleLock[]> locks;
	private Iterator<CSimpleLock[]> iterator;
	
	public CSimpleLockSource(List<CSimpleLock[]> locks) {
		this.locks = locks;
	}
	
	@Override
	public CSimpleLock[] getNextLock() {
		if(iterator == null)
			iterator = locks.iterator();
		try {
			return iterator.next();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

}
