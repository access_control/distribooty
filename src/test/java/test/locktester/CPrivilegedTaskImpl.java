package test.locktester;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import org.distribooty.TransactionID;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.lock.centralized.CPrivilegedTask;
import org.distribooty.lock.centralized.CPrivilegedTaskExecutionState;
import org.distribooty.lock.centralized.TimestampedCSimpleLocks;

import test.LockMonitor;

public class CPrivilegedTaskImpl extends PrivilegedTaskImpl implements CPrivilegedTask {

	public CPrivilegedTaskImpl(int taskId, LockMonitor lockMonitor, long duration, Logger logger) {
		super(taskId, lockMonitor, duration, logger);
	}

	@Override
	public void execute(TimestampedEntity timestampedEntity, TransactionID transactionId) throws Exception {
		lockMonitor.logLock((TimestampedCSimpleLocks) timestampedEntity, "C-Lock from task-id " + taskId);
		Thread.sleep(duration);
		lockMonitor.logUnlock((TimestampedCSimpleLocks) timestampedEntity, "C-Lock from task-id " + taskId);

	}

	@Override
	public void onStateChange(CPrivilegedTaskExecutionState currentState, Future<TimestampedEntity> context) {
		TimestampedEntity entity;
		
		try {
			entity = context.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			return;
		}
		
		logger.info("CPrivilegedTask.onStateChange with id " + taskId + 
				" srcPid " + entity.getSrcPid() + " timestamp " + entity.getTimestamp() + 
				" with task state " + currentState.name());
	}

	@Override
	public void onCancel(TimestampedEntity timestampedEntity, CPrivilegedTaskExecutionState lastState) {
		logger.info("CPrivilegedTask.onCancel with id " + taskId + " srcPid " +
		timestampedEntity.getSrcPid() + " timestamp " + timestampedEntity.getTimestamp() +
		" with last task state " + lastState.name());
	}
	
}
