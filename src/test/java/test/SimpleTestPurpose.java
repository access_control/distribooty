package test;

import java.nio.file.Files;
import java.nio.file.Path;

import org.distribooty.connector.DefaultPersistentAccessProvider;
import org.distribooty.connector.RemotePeerConnectorPersistentState;

public class SimpleTestPurpose {
	
	public static void printStackTrace() {
		StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		for(StackTraceElement s : ste)
			System.out.println(s.getClassName() + "/" + s.getMethodName());
	}

	public static void main(String[] args) throws Exception {
		
		Path p = Path.of("build", "classes");
		if(!Files.exists(p))
		{
			System.out.println("Not existing");
			return;
		}
		else
			System.out.println("Exists");
		if(!Files.isDirectory(p))
			System.out.println("is not directory");
		else
			System.out.println("is directory");
		Files.list(p.toAbsolutePath()).filter(f -> 
			{
				return f.getFileName().toString().endsWith(".dat");
			}
		).forEachOrdered(f -> {
			DefaultPersistentAccessProvider provider = new DefaultPersistentAccessProvider(f);
			RemotePeerConnectorPersistentState state;
			try {
				System.out.println(f.getFileName());
				state = (RemotePeerConnectorPersistentState) provider.readObject();
				System.out.println("Last Remote State: " + state.getRemoteState().name());
				System.out.println("Last term: " + state.getTermClock().getClock());
				System.out.println("");
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

}
