package test.testcases;

import static org.junit.jupiter.api.Assertions.*;

import org.distribooty.util.Range;
import org.junit.jupiter.api.Test;

class RangeTestCases {
	
	/*
	 * B1_B2 = [B1;B2] | B1 <= B2 : ∀ c => B1 ≤ c ≤ B2 // Special case [a;b] if a = b => c = a = b
	 * b1_b2 = (b1;b2) | b1 < b2 : ∀ c => b1 < c < b2
	 * B1_b2 = [B1;b2) | B1 < b2 : ∀ c => B1 ≤ c < b2
	 * b1_B2 = (b1;B2] | b1 < B2 : ∀ c => b1 < c ≤ B2
	 * _B = (-∞;B] | ∀ c => c ≤ B
	 * _b = (-∞;b) | ∀ c => c < b
	 * B_ = [B;+∞) | ∀ c => c ≥ B
	 * b_ = (b;+∞) | ∀ c => c > B
	 * __ = (-∞;+∞) | ∀ c
	 */
	private static Range<String> D_K = new Range<String>("d", true, "k", true),
								D_k = new Range<String>("d", true, "k", false),
								d_k = new Range<String>("d", false, "k", false),
								d_K = new Range<String>("d", false, "k", true),
								A_d = new Range<String>("a", true, "d", false),
								a_d = new Range<String>("a", false, "d", false),
								A_D = new Range<String>("a", true, "d", true),
								a_D = new Range<String>("a", false, "d", true),
								A_G = new Range<String>("a", true, "g", true),
								A_g = new Range<String>("a", true, "g", false),
								a_g = new Range<String>("a", false, "g", false),
								a_G = new Range<String>("a", false, "g", true),
								A_K = new Range<String>("a", true, "k", true),
								A_k = new Range<String>("a", true, "k", false),
								a_k = new Range<String>("a", false, "k", false),
								a_K = new Range<String>("a", false, "k", true),
								D_M = new Range<String>("d", true, "m", true),
								D_m = new Range<String>("d", true, "m", false),
								d_m = new Range<String>("d", false, "m", false),
								d_M = new Range<String>("d", false, "m", true),
								_G = new Range<String>(null, false, "g", true),
								_g = new Range<String>(null, true, "g", false),
								G_ = new Range<String>("g", true, null, false),
								g_ = new Range<String>("g", false, null, true),
								C_ = new Range<String>("c", true, null),
								c_ = new Range<String>("c", false, null),
								_C = new Range<String>(null, "c", true),
								_c = new Range<String>(null, "c", false),
								F_H = new Range<String>("f", true, "h", true),
								F_h = new Range<String>("f", true, "h", false),
								f_h = new Range<String>("f", false, "h", false),
								f_H = new Range<String>("f", false, "h", true),
								A_A = new Range<String>("a", true, "a", true),
								A_a = new Range<String>("a", true, "a", false),
								a_a = new Range<String>("a", false, "a", false),
								a_A = new Range<String>("a", false, "a", true);
	

	@Test
	public void test01() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Range<String>("b", false, "a", false);
		});
	}
	
	@Test
	public void test02() {
		assertTrue(a_a.isLowerBoundInclusive());
		assertTrue(a_a.isUpperBoundInclusive());
		assertTrue(A_a.isLowerBoundInclusive());
		assertTrue(A_a.isUpperBoundInclusive());
		assertTrue(a_A.isLowerBoundInclusive());
		assertTrue(a_A.isUpperBoundInclusive());
		assertTrue(A_A.isLowerBoundInclusive());
		assertTrue(A_A.isUpperBoundInclusive());
	}
	
	@Test
	public void test03() {
		assertFalse(A_d.isContinuousWith(d_m));
		assertFalse(A_d.isContinuousWith(d_M));
		assertTrue(A_d.isContinuousWith(D_M));
		assertTrue(A_d.isContinuousWith(D_m));
		assertTrue(A_D.isContinuousWith(d_m));
		assertTrue(A_D.isContinuousWith(d_M));
		assertFalse(A_D.isContinuousWith(D_M));
		assertFalse(A_D.isContinuousWith(D_m));
		
		assertFalse(d_m.isContinuousWith(A_d));
		assertFalse(d_M.isContinuousWith(A_d));
		assertTrue(D_M.isContinuousWith(A_d));
		assertTrue(D_m.isContinuousWith(A_d));
		assertTrue(d_m.isContinuousWith(A_D));
		assertTrue(d_M.isContinuousWith(A_D));
		assertFalse(D_M.isContinuousWith(A_D));
		assertFalse(D_m.isContinuousWith(A_D));
	}
	
	@Test
	public void test04() {
		assertEquals(-1, A_D.compareTo(d_K));
		assertEquals(-1, A_D.compareTo(d_k));
		assertEquals(-1, a_D.compareTo(d_K));
		assertEquals(-1, a_D.compareTo(d_k));
		
		assertEquals(-1, A_d.compareTo(D_K));
		assertEquals(-1, A_d.compareTo(D_k));
		assertEquals(-1, a_d.compareTo(D_K));
		assertEquals(-1, a_d.compareTo(D_k));
		
		assertEquals(0, A_D.compareTo(D_K));
		assertEquals(0, A_D.compareTo(D_k));
		assertEquals(0, a_D.compareTo(D_K));
		assertEquals(0, a_D.compareTo(D_k));
		
		assertEquals(1, d_K.compareTo(A_D));
		assertEquals(1, d_k.compareTo(A_D));
		assertEquals(1, d_K.compareTo(a_D));
		assertEquals(1, d_k.compareTo(a_D));
		
		assertEquals(1, D_K.compareTo(A_d));
		assertEquals(1, D_k.compareTo(A_d));
		assertEquals(1, D_K.compareTo(a_d));
		assertEquals(1, D_k.compareTo(a_d));
		
		assertEquals(0, D_K.compareTo(A_D));
		assertEquals(0, D_k.compareTo(A_D));
		assertEquals(0, D_K.compareTo(a_D));
		assertEquals(0, D_k.compareTo(a_D));
		
		assertEquals(0, A_G.compareTo(D_K));
		assertEquals(0, A_G.compareTo(D_k));
		assertEquals(0, A_G.compareTo(d_k));
		assertEquals(0, A_G.compareTo(d_K));
		
		assertEquals(0, A_g.compareTo(D_K));
		assertEquals(0, A_g.compareTo(D_k));
		assertEquals(0, A_g.compareTo(d_k));
		assertEquals(0, A_g.compareTo(d_K));
		
		assertEquals(0, a_g.compareTo(D_K));
		assertEquals(0, a_g.compareTo(D_k));
		assertEquals(0, a_g.compareTo(d_k));
		assertEquals(0, a_g.compareTo(d_K));
		
		assertEquals(0, a_G.compareTo(D_K));
		assertEquals(0, a_G.compareTo(D_k));
		assertEquals(0, a_G.compareTo(d_k));
		assertEquals(0, a_G.compareTo(d_K));
		
		/////
		assertEquals(0, D_K.compareTo(A_G));
		assertEquals(0, D_k.compareTo(A_G));
		assertEquals(0, d_k.compareTo(A_G));
		assertEquals(0, d_K.compareTo(A_G));
		
		assertEquals(0, D_K.compareTo(A_g));
		assertEquals(0, D_k.compareTo(A_g));
		assertEquals(0, d_k.compareTo(A_g));
		assertEquals(0, d_K.compareTo(A_g));
		
		assertEquals(0, D_K.compareTo(a_g));
		assertEquals(0, D_k.compareTo(a_g));
		assertEquals(0, d_k.compareTo(a_g));
		assertEquals(0, d_K.compareTo(a_g));
		
		assertEquals(0, D_K.compareTo(a_G));
		assertEquals(0, D_k.compareTo(a_G));
		assertEquals(0, d_k.compareTo(a_G));
		assertEquals(0, d_K.compareTo(a_G));
	}
	
	@Test
	public void test05() {
		assertEquals(null, A_d.merge(d_K, false));
		assertEquals(null, A_d.merge(d_k, false));
		assertEquals(null, a_d.merge(d_K, false));
		assertEquals(null, a_d.merge(d_k, false));
		
		assertEquals(null, d_K.merge(A_d, false));
		assertEquals(null, d_k.merge(A_d, false));
		assertEquals(null, d_K.merge(a_d, false));
		assertEquals(null, d_k.merge(a_d, false));
		
		// TODO with bigger gap
	}
	
	@Test
	public void test06() {
		assertEquals(null, A_K.getGapRange(D_M));
		assertEquals(null, A_K.getGapRange(D_m));
		assertEquals(null, A_K.getGapRange(d_m));
		assertEquals(null, A_K.getGapRange(d_M));
		
		assertEquals(null, A_k.getGapRange(D_M));
		assertEquals(null, A_k.getGapRange(D_m));
		assertEquals(null, A_k.getGapRange(d_m));
		assertEquals(null, A_k.getGapRange(d_M));
		
		assertEquals(null, a_k.getGapRange(D_M));
		assertEquals(null, a_k.getGapRange(D_m));
		assertEquals(null, a_k.getGapRange(d_m));
		assertEquals(null, a_k.getGapRange(d_M));
		
		assertEquals(null, a_K.getGapRange(D_M));
		assertEquals(null, a_K.getGapRange(D_m));
		assertEquals(null, a_K.getGapRange(d_m));
		assertEquals(null, a_K.getGapRange(d_M));
		
		
		
		assertEquals(null, D_M.getGapRange(A_K));
		assertEquals(null, D_m.getGapRange(A_K));
		assertEquals(null, d_m.getGapRange(A_K));
		assertEquals(null, d_M.getGapRange(A_K));
		
		assertEquals(null, D_M.getGapRange(A_k));
		assertEquals(null, D_m.getGapRange(A_k));
		assertEquals(null, d_m.getGapRange(A_k));
		assertEquals(null, d_M.getGapRange(A_k));
		
		assertEquals(null, D_M.getGapRange(a_k));
		assertEquals(null, D_m.getGapRange(a_k));
		assertEquals(null, d_m.getGapRange(a_k));
		assertEquals(null, d_M.getGapRange(a_k));
		
		assertEquals(null, D_M.getGapRange(a_K));
		assertEquals(null, D_m.getGapRange(a_K));
		assertEquals(null, d_m.getGapRange(a_K));
		assertEquals(null, d_M.getGapRange(a_K));
		
		
		assertEquals(null, A_D.getGapRange(d_K));
		assertEquals(null, A_D.getGapRange(d_k));
		assertEquals(null, A_d.getGapRange(D_K));
		assertEquals(null, A_d.getGapRange(D_k));
		
		assertEquals(null, a_D.getGapRange(d_K));
		assertEquals(null, a_D.getGapRange(d_k));
		assertEquals(null, a_d.getGapRange(D_K));
		assertEquals(null, a_d.getGapRange(D_k));
		
		assertEquals(null, d_K.getGapRange(A_D));
		assertEquals(null, d_k.getGapRange(A_D));
		assertEquals(null, D_K.getGapRange(A_d));
		assertEquals(null, D_k.getGapRange(A_d));
		
		assertEquals(null, d_K.getGapRange(a_D));
		assertEquals(null, d_k.getGapRange(a_D));
		assertEquals(null, D_K.getGapRange(a_d));
		assertEquals(null, D_k.getGapRange(a_d));
	}
	
	@Test
	public void test07() {
		Range<String> overlapped = A_K.getOverlappingRange(D_M);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertTrue(overlapped.isLowerBoundInclusive());
		assertTrue(overlapped.isUpperBoundInclusive());
		
		overlapped = A_k.getOverlappingRange(D_M);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertTrue(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		overlapped = A_k.getOverlappingRange(d_M);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		overlapped = A_K.getOverlappingRange(d_M);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertTrue(overlapped.isUpperBoundInclusive());
		
		overlapped = D_M.getOverlappingRange(A_K);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertTrue(overlapped.isLowerBoundInclusive());
		assertTrue(overlapped.isUpperBoundInclusive());
		
		overlapped = D_M.getOverlappingRange(A_k);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertTrue(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		overlapped = d_M.getOverlappingRange(A_k);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		overlapped = d_M.getOverlappingRange(A_K);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertTrue(overlapped.isUpperBoundInclusive());
		
		
		
		overlapped = A_K.getOverlappingRange(D_K);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertTrue(overlapped.isLowerBoundInclusive());
		assertTrue(overlapped.isUpperBoundInclusive());
		
		overlapped = A_K.getOverlappingRange(D_k);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertTrue(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		overlapped = A_k.getOverlappingRange(D_k);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertTrue(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		overlapped = A_k.getOverlappingRange(D_K);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertTrue(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		
		overlapped = A_K.getOverlappingRange(d_K);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertTrue(overlapped.isUpperBoundInclusive());
		
		overlapped = A_K.getOverlappingRange(d_k);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		overlapped = A_k.getOverlappingRange(d_k);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		overlapped = A_k.getOverlappingRange(d_K);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		
		overlapped = D_K.getOverlappingRange(D_M);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertTrue(overlapped.isLowerBoundInclusive());
		assertTrue(overlapped.isUpperBoundInclusive());
		
		overlapped = D_K.getOverlappingRange(d_M);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertTrue(overlapped.isUpperBoundInclusive());
		
		overlapped = d_K.getOverlappingRange(d_M);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertTrue(overlapped.isUpperBoundInclusive());
		
		overlapped = d_K.getOverlappingRange(D_M);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertTrue(overlapped.isUpperBoundInclusive());
		
		
		overlapped = D_k.getOverlappingRange(D_M);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertTrue(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		overlapped = D_k.getOverlappingRange(d_M);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		overlapped = d_k.getOverlappingRange(d_M);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		overlapped = d_k.getOverlappingRange(D_M);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
		
		// TODO generalize
		overlapped = D_M.getOverlappingRange(d_k);
		assertEquals("d", overlapped.getLowerBound());
		assertEquals("k", overlapped.getUpperBound());
		assertFalse(overlapped.isLowerBoundInclusive());
		assertFalse(overlapped.isUpperBoundInclusive());
	}
	
	@Test
	public void test08() {
		Range<String> gap = A_D.getGapRange(F_H);
		assertEquals("d", gap.getLowerBound());
		assertEquals("f", gap.getUpperBound());
		assertFalse(gap.isLowerBoundInclusive());
		assertFalse(gap.isUpperBoundInclusive());
		
		gap = A_D.getGapRange(f_H);
		assertEquals("d", gap.getLowerBound());
		assertEquals("f", gap.getUpperBound());
		assertFalse(gap.isLowerBoundInclusive());
		assertTrue(gap.isUpperBoundInclusive());
		
		gap = A_d.getGapRange(f_H);
		assertEquals("d", gap.getLowerBound());
		assertEquals("f", gap.getUpperBound());
		assertTrue(gap.isLowerBoundInclusive());
		assertTrue(gap.isUpperBoundInclusive());
		
		gap = A_d.getGapRange(F_H);
		assertEquals("d", gap.getLowerBound());
		assertEquals("f", gap.getUpperBound());
		assertTrue(gap.isLowerBoundInclusive());
		assertFalse(gap.isUpperBoundInclusive());
		
		// TODO generalize
		gap = F_H.getGapRange(A_d);
		assertEquals("d", gap.getLowerBound());
		assertEquals("f", gap.getUpperBound());
		assertTrue(gap.isLowerBoundInclusive());
		assertFalse(gap.isUpperBoundInclusive());
	}
	
	@Test
	public void test09() {
		Range<String> cloned = A_K.clone();
		assertEquals(A_K.getLowerBound(), cloned.getLowerBound());
		assertEquals(A_K.getUpperBound(), cloned.getUpperBound());
		assertTrue(A_K.isLowerBoundInclusive() == cloned.isLowerBoundInclusive());
		assertTrue(A_K.isUpperBoundInclusive() == cloned.isUpperBoundInclusive());
	}
	
	@Test
	public void test10() {
		Range<String> H_K = new Range<String>("h", true, "k", true);
		Range<String>[] ranges = A_K.cut(H_K);
		assertEquals(1, ranges.length);
		Range<String> cutted1 = ranges[0];
		assertEquals(cutted1.getLowerBound(), "a");
		assertTrue(cutted1.isLowerBoundInclusive());
		assertEquals(cutted1.getUpperBound(), "h");
		assertFalse(cutted1.isUpperBoundInclusive());
		Range<String> h_K = new Range<String>("h", false, "k", true);
		ranges = A_K.cut(h_K);
		assertEquals(1, ranges.length);
		cutted1 = ranges[0];
		assertEquals(cutted1.getLowerBound(), "a");
		assertTrue(cutted1.isLowerBoundInclusive());
		assertEquals(cutted1.getUpperBound(), "h");
		assertTrue(cutted1.isUpperBoundInclusive());
		Range<String> h_k = new Range<String>("h", false, "k", false);
		ranges = A_K.cut(h_k);
		assertEquals(2, ranges.length);
		cutted1 = ranges[0];
		Range<String> cutted2 = ranges[1];
		assertEquals(cutted1.getLowerBound(), "a");
		assertTrue(cutted1.isLowerBoundInclusive());
		assertEquals(cutted1.getUpperBound(), "h");
		assertTrue(cutted1.isUpperBoundInclusive());
		assertEquals(cutted2.getLowerBound(), "k");
		assertTrue(cutted2.isLowerBoundInclusive());
		assertEquals(cutted2.getUpperBound(), "k");
		assertTrue(cutted2.isUpperBoundInclusive());
		
		ranges = G_.cut(h_k);
		assertEquals(2, ranges.length);
		cutted1 = ranges[0];
		cutted2 = ranges[1];
		assertEquals(cutted1.getLowerBound(), "g");
		assertTrue(cutted1.isLowerBoundInclusive());
		assertEquals(cutted1.getUpperBound(), "h");
		assertTrue(cutted1.isUpperBoundInclusive());
		assertEquals(cutted2.getLowerBound(), "k");
		assertTrue(cutted2.isLowerBoundInclusive());
		assertEquals(cutted2.getUpperBound(), null);
		assertTrue(cutted2.isUpperBoundInclusive());
		
		ranges = _G.cut(h_k);
		assertEquals(ranges[0], _G);
	}
	
}
