package test.testcases.lockkeepers;

import org.distribooty.lock.LockEntity;
import org.distribooty.lock.TimestampedEntity;

public class LockMock extends TimestampedEntity {

	private static final long serialVersionUID = -1235732294920717861L;
	
	private LockEntity lockEntity;

	public LockMock(int srcPid, long timeStamp, LockEntity lockEntity) {
		super(srcPid, timeStamp);
		this.lockEntity = lockEntity;
	}

	public LockEntity getLockEntity() {
		return lockEntity;
	}
}
