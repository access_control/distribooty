package test.testcases;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import org.distribooty.lock.LockEntity;
import org.distribooty.lock.TimestampedEntity;
import org.distribooty.lock.centralized.CLockGraphVertex;
import org.distribooty.lock.centralized.CLockGraphVertexListener;
import org.distribooty.lock.centralized.CSimpleLockGraphVertexKeeperMemorySaving;
import org.distribooty.lock.centralized.DefaultCLockGraphVertex;
import org.distribooty.util.Cleanable;
import org.distribooty.util.Cleaner;

public class CLockVertexTester {
	
	private static class CustomInvocationHandler implements InvocationHandler {

		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			System.out.println("Calling " + method.getName());
			return null;
		}
		
	}
	
	private static class CustomListener implements CLockGraphVertexListener {
		
		private StringBuilder getStringifiedTimeStampedEntity(TimestampedEntity entity) {
			StringBuilder sb = new StringBuilder();
			sb.append("[").append(entity.getSrcPid()).append("/").append(entity.getTimestamp()).append("]");
			return sb;
		}
		
		private StringBuilder getStringifiedArguments(Object... args) {
			StringBuilder sb = new StringBuilder();
			boolean firstAppended = false;
			for(Object o : args)
			{
				if(firstAppended)
					sb.append(", ");
				else
					firstAppended = true;
				if(o instanceof DefaultCLockGraphVertex)
					sb.append(getStringifiedTimeStampedEntity(((DefaultCLockGraphVertex) o).getTimeStampedEntity()));
				else if(o instanceof Boolean)
					sb.append((Boolean) o);
				
			}
			return sb;
		}
		
		private StringBuilder getStringifiedMethodCall(Object... args) {
			StackTraceElement ste = Thread.currentThread().getStackTrace()[3];
			StringBuilder sb = new StringBuilder();
			sb.append(ste.getMethodName()).append("(").append(getStringifiedArguments(args)).append(")");
			return sb;
		}
		
		private void printMethodCall(Object...args) {
			System.out.println("Calling " + getStringifiedMethodCall(args));
		}

		@Override
		public void onRelease(CLockGraphVertex vertex) {
			printMethodCall(vertex);
		}

		@Override
		public void onIncommingVertexAdded(CLockGraphVertex incommingVertex, CLockGraphVertex targetVertex,
				boolean replacing) {
			printMethodCall(incommingVertex, targetVertex, replacing);			
		}

		@Override
		public void onIncommingVertexRemoved(CLockGraphVertex incommingVertex, CLockGraphVertex targetVertex,
				boolean replacing) {
			printMethodCall(incommingVertex, targetVertex, replacing);				
		}

		@Override
		public void onIncommingVertexReplaced(CLockGraphVertex oldIncommingVertex, CLockGraphVertex newIncommingVertex,
				CLockGraphVertex targetVertex) {
			printMethodCall(oldIncommingVertex, newIncommingVertex, targetVertex);	
		}

		@Override
		public void onIncommingVertexReleased(CLockGraphVertex releasedIncommingVertex, CLockGraphVertex targetVertex) {
			printMethodCall(releasedIncommingVertex, targetVertex);			
		}

		@Override
		public void onOutcommingVertexAdded(CLockGraphVertex outcommingVertex, CLockGraphVertex targetVertex,
				boolean replacing) {
			printMethodCall(outcommingVertex, targetVertex, replacing);			
		}

		@Override
		public void onOutcommingVertexRemoved(CLockGraphVertex outcommingVertex, CLockGraphVertex targetVertex,
				boolean replacing) {
			printMethodCall(outcommingVertex, targetVertex, replacing);			
		}

		@Override
		public void onOutcommingVertexReplaced(CLockGraphVertex oldOutcommingVertex,
				CLockGraphVertex newOutcommingVertex, CLockGraphVertex targetVertex) {
			printMethodCall(oldOutcommingVertex, newOutcommingVertex, targetVertex);
		}

		@Override
		public void onAllIncommingVerticesReleased(CLockGraphVertex vertex) {
			printMethodCall(vertex);
		}

		@Override
		public void onRemove(CLockGraphVertex vertex) {
			printMethodCall(vertex);			
		}
		
	}

	public static void main(String[] args) {
		
		CustomListener listener = new CustomListener();
		
		DefaultCLockGraphVertex v1 = new DefaultCLockGraphVertex(listener, new TimestampedEntity(1, 2), 1),
				v2 = new DefaultCLockGraphVertex(listener, new TimestampedEntity(1, 3), 2),
				v3 = new DefaultCLockGraphVertex(listener, new TimestampedEntity(2, 5), 4),
				v4 = new DefaultCLockGraphVertex(listener, new TimestampedEntity(5, 4), 20);
		CSimpleLockGraphVertexKeeperMemorySaving keeper = new CSimpleLockGraphVertexKeeperMemorySaving("bla", new Cleaner() {
			
			@Override
			public void reportUsage(Cleanable cleanableObject) {
				
			}
			
			@Override
			public boolean clean(Cleanable cleanableObject) {
				return false;
			}
		}, 1000L);
		LockEntity le = new LockEntity(true);
		keeper.acquire(le, v1);
		keeper.acquire(le, v2);
		v1.tryIfAllIncommingVerticesReleased();
	}

}
