package test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.distribooty.RemotePeerConnectorConfigurationBuilder;
import org.distribooty.RemotePeerConnectorLauncher;
import org.distribooty.debug.RemoteLogger;
import org.hjson.JsonObject;

import test.locktester.LockHandlerTester;
import test.locktester.LockHandlerTesterMode;

public class RemotePeerDemo {

	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		
		if(args.length < 2)
		{
			System.err.println("Configuration file in HJSON Format must be provided as first argument");
			System.err.println("The second argument must be one of { " + RemoteLogger.getPrintableList(LockHandlerTesterMode.values()) + " }");
			System.exit(-1);
		}
		System.out.println("Working directory path: " + System.getProperty("user.dir"));
		String configFileName = args[0];
		LockHandlerTesterMode mode = LockHandlerTesterMode.valueOf(args[1]);
		JsonObject config = HjsonUtils.getGlobalConfigObject(configFileName);
		JsonObject connectorConfig = config.get("connector").asObject();
		RemotePeerConnectorConfigurationBuilder builder = 
		RemotePeerConnectorConfigurationBuilderInitializer.initBuilder(connectorConfig);
		//builder.setRmiSecuritySpecs(null); // TODO try it without ssl
		LockHandlerTester tester = LockHandlerTester.getInstance(mode, config);
		RemotePeerConnectorLauncher.launch(builder, tester, true);
	}

}
