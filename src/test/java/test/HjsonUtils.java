package test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.hjson.JsonObject;
import org.hjson.JsonValue;

public class HjsonUtils {

	public static JsonObject getGlobalConfigObject(String filename) throws UnsupportedEncodingException,
	FileNotFoundException, IOException {
		Path file = Path.of(filename);
		JsonObject jo = null;
		if(Files.exists(file))
		{
			try(Reader reader = new InputStreamReader(new FileInputStream(file.toFile()), "UTF-8");) {
				jo =  JsonValue.readHjson(reader).asObject();
			}			
		}
		else
		{
			try(Reader reader = new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(filename), "UTF-8");) {
				jo =  JsonValue.readHjson(reader).asObject();
			}
		}
		return jo;
	}

}
