package test;

import java.nio.file.Paths;
import java.util.logging.Level;

import org.distribooty.RMISecuritySpecs;
import org.distribooty.RemotePeerConnectorConfigurationBuilder;
import org.distribooty.RemotePeerContact;
import org.distribooty.lock.centralized.CSimpleLockGraphVertexKeeperMode;
import org.hjson.JsonArray;
import org.hjson.JsonObject;
import org.hjson.JsonValue;

public class RemotePeerConnectorConfigurationBuilderInitializer {

	public static RemotePeerConnectorConfigurationBuilder initBuilder(JsonObject configObject) {
		RemotePeerConnectorConfigurationBuilder builder = new RemotePeerConnectorConfigurationBuilder();
		builder.setPid(configObject.get("pid").asInt());
		builder.setShutdownPassword(configObject.get("shutdownPassword").asString());
		builder.setMode(CSimpleLockGraphVertexKeeperMode.valueOf(configObject.get("mode").asString()));
		builder.setPersistenceFile(Paths.get(configObject.get("persistenceFile").asString()));
		builder.setStartElectionAutomatically(configObject.get("startElectionAutomatically").asBoolean());
		builder.setQuorumAware(configObject.get("quorumAware").asBoolean());
		builder.setCleanerPeriod(configObject.get("cleanerPeriod").asLong());
		builder.setHeartbeatPeriod(configObject.get("heartbeatPeriod").asLong());
		builder.setCSimpleLockKeeperLiveTime(configObject.get("cSimpleLockKeeperLiveTime").asLong());
		builder.setDLockWorkerLiveTime(configObject.get("dLockWorkerLiveTime").asLong());
		builder.setDLockWorkerTimeout(configObject.get("dLockWorkerTimeout").asLong());
		builder.setNeighbourDiscoveryPeriod(configObject.get("neighbourDiscoveryPeriod").asLong());
		builder.setLoggingLevel(Level.parse(configObject.get("loggingLevel").asString()));
		builder.setRmiSecuritySpecs(getRMISecuritySpecs(configObject.get("securitySpecs").asObject()));
		injectContacts(configObject.get("contacts").asArray(), builder);
		return builder;
	}	
	
	public static RMISecuritySpecs getRMISecuritySpecs(JsonObject securityObject) {
		if(securityObject == null)
			return null;
		return new RMISecuritySpecs(
				Paths.get(securityObject.get("keystorePath").asString()),
				securityObject.get("keystorePassword").asString(),
				securityObject.get("enabledCipherSuites").asString(),
				securityObject.get("enabledProtocols").asString(),
				securityObject.get("needClientAuth").asBoolean()
				);
	}
	
	public static RemotePeerContact getContact(JsonObject contactObject) {
		return new RemotePeerContact(
				contactObject.get("pid").asInt(), 
				contactObject.get("host").asString(),
				contactObject.get("port").asInt(), 
				contactObject.get("bindName").asString()
				);
	}
	
	public static void injectContacts(JsonArray contactArray, RemotePeerConnectorConfigurationBuilder builder) {
		for(JsonValue value : contactArray)		
			builder.addContact(getContact(value.asObject()));		
	}
}
