package test;

import java.rmi.Remote;
import java.rmi.RemoteException;

import org.distribooty.lock.centralized.TimestampedCSimpleLocks;
import org.distribooty.lock.decentralized.GenericDLockRequest;

public interface LockMonitor extends Remote {
	String BIND_NAME = "LockMonitor";
	
	void logLock(TimestampedCSimpleLocks lockRequest, String message) throws RemoteException;
	void logUnlock(TimestampedCSimpleLocks lockRequest, String message) throws RemoteException;
	void logLock(GenericDLockRequest lockRequest, String message) throws RemoteException;
	void logUnlock(GenericDLockRequest lockRequest, String message) throws RemoteException;
	void reset(int pids[]) throws RemoteException;
}
