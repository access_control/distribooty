package test;

import java.util.concurrent.ScheduledThreadPoolExecutor;

import org.distribooty.util.AbstractWorker;
import org.distribooty.util.ControlledBarrierListener;
import org.distribooty.util.ControlledFlexCyclicBarrier;
import org.distribooty.util.ControllerBarrierInterface;
import org.distribooty.util.Shutdowner;
import org.distribooty.util.WorkerBarrierInterface;

public class ControlledFlexCyclicBarrierTester {
	
	private static class SimpleWorker extends AbstractWorker {
		
		private WorkerBarrierInterface workerBarrierInterface;		

		public SimpleWorker(boolean notified, String name, WorkerBarrierInterface workerBarrierInterface) {
			super(notified, name);
			this.workerBarrierInterface = workerBarrierInterface;
		}

		@Override
		public void run() {
			
			onRunEnter();
			
			while(true)
			{
				boolean execStatus = workerBarrierInterface.waitForStart();
				if(shutdownFlag || !execStatus)
					break;
				System.out.println("Begin executing of " + name);
				try {
					Thread.sleep(1000L);
				} catch (InterruptedException e) {
					
				}
				System.out.println("End of executing of " + name);				
			}
			System.out.println("Finishing of " + name);				
			
			onRunLeave();
		}	
		
		@Override
		protected void onRunLeave() {
			workerBarrierInterface.leave();
			super.onRunLeave();
		}
	}
	
	
	private static class SimpleController extends AbstractWorker {
		private ControllerBarrierInterface controllerBarrierInterface;

		public SimpleController(boolean notified, String name, ControllerBarrierInterface controllerBarrierInterface) {
			super(notified, name);
			this.controllerBarrierInterface = controllerBarrierInterface;
		}

		@Override
		public void run() {
			try {
				onRunEnter();
				System.out.println("Controller + " + name + ": waiting for all parties");
				controllerBarrierInterface.waitForReadyParties(6);
				System.out.println("Controller + " + name + ": before execute");
				controllerBarrierInterface.execute(new ControlledBarrierListener() {
					
					@Override
					public void onBegin() {
						System.out.println("Begin of Controller " + name);					
					}
					
					@Override
					public void onEnd() {
						System.out.println("End of Controller " + name);					
					}
					
				});
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(-1);
			}
			onRunLeave();
		}		
	}

	public ControlledFlexCyclicBarrierTester() {
	}

	public static void main(String[] args) {
		
		try {
		
		ScheduledThreadPoolExecutor stpe = new ScheduledThreadPoolExecutor(100);
		ControlledFlexCyclicBarrier barrier = new ControlledFlexCyclicBarrier();
		SimpleWorker w1 = new SimpleWorker(false, "w1", barrier),
					 w2 = new SimpleWorker(false, "w2", barrier),
					 w3 = new SimpleWorker(false, "w3", barrier),
					 w4 = new SimpleWorker(false, "w4", barrier),
					 w5 = new SimpleWorker(false, "w5", barrier),
					 w6 = new SimpleWorker(false, "w6", barrier);
		SimpleController c1 = new SimpleController(false, "c1", barrier), c2 = new SimpleController(false, "c2", barrier);
		Shutdowner controllerShutdowner = new Shutdowner(c1, c2);
		System.out.println("Main: starting all");
		stpe.execute(c1);
		stpe.execute(c2);
		stpe.execute(w1);
		stpe.execute(w2);
		stpe.execute(w3);
		stpe.execute(w4);
		stpe.execute(w5);
		stpe.execute(w6);
		
		controllerShutdowner.waitForShutdownComplete();
		System.out.println("Main: all controllers finished");
		barrier.shutdown(true);
		barrier.waitForShutdownComplete();
		System.out.println("Main: all workers finished");
	
		try {
			boolean execStatus = barrier.execute(new ControlledBarrierListener() {
				
				@Override
				public void onBegin() {
					System.out.println("Begin of Finisher");				
				}
				
				@Override
				public void onEnd() {
					System.out.println("End of Finisher");		
					
				}			
			});
			System.out.println("Main: repeated barrier controller call status " + execStatus);
		} catch (Exception e) {
			System.out.println("Main: repeated barrier controller call status " + false);
		}
		stpe.shutdown();
		System.out.println("Main: all shutdown");
		} catch(Exception e) {
			System.err.println("Total error");
			e.printStackTrace();
			System.exit(-1);			
		}
	}

}
