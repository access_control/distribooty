package test;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.distribooty.lock.centralized.CSimpleLock;
import org.distribooty.lock.centralized.TimestampedCSimpleLocks;
import org.distribooty.lock.decentralized.GenericDLockRequest;
import org.distribooty.util.AbstractWorker;

public class LockMonitorImpl extends AbstractWorker implements LockMonitor {
	
	private long timeout;
	
	private class LockDataHolder {
		
		private int exclusivePid = -1;
		private Map<Integer, AtomicInteger> sharedPids = new ConcurrentHashMap<>();
		
		private boolean lockExclusive(int pid) {
			AtomicInteger counter;
			if(exclusivePid != -1 || (counter = sharedPids.get(pid)) != null && counter.get() != 0)
				return false;
			exclusivePid = pid;
			return true;
		}
		
		private boolean unlockExclusive(int pid) {
			if(exclusivePid != pid)
				return false;
			exclusivePid = -1;
			return true;
		}
		
		private boolean lockNonExclusive(int pid) {
			if(exclusivePid != -1)
				return false;
			sharedPids.computeIfAbsent(pid, key -> new AtomicInteger()).incrementAndGet();
			return true;
		}
		
		private boolean unlockNonExclusive(int pid) {
			AtomicInteger counter;
			if((counter = sharedPids.get(pid)) == null || counter.get() == 0)
				return false;
			counter.decrementAndGet();
			return true;
		}
		
		public boolean lock(int pid, boolean exclusive) {
			if(exclusive)
				return lockExclusive(pid);
			else
				return lockNonExclusive(pid);
		}
		
		public boolean unlock(int pid, boolean exclusive) {
			if(exclusive)
				return unlockExclusive(pid);
			else
				return unlockNonExclusive(pid);
		}
		
		public void reset(Set<Integer> pids) {
			if(pids.contains(exclusivePid))
				exclusivePid = -1;
			sharedPids.keySet().removeAll(pids);
		}
	}
	
	private Map<String, LockDataHolder> locks = new HashMap<>();

	public LockMonitorImpl(long timeout) throws RemoteException {
		super(true, "LockMonitorImpl");
		this.timeout = timeout;
	}
	
	public static void main(String[] args) throws RemoteException {
		if(args.length == 0)
		{
			System.out.println("The local port for the monitor is not provided");
			return;
		}
		int port;
		try {
			port = Integer.parseInt(args[0]);
			if(port < 1024)
			{
				System.out.println("The provided port is invalid");
				return;
			}
		} catch (NumberFormatException e) {
			System.out.println("The provided port is invalid (not a number at all)");
			return;
		}
		LockMonitorImpl lockObj = new LockMonitorImpl(1000L * 60L * 15L);
		LockMonitor remoteLockObj = (LockMonitor) UnicastRemoteObject.exportObject(lockObj, port);
		Registry localRegistry = LocateRegistry.createRegistry(port);
		localRegistry.rebind(BIND_NAME, remoteLockObj);
		Thread monitorThread = new Thread(lockObj);
		monitorThread.start();
		System.out.println("The Lock Monitor on port " + port + " is started");
		lockObj.waitForShutdownComplete();
		while(!UnicastRemoteObject.unexportObject(lockObj, false))
				;
		System.out.println("The Lock Monitor is automatically shutdown");
	}

	@Override
	public void logLock(TimestampedCSimpleLocks lockRequest, String message) throws RemoteException {
		wakeUp();
		synchronized(this.locks)
		{
			long timeStamp = lockRequest.getTimestamp();
			for(CSimpleLock cLock : lockRequest.getCSimpleLocks())
			{
				LockDataHolder holder = this.locks.computeIfAbsent(cLock.getLockId(), key -> new LockDataHolder());
				boolean exclusive = cLock.isExclusive();
				if(holder.lock(lockRequest.getSrcPid(), exclusive))
				{
					System.out.println("Lock of the " + (exclusive ? "Exclusive" : "Non-Exclusive") + " lock with lockId \"" + 
				cLock.getLockId() + "\" and timestamp " + timeStamp + 
				" from pid " + lockRequest.getSrcPid()+  " acquired. " + message);
				}
				else
				{
					System.out.println("!!!ATTENTION!!! Lock conflict by acquiring the " + 
				(exclusive ? "exclusive" : "non-exclusive") + " lock with lockId \"" + cLock.getLockId()+ 
				"\" and timestamp " + timeStamp + " from pid " + lockRequest.getSrcPid() + ". " + message);
				}
			}
			System.out.println("");
		}
		
	}

	@Override
	public void logUnlock(TimestampedCSimpleLocks lockRequest, String message) throws RemoteException {
		wakeUp();
		synchronized (this.locks)
		{
			long timeStamp = lockRequest.getTimestamp();
			for(CSimpleLock cLock : ((TimestampedCSimpleLocks) lockRequest).getCSimpleLocks())
			{
				LockDataHolder holder = this.locks.computeIfAbsent(cLock.getLockId(), key -> new LockDataHolder());
				boolean exclusive = cLock.isExclusive();
				if(holder.unlock(lockRequest.getSrcPid(), exclusive))
				{
					System.out.println("Unlock of the " + (exclusive ? "exclusive" : "non-exclusive") + 
							" lock with lockId \"" + cLock.getLockId() + "\" and timestamp " + timeStamp + 
							" from pid " + lockRequest.getSrcPid() + " acquired. " + message);
				}
				else
				{
					System.out.println("!!!ATTENTION!!! Unlock conflict by acquiring the " + 
				(exclusive ? "exclusive" : "non-exclusive") + " lock with lockId \"" + 
							cLock.getLockId() + "\" and timestamp " + timeStamp + 
							" from pid " + lockRequest.getSrcPid() + ". " + message);
				}
			}
			System.out.println("");
		}
	}
	
	

	
	@Override
	public void logLock(GenericDLockRequest lockRequest, String message) throws RemoteException {
		logLock(new TimestampedCSimpleLocks(lockRequest.getSrcPid(), lockRequest.getTimestamp(), new CSimpleLock(lockRequest.getLockId(), true)), message);
	}

	@Override
	public void logUnlock(GenericDLockRequest lockRequest, String message) throws RemoteException {
		logUnlock(new TimestampedCSimpleLocks(lockRequest.getSrcPid(), lockRequest.getTimestamp(), new CSimpleLock(lockRequest.getLockId(), true)), message);
	}

	private static String pidList(int pids[]) {
		StringBuilder strBuild = new StringBuilder();
		boolean firstWritten = false;
		for(int pid : pids)
		{
			if(firstWritten)
				strBuild.append(',').append(pid);
			else
			{
				strBuild.append(pid);
				firstWritten = true;
			}
		}
		return strBuild.toString();
	}

	@Override
	public void reset(int pids[]) throws RemoteException {
		wakeUp();
		if(pids == null)
			synchronized(this.locks)
			{
				this.locks.clear();
				System.out.println("\nThe locks resetted\n");
			}
		else
		{
			synchronized(this.locks)
			{
				
				Set<Integer> _pids = new HashSet<>();
				for(int p : pids)
					_pids.add(p);
				for(LockDataHolder l : this.locks.values())				
					l.reset(_pids);
				System.out.println("\nThe choosen locks resetted for pids: " + pidList(pids) + "\n");
			}
		}
	}

	@Override
	public void run() {
		onRunEnter();
		while(true)
		{
			try {
				synchronized(lock)
				{
					AwaitStatus awaitStatus = await(timeout);
					if(awaitStatus != AwaitStatus.NOTIFIED)
						break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		onRunLeave();
	}
}
