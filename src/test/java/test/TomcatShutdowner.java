package test;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class TomcatShutdowner {

	public TomcatShutdowner() {}
	
	public static int convertPort(String port) throws IllegalArgumentException {
		int p;
		try {
			p = Integer.parseInt(port);
			if(p < 1025)
				throw new IllegalArgumentException("Port " + p + " must be greater than 1024");
			return p;
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public static void main(String[] args) {
		
		int port;
		try {
			port = convertPort(args[0]);
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			return;
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Usage: port [shutdown message]");
			return;
		}
		String shutdownMessage;
		try {
			shutdownMessage = args[1];
		} catch (IndexOutOfBoundsException e) {
			shutdownMessage = "SHUTDOWN";
		}
		
		try {
			Socket client = new Socket("localhost", port);
			OutputStream os = client.getOutputStream();
			os.write(shutdownMessage.getBytes());
			client.close();
			System.out.println("Shutdown message sent successfully");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
